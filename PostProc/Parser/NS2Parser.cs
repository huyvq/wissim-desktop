﻿using PostProc.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace PostProc.Parser
{
    /// <summary>
    /// NS2 Parser, parser for ns2 trace file
    /// <author>Trong Nguyen</author>
    /// </summary>
    public class NS2Parser : IParser
    {
        private Dictionary<int, NodeData> nodeEnergies = new Dictionary<int, NodeData>();
        private Dictionary<int, NodeData> nodes = new Dictionary<int, NodeData>();
        private Dictionary<int, PacketData> packets = new Dictionary<int, PacketData>();
        private List<EventData> events = new List<EventData>();
        private List<string> packetTypes = new List<string>();
        private readonly double energyPatternRate = 0.1; //(second)

        public Dictionary<int, NodeData> GetNodes()
        {
            if (nodes == null) nodes = new Dictionary<int, NodeData>();
            return nodes;
        }

        public Dictionary<int, NodeData> GetNodeEnergies()
        {
            if (nodeEnergies == null) nodeEnergies = new Dictionary<int, NodeData>();
            return nodeEnergies;
        }

        public Dictionary<int, PacketData> GetPackets()
        {
            if (packets == null) packets = new Dictionary<int, PacketData>();
            return packets;
        }

        public List<EventData> GetEvents()
        {
            if (events == null) events = new List<EventData>();
            return events;
        }

        public List<string> GetPacketTypes()
        {
            if (packetTypes == null)
            {
                packetTypes = new List<string>();
            }

            return packetTypes;
        }

        public void Parse(string[] traceFiles)
        {
            //if (traceFiles.Length >= 3)
            {
                Console.WriteLine(DateTime.Now.ToString("hh.mm.ss.ffffff"));
                ParseEnergyFile(traceFiles[0]);
                ParseNeighbourFile(traceFiles[1]);
                ParseMainFile(traceFiles[2]);
                Console.WriteLine(DateTime.Now.ToString("hh.mm.ss.ffffff"));
            }
            //else
            //{
            //    throw new Exception("You have to choose at least 3 files");
            //}
        }

        private void ParseEnergyFile(string energyFilePath)
        {
            try
            {
                string currentLine = "";
                char[] delimiter = { '\t' };

                StreamReader file = new StreamReader(energyFilePath);

                while ((currentLine = file.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(currentLine) == false)
                    {
                        string[] retVal = currentLine.Split(delimiter);
                        int id = Convert.ToInt32(retVal[0]);
                        double x = Convert.ToDouble(retVal[1]);
                        double y = Convert.ToDouble(retVal[2]);
                        double energy = Convert.ToDouble(retVal[3]);
                        double offtime = Convert.ToDouble(retVal[4]);

                        NodeData node = new NodeData(id, x, y, energy, offtime);
                        nodeEnergies.Add(id, node);
                    }
                }

                file.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ParseNeighbourFile(string neighbourFilePath)
        {
            //1. Read node trace file
            try
            {
                string currentLine = "";
                System.IO.StreamReader file = new System.IO.StreamReader(neighbourFilePath);
                while ((currentLine = file.ReadLine()) != null)
                {
                    //TODO: Change regex to String.Split here -for faster
                    if (string.IsNullOrEmpty(currentLine) == false)
                    {
                        string[] retval = Regex.Split(currentLine, "\\s+");
                        int id = System.Convert.ToInt32(retval[0]);
                        double x = System.Convert.ToDouble(retval[1]);
                        double y = System.Convert.ToDouble(retval[2]);
                        NodeData node = new NodeData(id, x, y);
                        nodes.Add(id, node);
                        try
                        {
                            node.OffTime = System.Convert.ToDouble(retval[3]);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.StackTrace);
                        }
                    }
                }

                file.Close();
            }
            catch (Exception ex)
            {
            }
        }

        private void ParseMainFile(string mainFilePath)
        {
            int eventIndex = 0;
            int i = 0;
            string mainCurrentLine = "";
            try
            {
                //2. Read event trace file
                System.IO.StreamReader mainFileStream = new System.IO.StreamReader(mainFilePath);
                string[] mainRetVal;

                while ((mainCurrentLine = mainFileStream.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(mainCurrentLine) == false)
                    {
                        i = i + 1;
                        //replace double space by one space

                        mainRetVal = mainCurrentLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        //string [] headers = mainCurrentLine.Split(new char[] { ']' }, StringSplitOptions.RemoveEmptyEntries);
                        switch (mainRetVal[0])
                        {
                            case "N": // Energy: N -t 0.147751 -n 3 -e 999.998548
                                if (mainRetVal.Length < 7)
                                {
                                    //continue;
                                    Console.WriteLine("Energy: Trace in line " + i.ToString() + " is not in correct format");
                                    Console.WriteLine(mainCurrentLine);
                                    break;
                                }
                                int nodeId = System.Convert.ToInt32(mainRetVal[4]);
                                double time = System.Convert.ToDouble(mainRetVal[2]);

                                // Lay mau voi tan so energyPatternRate
                                if ((nodes[nodeId].Energies.Count == 0) || (time - nodes[nodeId].Energies.Last().Time > energyPatternRate))
                                {
                                    double nodeEnergy = System.Convert.ToDouble(mainRetVal[6]);
                                    nodes[nodeId].Energies.Add(new NodeEnergy(time, nodeEnergy));
                                }

                                break;

                            case "s": // Send:      s 0.002259878 _237_ RTR  --- 0 HELLO 36 [0 0 0 0] ------- [237:255 -1:255 32 0]
                            case "r": //Receieve:   r 0.003336783 _475_ RTR  --- 1 HELLO 36 [0 ffffffff 4b 800] ------- [75:255 -1:255 32 0]
                            case "f": // forward:   f 34.925440740 _939_ RTR  --- 1112 GRID 76 [13a 3ab 3ac 800] ------- [86:255 938:255 65 938]
                            case "D": // drop:      D 34.936054784 _256_ RTR  REPEAT 1113 GRID 74 [13a 100 3c3 800] ------- [537:255 256:255 65 256]
                                //With energy:
                                //Send: s 0.002259878 _237_ RTR  --- 0 HELLO 36 [0 0 0 0] [energy 1000.000000 ei 0.000 es 0.000 et 0.000 er 0.000] ------- [237:255 -1:255 32 0]
                                //Forward: f 364.672019363 _879_ RTR  --- 1398 cbr 123 [13a 36f 2ce 800] [energy 996.434502 ei 3.484 es 0.000 et 0.003 er 0.079] ------- [527:0 -1:0 94 878]

                                // --> Truong hop nay lai ko co IP header o sau cung (ma dang dung goi tin ARP de hoi thong tin.
                                //D 31.118554157 _1177_ IFQ  --- 0 ARP 28 [0 ffffffff 499 806] [energy 999.683405 ei 0.285 es 0.000 et 0.006 er 0.026] ------- [REQUEST 1177/1177 0/1062]
                                // New packet
                                if (mainRetVal.Length < 17)
                                {
                                    throw new Exception("Event: Trace in line " + i.ToString() + " is not in correct format");
                                }

                                int packetId = System.Convert.ToInt32(mainRetVal[5]);
                                PacketData packet;
                                if (!packets.ContainsKey(packetId))
                                {
                                    packet = new PacketData(packetId);
                                    packet.PacketType = mainRetVal[6];
                                    if (!this.packetTypes.Contains(packet.PacketType))
                                    {
                                        packetTypes.Add(packet.PacketType);
                                    }
                                    packet.PacketSize = System.Convert.ToInt32(mainRetVal[7]);
                                    if (mainRetVal[0] == "s")
                                    {
                                        string sourceNodeId = mainRetVal[13];
                                        string destinationNodeId = mainRetVal[14];
                                        if (mainRetVal[12] == "[energy")
                                        {
                                            sourceNodeId = mainRetVal[23];
                                            destinationNodeId = mainRetVal[24];
                                        }

                                        sourceNodeId = sourceNodeId.Substring(1, sourceNodeId.IndexOf(":") - 1);
                                        packet.SourceNode = nodes[System.Convert.ToInt32(sourceNodeId)];
                                        destinationNodeId = destinationNodeId.Substring(0, destinationNodeId.IndexOf(":"));
                                        if (destinationNodeId == "-1")
                                        {
                                            packet.DestinationNode = null;
                                            packet.IsMultiCast = true;
                                        }
                                        else
                                        {
                                            packet.DestinationNode = nodes[System.Convert.ToInt32(destinationNodeId)];
                                            //packet.SourceNode = nodes[System.Convert.ToInt32(sourceNodeId)];
                                            packet.IsMultiCast = false;
                                        }
                                        packet.IsDrop = false;
                                    }
                                    packets.Add(packetId, packet);
                                }
                                else
                                {
                                    packet = packets[packetId];
                                }

                                nodeId = System.Convert.ToInt32(mainRetVal[2].Substring(1, mainRetVal[2].Length - 2));

                                // New event
                                EventData e = new EventData();
                                e.Id = eventIndex++;
                                e.HappendTime = System.Convert.ToDouble(mainRetVal[1]);
                                e.Node = nodes[nodeId];
                                e.Layer = mainRetVal[3];
                                e.Message = mainRetVal[4];
                                if (mainRetVal[0] == "s") e.Type = EventData.EventType.SEND;
                                if (mainRetVal[0] == "r")
                                {
                                    e.Type = EventData.EventType.RECEIVE;

                                    //Update previousNode
                                    string previousNodeId = mainRetVal[10];
                                    e.PreviousNode = nodes[System.Convert.ToInt32(previousNodeId, 16)];
                                }
                                if (mainRetVal[0] == "f") e.Type = EventData.EventType.FORWARD;
                                if (mainRetVal[0] == "D")
                                {
                                    e.Type = EventData.EventType.DROP;
                                    if (packet.DestinationNode != null)
                                    {
                                        //Ko phai BroadCast. Truong hop (Distribued, Unicast). Multicast chua xac dinh dat o dau
                                        if ((nodeId != packet.DestinationNode.ID) || (packet.SourceLayer == e.Layer))
                                        {
                                            //Note: Gia thiet la da co 1 event send danh cho packet dang xet nay.
                                            packet.IsDrop = true;
                                        }
                                    }
                                }
                                e.Packet = packet;
                                //Update header of packets.
                                //string[] headers = mainCurrentLine.Split(new char[] { '[',']' }, StringSplitOptions.RemoveEmptyEntries);
                                //e.AddHeader(headers[1]);
                                //e.AddHeader(headers[headers.Length - 2]);
                                events.Add(e);

                                // Update packet.NodeList; packet.EventList
                                packet.EventList.Add(e);
                                //Co the khong can den tham so nay...
                                if (!packet.NodeList.Contains(e.Node))
                                {
                                    packet.NodeList.Add(e.Node);
                                }
                                // Update node.nodeEvent
                                nodes[nodeId].NodeEvent.Add(e);

                                //TODO: Update node.Packet.
                                break;

                            case "M": // Move:
                                // do nothing
                                break;
                        }
                    }
                }
                mainFileStream.DiscardBufferedData();
                mainFileStream.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                Console.WriteLine(mainCurrentLine);
            }
        }

        public void Reset()
        {
            nodeEnergies.Clear();
            nodes.Clear();
            packets.Clear();
            events.Clear();
        }
    }
}