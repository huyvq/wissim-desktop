﻿using PostProc.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PostProc.Parser
{
    /// <summary>
    /// Interface for Parser
    /// <author>Trong Nguyen</author>
    /// </summary>
    interface IParser
    {
        //TODO: NodeData --> INode
        //PacketData --> IPacket
        Dictionary<int, NodeData> GetNodes();
        Dictionary<int, PacketData> GetPackets();
        List<EventData> GetEvents();
        List<string> GetPacketTypes();
    }
}
