using PostProc.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostProc
{
    public class Process
    {
        public static double calc_average_energy(Dictionary<int, NodeData> energy)
        {
            double sum = 0;
            foreach (var e in energy)
            {
                sum += (1000 - e.Value.RemainingEnergy);
            }
            return sum / energy.Count;
        }

        public static double calc_standard_deviation_energy(Dictionary<int, NodeData> energy)
        {
            double sum = 0;
            double sum_of_mean = 0;

            foreach (var e in energy)
            {
                sum += e.Value.RemainingEnergy;
            }

            double mean = sum / energy.Count;

            foreach (var e in energy.Values)
            {
                sum_of_mean += (e.RemainingEnergy - mean) * (e.RemainingEnergy - mean);
            }

            return Math.Sqrt(sum_of_mean / energy.Count);
        }

        public static double calc_avg_hop_count(Dictionary<int, PacketData> packets)
        {
            List<PacketData> list = new List<PacketData>();
            double sum = 0;
            long cbr_number = 0;
            foreach (var p in packets.Values)
            {
                if (String.Equals(p.PacketType, "CBR", StringComparison.OrdinalIgnoreCase))
                {
                    if (!p.IsDrop)
                    {
                        int hopcount = 0;
                        foreach (var e in p.EventList)
                        {
                            if (e.Layer == "RTR" && e.Type == EventData.EventType.RECEIVE)
                            {
                                hopcount++;
                            }
                        }
                        cbr_number++;
                        sum += hopcount;
                    }
                }
            }

            return sum / cbr_number;
        }

        public static double calc_drop_ratio(Dictionary<int, PacketData> packets)
        {
            List<PacketData> list = new List<PacketData>();
            double drop = 0;
            long cbr_number = 0;

            foreach (var p in packets.Values)
            {
                if (String.Equals(p.PacketType, "CBR", StringComparison.OrdinalIgnoreCase))
                {
                    if (p.IsDrop)
                    {
                        drop++;
                    }

                    cbr_number++;
                }
            }

            return drop / cbr_number;
        }

        public static double calc_network_life_time(Dictionary<int, NodeData> nodes, double percent)
        {
            double number_of_nodes = nodes.Count;
            List<double> life_times = nodes.OrderBy(x => x.Value.OffTime).Select(x => x.Value.OffTime).ToList();

            return life_times[(int)Math.Round(number_of_nodes * percent / 100)];
        }

        public static Dictionary<NodeData, int> analyze(Dictionary<int, NodeData> dict)
        {
            Dictionary<NodeData, int> result = new Dictionary<NodeData, int>();

            for (int i = 0; i < 1500; i++)
            {
                NodeData node = new NodeData(i, 0, 0);
                result.Add(node, 0);
            }

            foreach (var nodeData in dict)
            {
                foreach (var packetData in nodeData.Value.PacketList)
                {
                    if (packetData.IsDrop == false && packetData.PacketType == "cbr")
                    {
                        var data = nodeData;
                        var o = result.Where(x => x.Key.ID == data.Value.ID);
                        if (o.Any())
                            result[o.FirstOrDefault().Key]++;
                    }
                }
            }

            return result;
        }

        public static double get_min_lifetime(Dictionary<int, NodeData> energy)
        {
            return energy.OrderBy(x => x.Value.OffTime).First().Value.OffTime;
        }
    }
}