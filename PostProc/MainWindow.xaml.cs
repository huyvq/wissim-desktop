using Ookii.Dialogs.Wpf;
using PostProc.Parser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace PostProc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly string EneryFile = "energy.tr";
        private static readonly string NeighborsFile = "neighbors.tr";
        private static readonly string TraceFile = "trace.tr";

        private CancellationTokenSource _cts;

        private string workingDirectory = string.Empty;

        private bool isRunning = false;

        private bool? AvgEnergy = false;
        private bool? AvgHopCount = false;
        private bool? DropRatio = false;
        private bool? StdDeviation = false;

        private bool? Percent10 = false;
        private bool? Percent20 = false;
        private bool? Percent30 = false;
        private bool? Percent50 = false;

        private int ExpNumber = 0;

        public MainWindow()
        {
            InitializeComponent();
        }

        #region Select Working Directory

        private void btnSelectPath_Click(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog dlg = new VistaFolderBrowserDialog();
            dlg.RootFolder = System.Environment.SpecialFolder.Desktop;
            dlg.ShowNewFolderButton = true;
            if (dlg.ShowDialog() == true)
            {
                tbxPath.Text = dlg.SelectedPath;
                read_folder_contents(dlg.SelectedPath);
                workingDirectory = dlg.SelectedPath;
            }
        }

        private void read_folder_contents(string folder_path)
        {
            DirectoryInfo dir = new DirectoryInfo(folder_path);
            DirectoryInfo[] info = dir.GetDirectories();

            foreach (var d in info)
            {
                if (validate_folder_content(d))
                {
                    ExpNumber++;
                }
            }
            pbStatus.Maximum = ExpNumber;
            txtNumExp.Text = ExpNumber.ToString();
            btnStart.IsEnabled = true;
        }

        /// <summary>
        /// đọc nội dung từng sub folder, yêu cầu có 3 file energy.tr, neighbors.tr, trace.tr
        /// </summary>
        /// <param name="sub_folder"></param>
        private bool validate_folder_content(DirectoryInfo sub_folder)
        {
            FileInfo[] info = sub_folder.GetFiles("*.tr");

            List<string> list = new List<string>();
            foreach (var f in info)
            {
                list.Add(f.Name);
            }

            return (list.FindAll(s => s.IndexOf(EneryFile, StringComparison.OrdinalIgnoreCase) >= 0).Count > 0 &&
                list.FindAll(s => s.IndexOf(NeighborsFile, StringComparison.OrdinalIgnoreCase) >= 0).Count > 0 &&
                list.FindAll(s => s.IndexOf(TraceFile, StringComparison.OrdinalIgnoreCase) >= 0).Count > 0);
            //return (list.FindAll(s => s.IndexOf(EneryFile, StringComparison.OrdinalIgnoreCase) >= 0).Count > 0);
        }

        #endregion Select Working Directory

        #region Parsing

        private void read_output_option()
        {
            AvgEnergy = enAvgEnergy.IsChecked;
            AvgHopCount = enAvgHopCount.IsChecked;
            DropRatio = enDropRatio.IsChecked;
            StdDeviation = enStdDeviation.IsChecked;

            Percent10 = en10.IsChecked;
            Percent20 = en20.IsChecked;
            Percent30 = en30.IsChecked;
            Percent50 = en50.IsChecked;
        }

        private async void btnStart_Click(object sender, RoutedEventArgs e)
        {
            isRunning = !isRunning;

            if (!isRunning)
            {
                mainPanel.IsEnabled = true;
                btnStart.Content = "Start";

                pbStatus.Value = pbStatus.Minimum;

                _cts.Cancel();
            }
            else
            {
                _cts = new CancellationTokenSource();
                var token = _cts.Token;

                pbStatus.Value = pbStatus.Minimum;
                mainPanel.IsEnabled = false;
                btnStart.Content = "Stop";

                read_output_option();
                try
                {
                    await Task.Run(() =>
                    {
                        token.ThrowIfCancellationRequested();
                        parse_folder_contents(workingDirectory);
                    });
                    process_done();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void parse_folder_contents(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            DirectoryInfo[] info = dir.GetDirectories();

            foreach (var d in info)
            {
                parse_sub_folder_contents(d);
                update_progress_bar();
            }
        }

        private void parse_sub_folder_contents(DirectoryInfo dir)
        {
            FileInfo enery = dir.GetFiles(EneryFile)[0];
            FileInfo neighbors = dir.GetFiles(NeighborsFile)[0];
            FileInfo trace = dir.GetFiles(TraceFile)[0];

            ///
            /// parse file
            ///
            string[] paths = { enery.FullName, neighbors.FullName, trace.FullName };

            NS2Parser ns2parser = new NS2Parser();
            ns2parser.Reset();
            ns2parser.Parse(paths);

            ///
            /// dump to output
            ///
            dump_output(dir.Name, ns2parser);
        }

        private void update_progress_bar()
        {
            this.Dispatcher.BeginInvoke((Action)delegate()
            {
                pbStatus.Value++;
            });
        }

        #endregion Parsing

        #region Post Processing

        private async void dump_output(string exp_name, NS2Parser parser)
        {
            StringBuilder sb = new StringBuilder();

            double avg_energy = Process.calc_average_energy(parser.GetNodeEnergies());
            double std_devia = Process.calc_standard_deviation_energy(parser.GetNodeEnergies());
            double avg_hop_count = Process.calc_avg_hop_count(parser.GetPackets());
            double lifetime = Process.get_min_lifetime(parser.GetNodeEnergies());

            double drop_ratio = Process.calc_drop_ratio(parser.GetPackets());

            //double p10 = Process.calc_network_life_time(parser.GetNodeEnergies(), 10);
            //double p20 = Process.calc_network_life_time(parser.GetNodeEnergies(), 20);
            //double p30 = Process.calc_network_life_time(parser.GetNodeEnergies(), 30);
            //double p50 = Process.calc_network_life_time(parser.GetNodeEnergies(), 50);

            sb.AppendLine(exp_name);

            sb.Append("Năng lượng tiêu thụ trung bình\t");
            sb.AppendLine(avg_energy.ToString());

            sb.Append("Độ lệch chuẩn năng lượng còn lại\t");
            sb.AppendLine(std_devia.ToString());

            sb.Append("Độ dài đường đi trung bình\t");
            sb.AppendLine(avg_hop_count.ToString());

            sb.Append("Tỉ lệ mất gói tin\t");
            sb.AppendLine(drop_ratio.ToString());

            //sb.Append("Thời gian sống của mạng tại 10%\t");
            //sb.AppendLine(p10.ToString());

            //sb.Append("Thời gian sống của mạng tại 20%\t");
            //sb.AppendLine(p20.ToString());

            //sb.Append("Thời gian sống của mạng tại 30%\t");
            //sb.AppendLine(p30.ToString());

            //sb.Append("Thời gian sống của mạng tại 50%\t");
            //sb.AppendLine(p50.ToString());

            sb.AppendLine("---");

            //// Write the stream contents to a new file named "AllTxtFiles.txt".
            using (StreamWriter outfile = new StreamWriter(workingDirectory + @"\Dump.txt", true))
            {
                await outfile.WriteAsync(sb.ToString());
            }
        }

        private void process_done()
        {
            mainPanel.IsEnabled = true;
            btnStart.Content = "Start";
        }

        #endregion Post Processing
    }
}