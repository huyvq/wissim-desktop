﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostProc.Model
{
    public class PacketData
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private List<EventData> eventList = new List<EventData>();

        internal List<EventData> EventList
        {
            get { return eventList; }
        }

        private List<NodeData> nodeList = new List<NodeData>();

        //TODO: Co can phai node list ko ? Y nghia cua no la gi? Co the remove.
        internal List<NodeData> NodeList
        {
            get { return nodeList; }
        }

        private string packetType;

        public string PacketType
        {
            get { return packetType; }
            set { packetType = value; }
        }

        private int packetSize;

        public int PacketSize
        {
            get { return packetSize; }
            set { packetSize = value; }
        }

        public PacketData(int Id)
        {
            this.id = Id;
        }

        public Boolean IsVisualization { get; set; }

        /*Implement bussiness function here*/

        #region Derived Data

        //Hop Count chi tinh duoc cho cac goi tin dinh tuyen binh thuong, va khong phai la multicast
        //Trong chuong trinh, mac dinh goi tin HELLO la goi tin multicast cho hang xom - bo qua.
        //Xac dinh la MultiCast neu nhu tren pacet nay xuat hien nhieu lan Su kien send
        // C2: do la su dung header o dang sau de xem node dich la id hay la fffffff;
        // hopCount = 0 neu packet la multiCast
        // hopCount = -1 neu chua duoc tinh
        private int hopCount = -1;

        public int HopCount
        {
            get
            {
                if (this.hopCount == -1)
                {
                    if (this.IsMultiCast == false)
                    {
                        this.hopCount = this.NodeList.Count - 1;
                    }
                    else
                    {
                        this.hopCount = 0;
                    }
                }

                return this.hopCount;
            }

            set { hopCount = value; }
        }

        private Boolean isMultiCast;

        public bool IsMultiCast
        {
            get
            { //Note: Da tinh toan isMultiCast o trong Parser truoc roi.
                return isMultiCast;
            }
            set { isMultiCast = value; }
        }

        public NodeData SourceNode { get; set; }

        public NodeData DestinationNode { get; set; }

        public bool IsDrop { get; set; }

        private double latency = 0;

        public double Latency
        {
            get
            {
                if (!this.IsMultiCast)
                {
                    EventData first = this.EventList.First();
                    EventData last = this.EventList.Last();
                    this.latency = last.HappendTime - first.HappendTime;
                }
                return this.latency;
            }

            set
            {
                latency = value;
            }
        }

        public string SourceLayer
        {
            get
            {
                return eventList[0].Layer;
            }
        }

        public double StartTime
        {
            get
            {
                return eventList[0].HappendTime;
            }
        }

        public double EndTime
        {
            get
            {
                return (eventList.Last()).HappendTime;
            }
        }

        #endregion Derived Data
    }

    public class PacketFilterOption
    {
        public string Type = "";
        public string MaxSize = "";
        public string MinSize = "";
        public string SourceGroup = "";
        public string DestinationGroup = "";
        public string Layer = "";
        public string StartTime = "";
        public string EndTime = "";
        public int Offset = 0;
        public int Limit = 50;
    }
}