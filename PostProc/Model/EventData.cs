﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostProc.Model
{
    public class EventData
    {
        public int Id { get; set; }

        public enum EventType { SEND, RECEIVE, FORWARD, DROP, ENERGY, UNKNOWN };

        private EventType type;

        public EventType Type
        {
            get { return type; }
            set { type = value; }
        }

        private double happendTime;

        public double HappendTime
        {
            get { return happendTime; }
            set { happendTime = value; }
        }

        private NodeData node;

        public NodeData Node
        {
            get { return node; }
            set { node = value; }
        }

        private NodeData previousNode;

        public NodeData PreviousNode
        {
            get { return previousNode; }
            set { previousNode = value; }
        }

        private PacketData packet;

        public PacketData Packet
        {
            get { return packet; }
            set { packet = value; }
        }

        private string layer;

        public string Layer
        {
            get { return layer; }
            set { layer = value; }
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private List<string> headers;

    }
}