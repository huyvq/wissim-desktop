﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using WisSim.NetworkComponents;

namespace WisSim.Model
{
    /// <summary>
    /// Interaction logic for Node.xaml
    /// <author>Trong Nguyen</author>
    /// </summary>
    public partial class Node : UserControl, INode, IXmlSerializable
    {
        public Node(Network network)
        {
            InitializeComponent();
            this.Network = network;

            NeighborList = new List<INode>();
            Edges = new List<UcEdge>();
            Connects = new List<UcConnect>();

            //range
            EllRange.Width = EllRange.Height = Range * 2;
            Canvas.SetLeft(EllRange, -EllRange.Width / 2 + this.Width / 2);
            Canvas.SetTop(EllRange, -EllRange.Height / 2 + this.Height / 2);

            States = NodeStates.Normal;
        }

        //Only use for Visualizer
        public Node()
        {
            InitializeComponent();
            this.EllNode.MouseEnter -= EllNode_MouseEnter;
            this.EllNode.MouseLeave -= EllNode_MouseLeave;
            this.EllNode.MouseRightButtonDown -= MouseRightDown;
            this.EllNode.MouseLeftButtonDown -= MouseLeftDown;
            States = NodeStates.Normal;
        }

        private IInputElement parrent;

        private NodeStates states;

        public NodeStates States
        {
            get { return states; }
            set
            {
                if (states != value)
                {
                    states = value;
                    switch (value)
                    {
                        case NodeStates.Stuck:
                            this.EllNode.Visibility = Visibility.Visible;
                            this.EllNode.Fill = new SolidColorBrush(Colors.Red);
                            break;

                        case NodeStates.Dead:
                            //this.EllNode.Fill = new SolidColorBrush(Colors.Red);
                            this.EllNode.Visibility = Visibility.Collapsed;
                            break;

                        case NodeStates.Source:
                            this.EllNode.Visibility = Visibility.Visible;
                            this.EllNode.Fill = new SolidColorBrush(Colors.Red);
                            break;

                        case NodeStates.Dest:
                            this.EllNode.Visibility = Visibility.Visible;
                            this.EllNode.Fill = new SolidColorBrush(Colors.Blue);
                            break;

                        case NodeStates.Drop:
                            this.EllNode.Visibility = Visibility.Visible;
                            this.EllNode.Fill = new SolidColorBrush(Colors.DarkBlue);
                            break;

                        case NodeStates.Alarm:
                            this.EllNode.Visibility = Visibility.Visible;
                            this.EllNode.Fill = new SolidColorBrush(Colors.Blue);
                            break;

                        case NodeStates.Normal:
                        default:
                            this.EllNode.Visibility = Visibility.Visible;
                            this.EllNode.Fill = new SolidColorBrush(Colors.DarkGray);
                            break;
                    }
                }
            }
        }

        [XmlAttribute]
        public int ID
        {
            get
            {
                return Network.NodeList.IndexOf(this);
            }
        }

        [XmlAttribute]
        public double X
        {
            get
            {
                return InkCanvas.GetLeft(this) + this.Width / 2;
            }
        }

        [XmlAttribute]
        public double Y
        {
            get
            {
                return InkCanvas.GetTop(this) + this.Height / 2;
            }
        }

        [XmlAttribute]
        public double OffTime { get; set; }

        public int Range
        {
            get
            {
                return Network.Range;
            }
        }

        private Network Network { get; set; }

        public IList<INode> NeighborList { get; private set; }

        public List<UcEdge> Edges { get; private set; }

        public List<UcConnect> Connects { get; private set; }

        public event EventHandler<MouseButtonEventArgs> Connection;

        private void EllNode_MouseEnter(object sender, MouseEventArgs e)
        {
            if (EllRange.Width != Range * 2)
            {
                EllRange.Width = EllRange.Height = Range * 2;
                Canvas.SetLeft(EllRange, -EllRange.Width / 2 + this.Width / 2);
                Canvas.SetTop(EllRange, -EllRange.Height / 2 + this.Height / 2);
            }

            EllRange.Visibility = Visibility.Visible;
            ToolTip = ID + " (" + X + ", " + Y + ")";
        }

        private void EllNode_MouseLeave(object sender, MouseEventArgs e)
        {
            //EllRange.Visibility = Visibility.Hidden;
            EllRange.Visibility = Visibility.Collapsed;
        }

        private void MouseRightDown(object sender, MouseButtonEventArgs e)
        {
            if (Network.CurrentTime == 0)
            {
                Network.RemoveNode(this);
            }
            else
            {
                OffTime = Network.CurrentTime;
                States = NodeStates.Dead;
            }

            e.Handled = true;
        }

        private void MouseLeftDown(object sender, MouseButtonEventArgs e)
        {
            if (Connection != null)
            {
                foreach (UcConnect c in Connects)
                {
                    if (c.SourceNode == this)
                    {
                        //Support only 1 node is start of only 1 connection.
                        return;
                    }
                }
                Connection(this, e);
                e.Handled = true;
            }
        }

        public void AddConnect(UcConnect c)
        {
            Connects.Add(c);
        }

        public void AddEdge(UcEdge e)
        {
            Edges.Add(e);
        }

        public void Resize(int newWidth, int newHeight)
        {
            this.Width = newWidth;
            this.Height = newHeight;
            this.EllNode.Width = newWidth;
            this.EllNode.Height = newHeight;
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("ID", ID.ToString());
            writer.WriteAttributeString("X", X.ToString());
            writer.WriteAttributeString("Y", Y.ToString());
            writer.WriteAttributeString("OffTime", OffTime.ToString());
        }

        #endregion IXmlSerializable Members

        /// <summary>
        /// BUG: NullException at Edges
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EllNode_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            bool visible = (bool)e.NewValue;
            if (Edges != null)
            {
                if (!visible)
                {
                    foreach (UcEdge edge in Edges)
                    {
                        edge.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    foreach (UcEdge edge in Edges)
                    {
                        edge.Visibility = Visibility.Visible;
                    }
                }
            }
        }
    }
}