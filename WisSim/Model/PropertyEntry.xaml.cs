﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WisSim.Model
{
    /// <summary>
    /// Interaction logic for PropertyEntry.xaml
    /// <author>Trong Nguyen</author>
    /// </summary>
    public partial class PropertyEntry : UserControl
    {
        public PropertyEntry()
        {
            InitializeComponent();
        }

        public event EventHandler ValueChanged;

        public static readonly DependencyProperty LabelProperty = DependencyProperty.Register(
            "Label", typeof (string), typeof (PropertyEntry), new PropertyMetadata(default(string)));

        public string Label
        {
            get { return (string) GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value", typeof (string), typeof (PropertyEntry), new PropertyMetadata(default(string)));

        public string Value
        {
            get { return (string) GetValue(ValueProperty); }
            set
            {
                SetValue(ValueProperty, value);
                if (ValueChanged != null) ValueChanged(this, null);
            }
        }

        private void PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Value = ((TextBox) sender).Text;
            }
        }

        private void LostFocus(object sender, RoutedEventArgs e)
        {
            Value = ((TextBox)sender).Text;
        }

    }
}
