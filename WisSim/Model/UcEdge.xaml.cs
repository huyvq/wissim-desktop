﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WisSim.Model
{
    /// <summary>
    /// Interaction logic for Edge.xaml
    /// <author>Trong Nguyen</author>
    /// </summary>
    public partial class UcEdge : UserControl
    {
        public UcEdge(Node node1, Node node2)
        {
            InitializeComponent();

            node1.AddEdge(this);
            node2.AddEdge(this);

            MainCanvas.Children.Add(new Path
                {
                    Data = new LineGeometry(new Point(node1.X, node1.Y), new Point(node2.X, node2.Y)),
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    VerticalAlignment = VerticalAlignment.Stretch,
                    StrokeThickness = 1,
                    Stroke = new SolidColorBrush(Colors.Black)
                });
        }
    }
}
