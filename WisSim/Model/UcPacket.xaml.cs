﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace WisSim.Model
{
    /// <summary>
    ///     Interaction logic for UcPacket.xaml
    /// </summary>
    public partial class UcPacket : UserControl
    {
        public enum UcPacketCreateType
        {
            NORMAL,
            PLAY_ANIMATION
        }

        private const double DELAY_TIME = 0.1f;
        private readonly PacketData packetData;
        private EventData currentEvent;
        private Storyboard storyboard;

        public UcPacket(PacketData packetData, UcPacketCreateType type)
        {
            InitializeComponent();

            this.packetData = packetData;

            if (packetData != null)
            {
                var tooltip = "Packet: " + packetData.Id + "\n";
                tooltip += "Type: " + packetData.PacketType + "\n";

                //2. Add paths and drop-nodes if it has.
                foreach (var e in packetData.EventList)
                {
                    if (e.Type == EventData.EventType.RECEIVE && e.Node != packetData.SourceNode)
                    {
                        if (e.PreviousNode != null)
                        {
                            var newEdge = new Path
                            {
                                Data =
                                    new LineGeometry(new Point(e.Node.X, e.Node.Y),
                                        new Point(e.PreviousNode.X, e.PreviousNode.Y)),
                                HorizontalAlignment = HorizontalAlignment.Stretch,
                                VerticalAlignment = VerticalAlignment.Stretch,
                                StrokeThickness = 2,
                                Stroke = new SolidColorBrush(Colors.Red)
                            };
                            newEdge.Name = "PE" + packetData.Id + "_" + e.Layer + "_" + e.PreviousNode.ID + "_" +
                                           e.Node.ID + "_" + e.Id;
                            mainCanvas.Children.Add(newEdge);

                            ////Edges animation
                            RegisterName(newEdge.Name, newEdge);
                            //DoubleAnimation animation = new DoubleAnimation();
                            //animation.From = 0.0;
                            //animation.To = 1.0;
                            //animation.Duration = new Duration(TimeSpan.FromSeconds(DELAY_TIME));
                            //interval += 1;
                            //animation.BeginTime = TimeSpan.FromSeconds(interval*DELAY_TIME);

                            //storyboard.Children.Add(animation);
                            //Storyboard.SetTargetName(animation, newEdge.Name);
                            //Storyboard.SetTargetProperty(animation, new PropertyPath(Rectangle.OpacityProperty));
                        }
                    }

                    if (e.Type == EventData.EventType.DROP && e.Node != packetData.SourceNode)
                    {
                        if (e.Node != null)
                        {
                            var dropNode = new Node();
                            dropNode.Name = "PN" + packetData.Id + "_" + e.Layer + "_" + e.Node.ID + "_" + e.Id;
                            dropNode.States = NodeStates.Drop;
                            dropNode.Resize(13, 13);
                            dropNode.ToolTip = "Packet dropped at node " + e.Node.ID;
                            if (e.PreviousNode != null)
                            {
                                dropNode.ToolTip += "\nFrom node: " + e.PreviousNode.ID;
                            }
                            dropNode.ToolTip += "\nMessage: " + e.Message;
                            mainCanvas.Children.Add(dropNode);
                            Canvas.SetLeft(dropNode, e.Node.X - dropNode.Width/2);
                            Canvas.SetTop(dropNode, e.Node.Y - dropNode.Height/2);

                            ////Drop node animation
                            RegisterName(dropNode.Name, dropNode);
                            //DoubleAnimation animation = new DoubleAnimation();
                            //animation.From = 0.0;
                            //animation.To = 1.0;
                            //animation.Duration = new Duration(TimeSpan.FromSeconds(DELAY_TIME));
                            //interval += 1;
                            //animation.BeginTime = TimeSpan.FromSeconds(interval * DELAY_TIME);

                            //storyboard.Children.Add(animation);
                            //Storyboard.SetTargetName(animation, dropNode.Name);
                            //Storyboard.SetTargetProperty(animation, new PropertyPath(Rectangle.OpacityProperty));
                        }
                    }
                }

                //1. Add source node
                if (packetData.SourceNode != null)
                {
                    var sourceNode = new Node();
                    sourceNode.Name = "PS" + packetData.Id + "_" + packetData.SourceNode.ID;
                    sourceNode.States = NodeStates.Source;
                    sourceNode.Resize(13, 13);
                    tooltip += "Source: " + packetData.SourceNode.ID + "\n";

                    Canvas.SetLeft(sourceNode, packetData.SourceNode.X - sourceNode.Width/2);
                    Canvas.SetTop(sourceNode, packetData.SourceNode.Y - sourceNode.Height/2);
                    mainCanvas.Children.Add(sourceNode);

                    ////Source node animation
                    RegisterName(sourceNode.Name, sourceNode);
                    //DoubleAnimation animation = new DoubleAnimation();
                    //animation.From = 0.0;
                    //animation.To = 1.0;
                    //animation.Duration = new Duration(TimeSpan.FromSeconds(DELAY_TIME));

                    //storyboard.Children.Add(animation);
                    //Storyboard.SetTargetName(animation, sourceNode.Name);
                    //Storyboard.SetTargetProperty(animation, new PropertyPath(Rectangle.OpacityProperty));
                }

                //3.Add Destination node
                if (packetData.DestinationNode != null)
                {
                    var destNode = new Node();
                    destNode.Name = "PD" + packetData.Id + "_" + packetData.DestinationNode.ID;
                    destNode.States = NodeStates.Dest;
                    destNode.Resize(13, 13);
                    tooltip += "Destination: " + packetData.DestinationNode.ID + "\n";
                    mainCanvas.Children.Add(destNode);
                    Canvas.SetLeft(destNode, packetData.DestinationNode.X - destNode.Width/2);
                    Canvas.SetTop(destNode, packetData.DestinationNode.Y - destNode.Height/2);

                    ////Destination node animation
                    RegisterName(destNode.Name, destNode);
                    //DoubleAnimation animation = new DoubleAnimation();
                    //animation.From = 0.0;
                    //animation.To = 1.0;
                    //animation.Duration = new Duration(TimeSpan.FromSeconds(DELAY_TIME));

                    //storyboard.Children.Add(animation);
                    //Storyboard.SetTargetName(animation, destNode.Name);
                    //Storyboard.SetTargetProperty(animation, new PropertyPath(Rectangle.OpacityProperty));
                }

                if (packetData.IsMultiCast == false)
                {
                    tooltip += "Hop count: " + packetData.HopCount + " hops\n";
                    tooltip += "Latency: " + packetData.EndTime.ToString("0.000") + " - ";
                    tooltip += packetData.StartTime.ToString("0.000") + " = ";
                    tooltip += packetData.Latency.ToString("0.000") + " s";
                }
                else
                {
                    tooltip += "Multicast packet: ";
                    tooltip += "\n     + Hop count: N/A";
                    tooltip += "\n     + Latency: N/A";
                }
                ToolTip = tooltip;

                Loaded += (o, a) =>
                {
                    if (type == UcPacketCreateType.PLAY_ANIMATION)
                    {
                        foreach (FrameworkElement ele in mainCanvas.Children)
                        {
                            ele.Opacity = 0;
                        }
                    }
                };
            }
        }

        public void ToggleControl()
        {
            if (Visibility == Visibility.Collapsed)
            {
                //Show packet
                Visibility = Visibility.Visible;
            }
            else if (Visibility == Visibility.Visible)
            {
                //hide packet
                Visibility = Visibility.Collapsed;
            }
        }

        public bool GenerateEventAnimation(EventData evt, double speed)
        {
            storyboard = new Storyboard();
            //storyboard.SetSpeedRatio(speed);
            var duration = DELAY_TIME*speed;

            storyboard.Completed += storyboard_Completed;
            currentEvent = evt;

            var flag = false;

            // 1. if this is source node
            if (packetData.SourceNode != null && evt.Node == packetData.SourceNode)
            {
                var elementName = "PS" + packetData.Id + "_" + packetData.SourceNode.ID;
                var sourceNode = (Node) mainCanvas.FindName(elementName);
                var animation = new DoubleAnimation
                {
                    From = 0.0,
                    To = 1.0,
                    Duration = new Duration(TimeSpan.FromSeconds(duration))
                };

                storyboard.Children.Add(animation);
                if (sourceNode != null)
                    Storyboard.SetTargetName(animation, sourceNode.Name);
                Storyboard.SetTargetProperty(animation, new PropertyPath(OpacityProperty));
                flag = true;
            }

            // 2. if this is destination node
            if (packetData.DestinationNode != null && evt.Node == packetData.DestinationNode)
            {
                var elementName = "PD" + packetData.Id + "_" + packetData.DestinationNode.ID;
                var destNode = (Node) mainCanvas.FindName(elementName);
                var animation = new DoubleAnimation
                {
                    From = 0.0,
                    To = 1.0,
                    Duration = new Duration(TimeSpan.FromSeconds(duration))
                };

                storyboard.Children.Add(animation);
                Storyboard.SetTargetName(animation, destNode.Name);
                Storyboard.SetTargetProperty(animation, new PropertyPath(OpacityProperty));
                flag = true;
            }

            // 3. if this is sub event (RECEIVE || DROP)
            if (evt.Type == EventData.EventType.RECEIVE
                && evt.Layer == "RTR"
                && evt.Node != packetData.SourceNode)
            {
                if (evt.PreviousNode != null)
                {
                    var elementName = "PE" + packetData.Id + "_" + evt.Layer + "_" + evt.PreviousNode.ID + "_" +
                                      evt.Node.ID + "_" + evt.Id;
                    var newEdge = (Path) mainCanvas.FindName(elementName);
                    var animation = new DoubleAnimation();
                    animation.From = 0.0;
                    animation.To = 1.0;
                    animation.Duration = new Duration(TimeSpan.FromSeconds(duration));
                    animation.BeginTime = TimeSpan.FromSeconds(duration);
                    storyboard.Children.Add(animation);
                    Storyboard.SetTargetName(animation, newEdge.Name);
                    Storyboard.SetTargetProperty(animation, new PropertyPath(OpacityProperty));
                    flag = true;
                }
            }

            if (evt.Type == EventData.EventType.DROP && evt.Node != packetData.SourceNode)
            {
                if (evt.Node != null)
                {
                    var elementName = "PN" + packetData.Id + "_" + evt.Layer + "_" + evt.Node.ID + "_" + evt.Id;
                    var dropNode = (Node) mainCanvas.FindName(elementName);
                    var animation = new DoubleAnimation();
                    animation.From = 0.0;
                    animation.To = 1.0;
                    animation.Duration = new Duration(TimeSpan.FromSeconds(duration));
                    animation.BeginTime = TimeSpan.FromSeconds(duration);

                    storyboard.Children.Add(animation);
                    Storyboard.SetTargetName(animation, dropNode.Name);
                    Storyboard.SetTargetProperty(animation, new PropertyPath(OpacityProperty));
                    flag = true;
                }
            }

            return flag;
        }

        public void PlayAnimation()
        {
            storyboard.Begin(this);
        }

        private void storyboard_Completed(object sender, EventArgs e)
        {
            storyboard.Stop();
            if (OnAnimationCompleted != null)
                OnAnimationCompleted(currentEvent, packetData);
        }

        #region Storyboard Completed Event

        public delegate void AnimationCompleted(EventData e, PacketData packet);

        public event AnimationCompleted OnAnimationCompleted;

        #endregion Storyboard Completed Event
    }
}