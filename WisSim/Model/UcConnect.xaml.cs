﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WisSim.Model
{
    /// <summary>
    /// Interaction logic for UcConnect.xaml
    /// <author>Trong Nguyen</author>
    /// </summary>
    public partial class UcConnect : UserControl
    {
        public Node SourceNode, DestNode;

        public EventHandler<EventArgs> Remove;

        public UcConnect(Node sourceNode, Node destNode)
        {
            InitializeComponent();

            this.SourceNode = sourceNode;
            this.DestNode = destNode;

            sourceNode.AddConnect(this);
            destNode.AddConnect(this);

            MainCanvas.Children.Add(new Path
            {
                Data = new LineGeometry(new Point(sourceNode.X, sourceNode.Y), new Point(destNode.X, destNode.Y)),
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                StrokeThickness = 2,
                Stroke = new SolidColorBrush(Colors.Blue)
            });

            Arrow.Margin = new Thickness(destNode.X - Arrow.Width / 2, destNode.Y - Arrow.Height, 0, 0);

            var angle = Math.Atan2(sourceNode.Y - destNode.Y, sourceNode.X - destNode.X) * 180 / Math.PI;
            
            Arrow.RenderTransform = new RotateTransform(angle + 90);
        }

        private void MouseRightUp(object sender, MouseButtonEventArgs e)
        {
            if (Remove != null)
            {
                Remove(this, e);
                e.Handled = true;
            }            
        }
    }
}
