﻿using HoleGenerator.Model;
using Ookii.Dialogs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using WisSim.GraphAlgorithms.Voronoi;
using WisSim.GraphAlgorithms.Voronoi.Views;
using WisSim.NetworkComponents;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;

namespace WisSim.Model
{
    /// <summary>
    /// Network - Contain all other components
    /// <author>Trong Nguyen</author>
    /// </summary>
    public partial class Network : INetwork, IXmlSerializable, INotifyPropertyChanged
    {
        // simulation Time
        [XmlAttribute]
        public double SimulationTime { get; set; }

        public IList NodeList
        {
            get
            {
                return NodeCanvas.Children;
            }
        }

        [XmlIgnore]
        public bool HasEdges
        {
            get
            {
                if (EdgeCanvas.Children.Count > 0)
                {
                    return true;
                }
                return false;
            }
        }

        [XmlIgnore]
        public IList Connects
        {
            get
            {
                return ConnectCanvas.Children;
            }
        }

        [XmlIgnore]
        public IList VisualizeHoles
        {
            get
            {
                return VisualizeHoleCanvas.Children;
            }
        }

        [XmlAttribute]
        public int IfqLen { get; set; }

        [XmlAttribute]
        public double DumpAt { get; set; }

        public Network()
        {
            InitializeComponent();
            SimulationTime = 500;   // default simulation time
            IfqLen = 50;            // default Ifqlen
            DumpAt = 90.0;

            NodeCanvas.SelectionMoved += MoveNodes;
            NodeCanvas.PreviewKeyDown += RemoveNodes;
            NodeCanvas.PreviewKeyDown += AddGroup;
            NodeCanvas.KeyUp += NodeCanvas_KeyUp;

            GroupsList = new Dictionary<string, List<Node>>();
        }

        private double currentTime;

        [XmlIgnore]
        public double CurrentTime
        {
            get { return currentTime; }
            set
            {
                if (value != currentTime)
                {
                    currentTime = value;
                    foreach (Node node in NodeList)
                    {
                        if (node.OffTime > 0 && node.OffTime <= value)
                            node.States = NodeStates.Dead;
                        else
                            node.States = NodeStates.Normal;
                    }
                }
            }
        }

        public static readonly DependencyProperty RangeProperty = DependencyProperty.Register(
            "Range", typeof(int), typeof(Network),
            new PropertyMetadata(40, (sender, e) => { ((Network)sender).UpdateNeighbors(); }));

        [XmlAttribute]
        public int Range
        {
            get { return (int)GetValue(RangeProperty); }
            set { SetValue(RangeProperty, value); }
        }

        [XmlIgnore]
        public bool IsSelectHoles
        {
            get { return HoleCanvas.Enable; }
            set { HoleCanvas.Enable = value; }
        }

        public Dictionary<string, List<Node>> GroupsList { get; private set; }

        #region node

        private void AddNode(object sender, MouseButtonEventArgs e)
        {
            if (startNode == null)
            {
                AddNode((int)e.MouseDevice.GetPosition(Main_Canvas).X, (int)e.MouseDevice.GetPosition(Main_Canvas).Y);

                OnPropertyChanged(() => NodeList);
                //update(this, null);
            }
            else
            {
                if (startNode.OffTime > 0 && startNode.OffTime < CurrentTime)
                {
                    startNode.States = NodeStates.Dead;
                }
                else
                {
                    startNode.States = NodeStates.Normal;
                }
                startNode = null;
            }
            e.Handled = true;
        }

        private void AddNode(double x, double y)
        {
            var node = new Node(this);
            node.Connection += AddConnect;
            NodeCanvas.Children.Add(node);
            InkCanvas.SetLeft(node, x - node.Width / 2);
            InkCanvas.SetTop(node, y - node.Height / 2);

            // update neighbor
            UpdateNeighbors(node);

            // check edge
            foreach (var item in node.NeighborList)
            {
                AddEdge(node, item as Node);
            }
        }

        public void AddRandomNodes(int numNode, double rand = 0)
        {
            rand = rand / 100;
            var r = new Random();
            var t = Math.Sqrt(numNode / (Width * Height));
            var row = (int)(Height * t);      // number of rows
            var col = (int)(Width * t);       // number of columns;
            double lrow = Height / row;
            double lcol = Width / col;

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    var node = new Node(this);
                    node.Connection += AddConnect;
                    NodeCanvas.Children.Add(node);
                    InkCanvas.SetTop(node, (int)(lrow / 2 + i * lrow + (rand - 2 * rand * r.NextDouble()) * lrow) - node.Height / 2);
                    InkCanvas.SetLeft(node, (int)(lcol / 2 + j * lcol + (rand - 2 * rand * r.NextDouble()) * lcol) - node.Width / 2);
                }
            }

            for (int i = numNode - row * col; i > 0; i--)
            {
                var node = new Node(this);
                node.Connection += AddConnect;
                NodeCanvas.Children.Add(node);
                InkCanvas.SetLeft(node, (int)(r.NextDouble() * Width) - node.Width / 2);
                InkCanvas.SetTop(node, (int)(r.NextDouble() * Height) - node.Height / 2);
            }

            UpdateNeighbors();

            // reset the edge
            EdgeCanvas.Children.Clear();
            foreach (Node item in NodeList)
            {
                foreach (var neigh in item.NeighborList.Cast<Node>())
                {
                    if (item.ID < neigh.ID)
                    {
                        AddEdge(item, neigh);
                    }
                }
            }

            OnPropertyChanged(() => NodeList);
            //Update(this, null);
        }

        private void MoveNodes(object sender, EventArgs e)
        {
            foreach (var node in NodeCanvas.GetSelectedElements().Cast<Node>())
            {
                // remove old edge
                foreach (var ed in node.Edges)
                {
                    EdgeCanvas.Children.Remove(ed);
                }

                foreach (var c in node.Connects)
                {
                    ConnectCanvas.Children.Remove(c);
                }

                foreach (var item in node.NeighborList)
                {
                    item.NeighborList.Remove(node);
                }
                node.NeighborList.Clear();

                // add new edge
                UpdateNeighbors(node);

                // update edge
                foreach (var item in node.NeighborList)
                {
                    AddEdge(node, item as Node);
                }
            }
        }

        private void RemoveNodes_Click(object sender, RoutedEventArgs e)
        {
            RemoveSelectedNodes();
        }

        private void RemoveSelectedNodes()
        {
            var removedNodes = new List<Node>();
            foreach (var node in NodeCanvas.GetSelectedElements().Cast<Node>())
            {
                removedNodes.Add(node);

                //Remove edges
                foreach (var ed in node.Edges)
                {
                    EdgeCanvas.Children.Remove(ed);
                }

                //Remove connects
                foreach (var c in node.Connects)
                {
                    ConnectCanvas.Children.Remove(c);
                }

                //Remove in neighbors object
                foreach (var item in node.NeighborList)
                {
                    item.NeighborList.Remove(node);
                }

                //Remove in groups and update
                var keys = GroupsList.Keys.ToList();
                foreach (var groupName in keys)
                {
                    if (GroupsList[groupName].Contains(node))
                    {
                        GroupsList[groupName].Remove(node);
                    }

                    if (GroupsList[groupName].Count <= 0)
                    {
                        MessageBox.Show("Remove group '" + groupName + "' because it becomes empty while remove nodes");
                        GroupsList.Remove(groupName);
                    }
                }
            }

            //Remove nodes
            try
            {
                foreach (var node in removedNodes)
                {
                    NodeCanvas.Children.Remove(node);
                }
            }
            catch (Exception)
            {
                //Do nothing.
            }

            if (GroupChanged != null)
            {
                GroupChanged(this, new NodesGroupsChangedEventArgs(GroupsList));
            }
        }

        private void RemoveNodes(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                RemoveSelectedNodes();
            }
        }

        private void NodeCanvas_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                OnPropertyChanged(() => NodeList);
            }
        }

        public void RemoveNode(Node node)
        {
            // remove label
            NodeCanvas.Children.Remove(node);

            foreach (var e in node.Edges)
            {
                EdgeCanvas.Children.Remove(e);
            }

            // remove connect
            foreach (var c in node.Connects)
            {
                ConnectCanvas.Children.Remove(c);
            }

            // Remove neighbor relationship when a node n is removed from the Network
            foreach (var item in node.NeighborList)
            {
                item.NeighborList.Remove(node);
            }

            //Remove in groups and update
            var keys = GroupsList.Keys.ToList();
            foreach (var groupName in keys)
            {
                if (GroupsList[groupName].Contains(node))
                {
                    GroupsList[groupName].Remove(node);
                }

                if (GroupsList[groupName].Count <= 0)
                {
                    MessageBox.Show("Remove group '" + groupName + "' because it becomes empty while remove nodes");
                    GroupsList.Remove(groupName);
                }
            }
            if (GroupChanged != null)
            {
                GroupChanged(this, new NodesGroupsChangedEventArgs(GroupsList));
            }

            OnPropertyChanged(() => NodeList);
        }

        #endregion node

        #region connect

        private void AddConnect(Node s, Node d)
        {
            if (s == null || d == null || s == d) return;

            var connect = new UcConnect(s, d);
            connect.Remove += RemoveConnect;
            //pvhau: check and update connect (because wsn algorithm use unicast connection)
            //var checkU = from c in ConnectCanvas.Children.OfType<UcConnect>()
            //             where c.SourceNode.ID == connect.SourceNode.ID
            //             select c;
            //if (checkU.Count<UcConnect> == 0)
            ConnectCanvas.Children.Add(connect);
            s.States = NodeStates.Source;
            d.States = NodeStates.Dest;

            //NguyenTT: Add node to start and end Group
            if (GroupsList == null) GroupsList = new Dictionary<string, List<Node>>();
            if (!GroupsList.Keys.Contains("source"))
            {
                AddGroup("source", new List<Node>());
            }
            if (!GroupsList["source"].Contains(s)) GroupsList["source"].Add(s);

            if (!GroupsList.Keys.Contains("destination"))
            {
                AddGroup("destination", new List<Node>());
            }
            if (!GroupsList["destination"].Contains(d)) GroupsList["destination"].Add(d);
        }

        private void RemoveConnect(object sender, EventArgs e)
        {
            var c = sender as UcConnect;
            if (c == null) return;

            ConnectCanvas.Children.Remove(c);
            c.SourceNode.Connects.Remove(c);
            c.DestNode.Connects.Remove(c);

            ////Remove node from group.
            var removeSource = true;
            var removeDestination = true;
            foreach (UcConnect connect in ConnectCanvas.Children)
            {
                if (connect.SourceNode == c.SourceNode)
                {
                    removeSource = false;
                }

                if (connect.DestNode == c.DestNode)
                {
                    removeDestination = false;
                }
            }
            if (removeSource) GroupsList["source"].Remove(c.SourceNode);
            if (removeDestination) GroupsList["destination"].Remove(c.DestNode);
        }

        private Node startNode;

        private void AddConnect(object sender, MouseButtonEventArgs e)
        {
            if (startNode == null)
            {
                startNode = sender as Node;
                startNode.States = NodeStates.Source;
            }
            else
            {
                if (startNode != sender)
                {
                    AddConnect(startNode, sender as Node);
                }

                if (startNode.OffTime > 0 && startNode.OffTime < CurrentTime)
                {
                    startNode.States = NodeStates.Dead;
                }
                else
                {
                    //startNode.States = NodeStates.Normal;
                }
                startNode = null;
            }
        }

        public void AddConnection(string sourceGroup, string destGroup, int num)
        {
            var rand = new Random();

            for (var i = 0; i < num; i++)
            {
                Node s;
                if (GroupsList.ContainsKey("source"))
                {
                    s = GroupsList[sourceGroup].Find(e => !GroupsList["source"].Contains(e));
                    if (s == null)
                    {
                        MessageBox.Show("Generated " + i + " / " + num + " connections.", "Source nodes are not available enough");
                        return;
                    }
                }
                else
                {
                    s = GroupsList[sourceGroup][rand.Next(GroupsList[sourceGroup].Count)];
                }

                Node d = GroupsList[destGroup][rand.Next(GroupsList[destGroup].Count)];
                AddConnect(s, d);
            }
        }

        public void AddConnection(int sourceDist, int destDist, int sourceMargin, int destMargin)
        {
            Node s = null;
            for (int i = (int)(Width - sourceMargin); i > sourceMargin; i -= sourceDist)
            {
                s = FindNode(i, sourceMargin);
                if (s != null)
                {
                    for (int j = (int)(Width - destMargin); j > destMargin; j -= destDist)
                    {
                        AddConnect(s, FindNode(j, destMargin, false));
                        s = FindNode(i, sourceMargin);
                        AddConnect(s, FindNode(j, (int)(Height - destMargin), false));
                        s = FindNode(i, sourceMargin);
                    }

                    for (int j = (int)(Height - destMargin); j > destMargin; j -= destDist)
                    {
                        AddConnect(s, FindNode(destMargin, j, false));
                        s = FindNode(i, sourceMargin);
                        AddConnect(s, FindNode((int)(Width - destMargin), j, false));
                        s = FindNode(i, sourceMargin);
                    }
                }

                s = FindNode(i, (int)(Height - sourceMargin));
                if (s != null)
                {
                    for (int j = (int)(Width - destMargin); j > destMargin; j -= destDist)
                    {
                        AddConnect(s, FindNode(j, destMargin, false));
                        s = FindNode(i, (int)(Height - sourceMargin));
                        AddConnect(s, FindNode(j, (int)(Height - destMargin), false));
                        s = FindNode(i, (int)(Height - sourceMargin));
                    }

                    for (int j = (int)(Height - destMargin); j > destMargin; j -= destDist)
                    {
                        AddConnect(s, FindNode(destMargin, j, false));
                        s = FindNode(i, (int)(Height - sourceMargin));
                        AddConnect(s, FindNode((int)(Width - destMargin), j, false));
                        s = FindNode(i, (int)(Height - sourceMargin));
                    }
                }
            }

            for (int i = (int)(Height - sourceMargin); i > sourceMargin; i -= sourceDist)
            {
                s = FindNode(sourceMargin, i);
                if (s != null)
                {
                    for (int j = (int)(Width - destMargin); j > destMargin; j -= destDist)
                    {
                        AddConnect(s, FindNode(j, destMargin, false));
                        s = FindNode(sourceMargin, i);
                        AddConnect(s, FindNode(j, (int)(Height - destMargin), false));
                        s = FindNode(sourceMargin, i);
                    }

                    for (int j = (int)(Height - destMargin); j > destMargin; j -= destDist)
                    {
                        AddConnect(s, FindNode(destMargin, j, false));
                        s = FindNode(sourceMargin, i);
                        AddConnect(s, FindNode((int)(Width - destMargin), j, false));
                        s = FindNode(sourceMargin, i);
                    }
                }

                s = FindNode((int)(Width - sourceMargin), i);
                if (s != null)
                {
                    for (int j = (int)(Width - destMargin); j > destMargin; j -= destDist)
                    {
                        AddConnect(s, FindNode(j, destMargin, false));
                        s = FindNode((int)(Width - sourceMargin), i);
                        AddConnect(s, FindNode(j, (int)(Height - destMargin), false));
                        s = FindNode((int)(Width - sourceMargin), i);
                    }

                    for (int j = (int)(Height - destMargin); j > destMargin; j -= destDist)
                    {
                        AddConnect(s, FindNode(destMargin, j, false));
                        s = FindNode((int)(Width - sourceMargin), i);
                        AddConnect(s, FindNode((int)(Width - destMargin), j, false));
                        s = FindNode((int)(Width - sourceMargin), i);
                    }
                }
            }
        }

        private Node FindNode(int x, int y, bool checkSource = true)
        {
            double m = Double.MaxValue;
            Node n = null;
            foreach (Node node in NodeList)
            {
                if (checkSource && GroupsList.Keys.Contains("source") && GroupsList["source"].Contains(node)) continue;

                double d = (node.X - x) * (node.X - x) + (node.Y - y) * (node.Y - y);
                if (d < m)
                {
                    m = d;
                    n = node;
                }
            }
            return n;
        }

        #endregion connect

        #region edge

        private void UpdateNeighbors()
        {
            // remove all edge
            EdgeCanvas.Children.Clear();

            // remove all neighbors lists of nodes in the Network
            foreach (Node item in NodeList)
                item.NeighborList.Clear();

            for (int i = 0; i < NodeList.Count; i++)
            {
                for (int j = i + 1; j < NodeList.Count; j++)
                {
                    Node nodei = (Node)NodeList[i];
                    Node nodej = (Node)NodeList[j];

                    double dist2 = (nodei.X - nodej.X) * (nodei.X - nodej.X) + (nodei.Y - nodej.Y) * (nodei.Y - nodej.Y);
                    if (dist2 <= Range * Range)
                    {
                        nodej.NeighborList.Add(nodei);
                        nodei.NeighborList.Add(nodej);

                        AddEdge(nodej, nodei);
                    }
                }
            }
        }

        private void UpdateNeighbors(Node node)
        {
            // first remove this node in neighbor lists of other nodes
            // because the neighbor list of each node usually has small size, this would ensure low cost
            foreach (var item in node.NeighborList)
                item.NeighborList.Remove(node);
            node.NeighborList.Clear();

            foreach (var edge in node.Edges)
                EdgeCanvas.Children.Remove(edge);

            // now it looks like we remove mNode, and add it again at other position
            // call the UpdateNeighbors() to update (O^(n))
            foreach (var item in NodeList)
            {
                Node n = (Node)item;
                if (node.ID != n.ID)
                {
                    double dist2 = (node.X - n.X) * (node.X - n.X) + (node.Y - n.Y) * (node.Y - n.Y);

                    if (dist2 <= Range * Range)
                    {
                        n.NeighborList.Add(node);
                        node.NeighborList.Add(n);

                        AddEdge(node, n);
                    }
                }
            }
        }

        private void AddEdge(Node node1, Node node2)
        {
            string edgeName = "E";
            if (node1.ID <= node2.ID)
            {
                edgeName += node1.ID + "_" + node2.ID;
            }
            else
            {
                edgeName += node2.ID + "_" + node1.ID;
            }
            var edge = new UcEdge(node1, node2) { Name = edgeName };
            EdgeCanvas.Children.Add(edge);
        }

        [XmlIgnore]
        public bool ShowEdges
        {
            get { return EdgeCanvas.Visibility == Visibility.Visible; }
            set { EdgeCanvas.Visibility = value ? Visibility.Visible : Visibility.Hidden; }
        }

        #endregion edge

        #region hole

        private double minRadius = 100;

        public double MinRadius
        {
            get { return minRadius; }
            set { minRadius = value; }
        }

        private double maxRadius = 500;

        public double MaxRadius
        {
            get { return maxRadius; }
            set { maxRadius = value; }
        }

        public void AddHole(object sender, MouseButtonEventArgs e)
        {
            if (MinRadius == 0) MinRadius = 100;
            if (MaxRadius == 0) MaxRadius = 500;
            SingleHole newHole = HoleCanvas.AddHole(
                            (int)e.MouseDevice.GetPosition(Main_Canvas).X, (int)e.MouseDevice.GetPosition(Main_Canvas).Y,
                            MinRadius, MaxRadius, CurrentTime, SimulationTime);

            foreach (Node node in NodeCanvas.Children)
            {
                double offTime = newHole.PointOffTime(new DPoint(node.X, node.Y));
                if (!double.IsNaN(offTime))
                {
                    if (node.OffTime == null || node.OffTime == 0 || node.OffTime == double.NaN || node.OffTime > offTime)
                    {
                        node.OffTime = offTime;
                    }
                }
            }

            e.Handled = true;
        }

        /// <summary>
        /// update node offtime after remove hole in DynamicHole
        /// </summary>
        /// <param name="hole"></param>
        public void RemoveHole(SingleHole hole)
        {
            foreach (Node node in NodeCanvas.Children)
            {
                double offTime = hole.PointOffTime(new DPoint(node.X, node.Y));
                if (!double.IsNaN(offTime))
                {
                    if (node.OffTime == offTime)
                    {
                        // update offtime of node
                        HoleCanvas.OffTime(node.X, node.Y);
                    }
                }
            }
        }

        public void UpdateHoleOfftime(SingleHole hole)
        {
            foreach (Node node in NodeCanvas.Children)
            {
                double offTime = hole.PointOffTime(new DPoint(node.X, node.Y));
                if (!double.IsNaN(offTime))
                {
                    if (node.OffTime == null || node.OffTime == 0 || node.OffTime == double.NaN || node.OffTime > offTime)
                    {
                        node.OffTime = offTime;
                    }
                }
            }
        }

        #endregion hole

        public void Reset()
        {
            NodeCanvas.Children.Clear();
            EdgeCanvas.Children.Clear();
            ConnectCanvas.Children.Clear();
            PacketCanvas.Children.Clear();
            HoleCanvas.ClearHole();
            GraphCanvas.Children.Clear();
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            for (var i = 0; i < NodeList.Count; i++)
            {
                var node = NodeList[i] as Node;
                if (node.X > Width || node.Y > Height)
                {
                    NodeList.Remove(node);
                    i--;
                }
            }
        }

        #region packet

        public UcPacket AddPacketPath(PacketData packet, UcPacket.UcPacketCreateType type)
        {
            string pathName = "P" + packet.Id.ToString();
            var ele = PacketCanvas.Children.OfType<UcPacket>().FirstOrDefault(e => e.Name == pathName);
            if (ele != null)
                return ele;

            var packetPath = new UcPacket(packet, type) { Name = pathName };
            PacketCanvas.Children.Add(packetPath);
            return packetPath;
        }

        public void ClearPacketPaths()
        {
            PacketCanvas.Children.Clear();
        }

        public void RemovePacketPath(int packetId)
        {
            string pathName = "P" + packetId.ToString();

            var ele = PacketCanvas.Children.OfType<UcPacket>().FirstOrDefault(e => e.Name == pathName);
            Debug.WriteLine(ele);
            PacketCanvas.Children.Remove(ele);
        }

        //public void PlayPacketPaths()
        //{
        //    foreach (UcPacket packet in PacketCanvas.Children)
        //    {
        //        packet.Play(0.1);
        //    }
        //}

        #endregion packet

        #region group of visualizer and label of EditorView

        public event EventHandler<NodesGroupEventArgs> GroupDeleted;

        public event EventHandler<NodesGroupEventArgs> GroupAdded;

        public event EventHandler<NodesGroupsChangedEventArgs> GroupChanged;

        private void AddGroup_Click(object sender, RoutedEventArgs e)
        {
            AddGroup();
        }

        private void AddGroup(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                AddGroup();
            }
        }

        private void AddGroup(string groupName, List<Node> groupNodes)
        {
            try
            {
                GroupsList.Add(groupName, groupNodes);

                if (GroupAdded != null)
                {
                    GroupAdded(this, new NodesGroupEventArgs(groupNodes, groupName));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddGroup()
        {
            if (NodeCanvas.EditingMode == InkCanvasEditingMode.Select)
            {
                InputDialog i = new InputDialog
                {
                    MainInstruction = "Group name",
                    WindowTitle = "Group name"
                };
                if (i.ShowDialog() == DialogResult.OK)
                {
                    string groupName = i.Input;
                    int index = 0;
                    while (this.GroupsList.Keys.Contains(groupName))
                    {
                        index++;
                        groupName = i.Input + index.ToString();
                    }
                    if (groupName != i.Input)
                    {
                        MessageBox.Show("Group name '" + i.Input + "' is available  Add new group with name '" + groupName + "'");
                    }

                    var groupNodes = new List<Node>();
                    foreach (Node node in NodeCanvas.GetSelectedElements())
                    {
                        groupNodes.Add(node);
                    }

                    AddGroup(groupName, groupNodes);
                    NodeCanvas.Select(null, null);
                }
            }
        }

        public void SelectGroup(List<Node> group)
        {
            NodeCanvas.Select(group);
        }

        public void SelectGroup(NodesGroup nodeGroup)
        {
            List<Node> group = new List<Node>();
            foreach (INode inode in this.NodeList)
            {
                Node node = (Node)inode;
                int nodeid = Convert.ToInt32(node.Name.Replace("N", ""));
                if (nodeGroup.Nodes.Contains(nodeid))
                {
                    group.Add(node);
                }
            }
            this.SelectGroup(group);
        }

        #endregion group of visualizer and label of EditorView

        #region Special method for Visualizer

        public void SetToViewOnlyMode()
        {
            //TODO: ....
            //this.MouseLeftButtonDown -= AddNode;
            NodeCanvas.MouseLeftButtonDown -= AddNode;
            NodeCanvas.SelectionMoved -= MoveNodes;
            NodeCanvas.PreviewKeyDown -= RemoveNodes;
            NodeCanvas.KeyUp -= NodeCanvas_KeyUp;

            //Main_Canvas.MouseLeftButtonDown -= MainCanvas_MouseLeftButtonDown;
        }

        public Node AddNode(int id, double x, double y)
        {
            var node = new Node(this);
            node.Name = "N" + id.ToString();
            //node.Connection += AddConnect;
            NodeCanvas.Children.Add(node);
            InkCanvas.SetLeft(node, x - node.Width / 2);
            InkCanvas.SetTop(node, y - node.Height / 2);

            return node;
        }

        public Node AddNode(int id, double x, double y, double t)
        {
            Node node = AddNode(id, x, y);
            node.OffTime = t;

            return node;
        }

        public void UpdateNeighbors(int radioRange)
        {
            // remove all edge
            EdgeCanvas.Children.Clear();

            // remove all neighbors lists of nodes in the Network
            foreach (Node item in NodeList)
                item.NeighborList.Clear();

            for (int i = 0; i < NodeList.Count; i++)
            {
                for (int j = i + 1; j < NodeList.Count; j++)
                {
                    Node nodei = (Node)NodeList[i];
                    Node nodej = (Node)NodeList[j];

                    double dist2 = (nodei.X - nodej.X) * (nodei.X - nodej.X) + (nodei.Y - nodej.Y) * (nodei.Y - nodej.Y);
                    if (dist2 <= radioRange * radioRange)
                    {
                        nodej.NeighborList.Add(nodei);
                        nodei.NeighborList.Add(nodej);

                        AddEdge(nodej, nodei);
                    }
                }
            }
        }

        public void AddPolygon(int polygonId, List<NodeData> bound, Color color, string tag)
        {
            List<Node> nodes = new List<Node>();
            for (int i = 0; i < bound.Count; i++)
            {
                NodeData node = bound[i];
                NodeData next = i == bound.Count - 1 ? bound[0] : bound[i + 1];
                Path p = new Path
                {
                    Data = new LineGeometry(new Point(node.X, node.Y), new Point(next.X, next.Y)),
                    StrokeThickness = 2,
                    Stroke = new SolidColorBrush(color)
                };
                p.Tag = tag;

                VisualizeHoles.Add(p);
            }
        }

        public void AddBoundHole(int boundId, List<NodeData> bound)
        {
            AddPolygon(boundId, bound, Colors.Green, "BOUNDHOLE");
        }

        public void AddCircleShape(int boundId, double centerX, double centerY, double radiusX, double radiusY, Color color, string tag)
        {
            Ellipse c = new Ellipse();
            c.Width = radiusX * 2;
            c.Height = radiusY * 2;
            c.StrokeThickness = 2;
            c.Stroke = new SolidColorBrush(color);
            Canvas.SetLeft(c, centerX - radiusX);
            Canvas.SetTop(c, centerY - radiusY);
            c.Tag = tag;

            VisualizeHoles.Add(c);
        }

        public void AddCircleBoundHole(int boundId, double centerX, double centerY, double radiusX, double radiusY)
        {
            AddCircleShape(boundId, centerX, centerY, radiusX, radiusY, Colors.Green, "BOUNDHOLE");
        }

        #endregion Special method for Visualizer

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            SimulationTime = int.Parse(reader["SimulationTime"]);
            Width = int.Parse(reader["Width"]);
            Height = int.Parse(reader["Height"]);
            Range = int.Parse(reader["Range"]);
            IfqLen = int.Parse(reader["IfqLen"]);
            DumpAt = double.Parse(reader["DumpAt"]);

            if (reader.ReadToDescendant("nodes"))
            {
                if (reader.ReadToDescendant("node"))
                {
                    do
                    {
                        var node = new Node(this);
                        node.Connection += AddConnect;
                        NodeCanvas.Children.Add(node);
                        node.OffTime = double.Parse(reader["OffTime"]);
                        InkCanvas.SetLeft(node, double.Parse(reader["X"]) - node.Width / 2);
                        InkCanvas.SetTop(node, double.Parse(reader["Y"]) - node.Height / 2);
                    } while (reader.ReadToNextSibling("node"));

                    UpdateNeighbors();
                }
            }
            //reader.Read(); //Close tag
            //MessageBox.Show("HEHE", "",MessageBoxButton.YesNo);
            if (reader.ReadToNextSibling("connections"))
            {
                if (reader.ReadToDescendant("connection"))
                {
                    do
                    {
                        Node s = (Node)NodeList[int.Parse(reader["Source"])];
                        Node d = (Node)NodeList[int.Parse(reader["Destination"])];
                        var connect = new UcConnect(s, d);
                        connect.Remove += RemoveConnect;
                        ConnectCanvas.Children.Add(connect);
                    } while (reader.ReadToNextSibling("connection"));
                }
            }

            //MessageBox.Show("Group", "", MessageBoxButton.YesNo);
            if (reader.ReadToNextSibling("groups"))
            {
                GroupsList = new Dictionary<string, List<Node>>();
                if (reader.ReadToDescendant("group"))
                {
                    do
                    {
                        List<Node> group = new List<Node>();
                        //if (reader.ReadToDescendant("item"))
                        //{
                        //    do
                        //    {
                        //        int nodeId = int.Parse(reader["ID"]);
                        //        group.Add((Node)NodeList[nodeId]);
                        //    } while (reader.ReadToNextSibling("item"));
                        //}
                        string groupName = reader["name"];
                        string groupString = reader["nodes"];
                        string[] groupNodes = groupString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string nodeId in groupNodes)
                        {
                            group.Add((Node)NodeList[int.Parse(nodeId)]);
                        }
                        GroupsList.Add(groupName, group);
                    } while (reader.ReadToNextSibling("group"));
                }
                //MessageBox.Show("Group", GroupsList.Count.ToString(), MessageBoxButton.YesNo);
                //MessageBox.Show("Group", GroupsList.Count.ToString(), MessageBoxButton.YesNo);
            }

            //MessageBox.Show("finish network", "", MessageBoxButton.YesNo);
            reader.Read();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("SimulationTime", SimulationTime.ToString());
            writer.WriteAttributeString("Width", Width.ToString());
            writer.WriteAttributeString("Height", Height.ToString());
            writer.WriteAttributeString("Range", Range.ToString());
            writer.WriteAttributeString("IfqLen", IfqLen.ToString());
            writer.WriteAttributeString("DumpAt", IfqLen.ToString());
            writer.WriteStartElement("nodes");
            foreach (Node node in NodeList)
            {
                writer.WriteStartElement("node");
                node.WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteStartElement("connections");
            foreach (UcConnect connect in Connects)
            {
                writer.WriteStartElement("connection");
                writer.WriteAttributeString("Source", connect.SourceNode.ID.ToString());
                writer.WriteAttributeString("Destination", connect.DestNode.ID.ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            if (GroupsList != null && GroupsList.Count > 0)
            {
                writer.WriteStartElement("groups");

                foreach (var groupName in GroupsList.Keys)
                {
                    writer.WriteStartElement("group");
                    List<Node> group = GroupsList[groupName];
                    writer.WriteAttributeString("name", groupName);
                    string groupString = "";
                    foreach (var node in group)
                    {
                        groupString = groupString + node.ID + ",";
                        //writer.WriteStartElement("item");
                        //writer.WriteAttributeString("ID", node.ID.ToString());
                        //writer.WriteEndElement();
                    }
                    writer.WriteAttributeString("nodes", groupString);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

        #endregion IXmlSerializable Members

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string property = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void OnPropertyChanged<T>(Expression<Func<T>> propertyLambda)
        {
            string result = GetPropertyName(propertyLambda);
            OnPropertyChanged(result);
        }

        private static string GetPropertyName<T>(Expression<Func<T>> propertyLambda)
        {
            MemberExpression me = propertyLambda.Body as MemberExpression;
            if (me == null)
            {
                throw new ArgumentException(
                    "You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");
            }

            string result = string.Empty;
            do
            {
                result = me.Member.Name + "." + result;
                me = me.Expression as MemberExpression;
            } while (me != null);

            result = result.Remove(result.Length - 1); // remove the trailing "."
            return result;
        }

        #endregion INotifyPropertyChanged Members

        #region animation

        //private int index = 0;
        //private NetworkAnimation networkAnimation = null;

        //public void PlayAnimation(NetworkAnimation na)
        //{
        //    index = 0;
        //    networkAnimation = na;
        //    PlayAnimation();
        //}

        //private void PlayAnimation()
        //{
        //    if (index >= networkAnimation.Events.Count)
        //        return;

        //    var e = networkAnimation.Events[index++];
        //    var ele = AddPacketPath(e.Packet, UcPacket.UcPacketCreateType.PLAY_ANIMATION);

        //    if (ele.GenerateEventAnimation(e) == false)
        //    {
        //        PlayAnimation();
        //        return;
        //    }

        //    ele.OnAnimationCompleted -= OnAnimationCompleted;
        //    ele.OnAnimationCompleted += OnAnimationCompleted;

        //    ele.PlayAnimation();
        //}

        //private void OnAnimationCompleted(EventData e, PacketData packet)
        //{
        //    if (packet.EventList[packet.EventList.Count - 1] == e)
        //    {
        //        RemovePacketPath(packet.Id);
        //    }
        //    PlayAnimation();
        //}

        #endregion animation

        #region Graph Algorithms

        public void GenerateVoronoi()
        {
            GraphCanvas.Children.Clear();
            List<Point> points = new List<Point>();
            foreach (INode n in NodeList)
            {
                points.Add(new Point(n.X, n.Y));
            }

            Voronoi v = new Voronoi(points, SiteColors.Colors,
                new Rect(0, 0, Main_Canvas.ActualWidth, Main_Canvas.ActualHeight));

            foreach (var l in v.VoronoiDiagram())
            {
                GraphCanvas.Children.Add(new VoronoiEdge(l.p0.Value, l.p1.Value));
            }
        }

        public void GenerateDelaunay()
        {
            GraphCanvas.Children.Clear();
            List<Point> points = new List<Point>();
            foreach (INode n in NodeList)
            {
                points.Add(new Point(n.X, n.Y));
            }

            Voronoi v = new Voronoi(points, SiteColors.Colors,
                new Rect(0, 0, Main_Canvas.ActualWidth, Main_Canvas.ActualHeight));

            foreach (var l in v.DelaunayTriangulation())
            {
                GraphCanvas.Children.Add(new VoronoiEdge(l.p0.Value, l.p1.Value));
            }
        }

        public void ClearGraphs()
        {
            GraphCanvas.Children.Clear();
        }

        #endregion Graph Algorithms

        #region Generate random hole

        public void GenerateRandomHole(double area, double r)
        {
            var rand = new Random();
            double unit_height = r;
            double unit_width = r;
            double unit_area = unit_height * unit_width;

            int nrow = (int)(Width / unit_width);
            int ncol = (int)(Height / unit_height);
            int nRemoveUnit = (int)(area / unit_area);

            int startX = nrow / 2;
            int startY = ncol / 2;
            var removedUnits = new Dictionary<Point, Boolean>();

            Point center = new Point(Width / 2, Height / 2);

            for (int i = 0; i < nRemoveUnit; i++)
            {
                removedUnits.Add(new Point(startX, startY), true);
                removeNodesInsideUnit(center.X - unit_width / 2, center.Y - unit_height / 2, center.X + unit_width / 2, center.Y + unit_height / 2);
                while (true)
                {
                    int nextMove = rand.Next(4);
                    switch (nextMove + 1)
                    {
                        case 1:
                            startX--;
                            center.X -= unit_width; break;
                        case 2:
                            startX++;
                            center.X += unit_width; break;
                        case 3:
                            startY--;
                            center.Y -= unit_height; break;
                        case 4:
                            startY++;
                            center.Y += unit_height; break;
                    }
                    bool removed = false;
                    try
                    {
                        removedUnits.TryGetValue(new Point(startX, startY), out removed);
                    }
                    catch (Exception e) { removed = false; }
                    if (!removed)
                    {
                        break;
                    }
                }
            }
        }

        private void removeNodesInsideUnit(double minx, double miny, double maxx, double maxy)
        {
            for (var i = 0; i < NodeList.Count; i++)
            {
                var node = NodeList[i] as Node;
                if ((node.X >= minx && node.X <= maxx) && (node.Y >= miny && node.Y <= maxy))
                {
                    NodeList.Remove(node);
                    i--;
                }
            }
        }

        #endregion Generate random hole
    }
}