﻿using HoleGenerator.Distribution;
using HoleGenerator.Model;
using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace WisSim.Model
{
    /// <summary>
    /// Interaction logic for DynamicHole.xaml
    /// </summary>
    public partial class DynamicHole : UserControl
    {
        private List<SingleHole> holeList;

        public List<SingleHole> HoleList
        {
            get
            {
                if (holeList == null)
                {
                    holeList = new List<SingleHole>();
                }
                return holeList;
            }
        }

        public SingleHole selectedHole;

        public DynamicHole()
        {
            InitializeComponent();

            this.DataContext = this;
            selectedHole = null;
        }

        public bool Enable { get; set; }

        /// <summary>
        /// Call when time is changed
        /// </summary>
        /// <param name="time">new time</param>
        public void TimeChanging(double time)
        {
            // do-nothing
        }

        /// <summary>
        /// Get OffTime of an individual point
        /// </summary>
        /// <param name="x">x possition of point</param>
        /// <param name="y">y possition of point</param>
        /// <returns>time this point is inside the hole</returns>
        public double OffTime(double x, double y)
        {
            ///
            /// return first hole only
            ///
            DPoint point = new DPoint(x, y);
            double minOffTime = double.MaxValue;
            int i = 0;
            foreach (SingleHole hole in HoleList)
            {
                double offtime = hole.PointOffTime(point);
                if (offtime < minOffTime)
                {
                    minOffTime = offtime;
                }
            }
            return minOffTime;
        }

        public SingleHole AddHole(double x, double y, double minR, double maxR, double startTime, double endTime)
        {
            var center = new DPoint(x, y);
            if (minR > maxR) maxR = minR;

            //IRadiusCalculate radiusCalculate = new LinearRadiusByTime(minR, maxR, 5000 - startTime * 10, 1);
            //Distribution distribution = new Distribution6(radiusCalculate);
            SimpleDistribution distributor = new SimpleDistribution();

            //var hole = new Hole(center, distribution, startTime);
            if (endTime < startTime)
            {
                endTime = startTime;
            }
            var hole = new SingleHole(center, startTime, endTime - startTime, maxR, minR, distributor);
            MainCanvas.Children.Add(hole);
            Canvas.SetLeft(hole, x - 10 / 2);
            Canvas.SetTop(hole, y - 10 / 2);
            HoleList.Add(hole);
            return hole;
        }

        public void ClearHole()
        {
            HoleList.Clear();
            MainCanvas.Children.Clear();
        }

        // remove hole
        internal void RemoveHole(SingleHole deletedHole)
        {
            foreach (SingleHole hole in holeList)
            {
                // ensure hole is exist
                if (hole == deletedHole)
                {
                    holeList.Remove(hole);
                    MainCanvas.Children.Remove(deletedHole);
                    ((Parent as Canvas).Parent as Network).RemoveHole(deletedHole);
                    break;
                }
            }
        }

        #region event handler

        private void MainCanvas_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (selectedHole != null)
            {
                double x = e.MouseDevice.GetPosition(selectedHole).X;
                double y = e.MouseDevice.GetPosition(selectedHole).Y;
                double range = Math.Sqrt(x * x + y * y);

                if (selectedHole.State == HoleStates.MinRangeSelecting)
                {
                    selectedHole.MinRange = range;
                }
                else if (selectedHole.State == HoleStates.MaxRangeSelecting)
                {
                    selectedHole.MaxRange = range > selectedHole.MinRange ? range : selectedHole.MinRange;
                }

                e.Handled = true;
            }
        }

        private void MainCanvas_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (selectedHole != null)
            {
                if (selectedHole.State == HoleStates.MinRangeSelecting)
                {
                    selectedHole.State = HoleStates.MaxRangeSelecting;
                }
                else
                {
                    selectedHole.State = HoleStates.Normal;
                    selectedHole.EnableMouseCapture();
                    UpdateHoleOfftime(selectedHole);
                    selectedHole = null;
                }

                e.Handled = true;
            }
        }

        private void MainCanvas_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (selectedHole != null)
            {
                if (selectedHole.State == HoleStates.MinRangeSelecting)
                {
                    selectedHole.MinRange = selectedHole.OldMinRange;
                    selectedHole.State = HoleStates.MaxRangeSelecting;
                }
                else
                {
                    selectedHole.MaxRange = selectedHole.OldMaxRange;
                    selectedHole.State = HoleStates.Normal;
                    selectedHole.EnableMouseCapture();
                    UpdateHoleOfftime(selectedHole);
                    selectedHole = null;
                }

                e.Handled = true;
            }
        }

        private void UpdateHoleOfftime(SingleHole hole)
        {
            ((Parent as Canvas).Parent as Network).UpdateHoleOfftime(hole);
        }

        #endregion event handler
    }
}