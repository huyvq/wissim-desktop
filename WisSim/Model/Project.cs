﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml;
using System.Xml.Serialization;

namespace WisSim.Model
{
    /// <summary>
    /// project contain all network component, setting, section ID
    /// project is saved as wis file
    /// <author>Trong Nguyen</author>
    /// </summary>
    public class Project : INotifyPropertyChanged
    {
        [XmlAttribute]        
        public string Version { get; set; }

        private XmlDocument setting;
        [XmlElement]
        public XmlDocument Setting
        {
            get { return setting; }
            set
            {
                setting = value;
                setting.NodeChanged += (sender, e) => OnPropertyChanged();
            }
        }

        private XmlDocument energy;
        [XmlElement]
        public XmlDocument Energy
        {
            get { return energy; }
            set
            {
                energy = value;
                energy.NodeChanged += (sender, e) => OnPropertyChanged();
            }
        }

        private Network network;
        [XmlElement]        
        public Network Network 
        {
            get { return network; }
            set
            {
                network = value;
                network.PropertyChanged += (sender, e) => OnPropertyChanged();
            } 
        }
        
        private string sectionId ;
        [XmlAttribute]
        public string SectionId
        {
            get { return sectionId; }
            set
            {
                sectionId = value;
                OnPropertyChanged();
            }
        }

        private string name;
        [XmlAttribute]
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }

        private string workingDirectory;
        [XmlAttribute]
        public string WorkingDirectory
        {
            get
            {
                if ((workingDirectory == null) || (!Directory.Exists(workingDirectory)))
                {
                    //TODO: set to default directory (Now it is application CurrentDirectory)
                     workingDirectory = Directory.GetCurrentDirectory();        
                }
                return workingDirectory;
            }
            set
            {
                DirectoryInfo di = new DirectoryInfo(value);                
                if (di.Exists)
                {
                    workingDirectory = value;
                    OnPropertyChanged();
                }
                else
                {
                    System.Windows.MessageBox.Show("Working directory is invalid","Warning");
                    workingDirectory = Directory.GetCurrentDirectory();        
                    //throw new Exception("Working directory is invalid");
                }
            }
        }

        [XmlIgnore]
        public string ProjectNamePath
        {
            get { return System.IO.Path.Combine(WorkingDirectory, Name); }
        }

        [XmlIgnore]
        public bool? IsModify { get; set; }

        public Project()
        {
            Network = new Network();
            Name = "new Project";
            IsModify = null;
            Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public void Save(string path)
        {            
            try
            {
            FileInfo fi = new FileInfo(path);
            
            workingDirectory = fi.DirectoryName;
            Name = fi.Name;

            XmlSerializer sw = new XmlSerializer(typeof(Project));
            using (StreamWriter file = new StreamWriter(path))
            {
                sw.Serialize(file, this);
                file.Close();                
            }

            IsModify = false;

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        internal static Project OpenFile(string path)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Project));
                StreamReader streamReader = new StreamReader(path);
                Project p = (Project)serializer.Deserialize(streamReader);
                streamReader.Close();
                if (p.WorkingDirectory != Path.GetDirectoryName(path))
                {
                    p.WorkingDirectory = Path.GetDirectoryName(path);
                }
                p.IsModify = false;
                return p;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.StackTrace);
                return null;
            }

        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string property = null)
        {
            if (PropertyChanged != null)
            {
                IsModify = true;
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
