﻿using HoleGenerator.DistanceFunction;
using HoleGenerator.Distribution;
using HoleGenerator.Helper;
using HoleGenerator.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WisSim.DynamicHole.DataModel;

namespace WisSim.Model
{
    /// <summary>
    /// Interaction logic for SingleHole.xaml
    /// </summary>
    public partial class SingleHole : UserControl
    {
        #region Properties

        // current time of hole (update by UpdateBoundByTime)
        private double currentTime;

        // bound hole at currentTime
        private List<BoundPoint> _currentBoundPoint = new List<BoundPoint>();

        public List<BoundPoint> CurrentBoundPoint { get { return _currentBoundPoint; } }

        public double LifeTime { get; set; }

        public double StartTime { get; set; }

        private double _deltaTime = 0.1;

        //range
        private double _minRange = 0;

        //update OldMinRange when State change
        public double OldMinRange;

        public double MinRange
        {
            get { return _minRange; }
            set
            {
                if (value != _minRange)
                {
                    _minRange = value;

                    if (EllMinRange != null)
                    {
                        EllMinRange.Width = EllMinRange.Height = MinRange * 2;
                        EllMinRange.ToolTip = "Min:" + MinRange;
                        Canvas.SetLeft(EllMinRange, -EllMinRange.Width / 2 + this.Width / 2);
                        Canvas.SetTop(EllMinRange, -EllMinRange.Height / 2 + this.Height / 2);
                    }
                }
            }
        }

        private double _maxRange = 0;

        //update OldMaxRange when State change
        public double OldMaxRange;

        public double MaxRange
        {
            get { return _maxRange; }
            set
            {
                if (value != _maxRange)
                {
                    _maxRange = value;

                    if (EllMaxRange != null)
                    {
                        EllMaxRange.Width = EllMaxRange.Height = MaxRange * 2;
                        EllMaxRange.ToolTip = "Max:" + MaxRange;
                        Canvas.SetLeft(EllMaxRange, -EllMaxRange.Width / 2 + this.Width / 2);
                        Canvas.SetTop(EllMaxRange, -EllMaxRange.Height / 2 + this.Height / 2);
                    }
                }
            }
        }

        public double DeltaTime
        {
            get { return _deltaTime; }
            set { _deltaTime = value; }
        }

        // last bound hole (at end of lifetime)
        protected List<BoundPoint> _lastBoundPoints = new List<BoundPoint>();

        public List<BoundPoint> LastBoundPoints { get { return _lastBoundPoints; } }

        // list distance function for each point on bound hole
        protected List<SimpleDistanceFunction> _distanceFuncs = new List<SimpleDistanceFunction>();

        public List<SimpleDistanceFunction> DistanceFuncs { get { return _distanceFuncs; } }

        // hole's center
        public DPoint Center { get; set; }

        #endregion Properties

        #region UI variables

        private bool evalRangeStart = false;

        // node state
        private HoleStates _state = HoleStates.Normal;

        public HoleStates State
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _state = value;
                    switch (_state)
                    {
                        case HoleStates.MinRangeSelecting:
                            OldMinRange = _minRange;
                            EllMinRange.Visibility = Visibility.Visible;
                            EllMaxRange.Visibility = Visibility.Visible;
                            break;

                        case HoleStates.MaxRangeSelecting:
                            MaxRange = _maxRange >= _minRange ? _maxRange : _minRange;
                            OldMaxRange = _maxRange;
                            EllMinRange.Visibility = Visibility.Visible;
                            EllMaxRange.Visibility = Visibility.Visible;
                            break;

                        default:
                            OldMinRange = _minRange;
                            OldMaxRange = _maxRange;
                            EllMinRange.Visibility = Visibility.Collapsed;
                            EllMaxRange.Visibility = Visibility.Collapsed;
                            break;
                    }
                }
            }
        }

        #endregion UI variables

        public SingleHole(DPoint center, double startTime, double lifeTime, double rMax, double rMin, SimpleDistribution distributor)
        {
            InitializeComponent();

            //range
            MinRange = rMin;
            MaxRange = rMax;

            distributor.generateHole(MinRange, MaxRange);
            List<BoundPoint> boundPoints = distributor.getBoundPoints();
            Center = new DPoint(center.X, center.Y);
            LifeTime = lifeTime;
            StartTime = startTime;
            currentTime = 0;
            _lastBoundPoints.Clear();
            foreach (BoundPoint bp in boundPoints)
            {
                // add bound point
                _lastBoundPoints.Add(new BoundPoint(bp.Degree, bp.Radius, BoundPoint.CoordinateType.POLAR));
                // add simple distance function as default
                _distanceFuncs.Add(new SimpleDistanceFunction());
            }
        }

        #region event handler

        private void EllHole_MouseEnter(object sender, MouseEventArgs e)
        {
            EllMinRange.Visibility = Visibility.Visible;
            EllMaxRange.Visibility = Visibility.Visible;
            EllHole.ToolTip = string.Format("({0},{1})\nMin range:{2:0.0}\nMax range:{3:0.0}", Center.X, Center.Y, MinRange, MaxRange);

            e.Handled = true;
        }

        private void EllHole_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!evalRangeStart)
            {
                EllMinRange.Visibility = Visibility.Collapsed;
                EllMaxRange.Visibility = Visibility.Collapsed;
            }

            e.Handled = true;
        }

        private void EllHole_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ((Parent as Canvas).Parent as DynamicHole).RemoveHole(this);
        }

        private void EllHole_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            evalRangeStart = true;
            EllHole.IsHitTestVisible = false;
            EllHole.IsEnabled = false;

            ((Parent as Canvas).Parent as DynamicHole).selectedHole = this;
            State = HoleStates.MinRangeSelecting;
            MinRange = 0;

            e.Handled = true;
        }

        public void EnableMouseCapture()
        {
            evalRangeStart = false;
            EllHole.IsHitTestVisible = true;
            EllHole.IsEnabled = true;
        }

        #endregion event handler

        #region hole method

        /// <summary>
        /// detect offtime of point p
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public double PointOffTime(DPoint p)
        {
            // translate p to root (0,0)
            DPoint tempDPoint = new DPoint(p.X - Center.X, p.Y - Center.Y);

            // detect 2 boundpoint between Point p
            double angle = tempDPoint.Degree;

            if (double.IsNaN(angle)) return 0;

            int index1 = (int)Math.Floor(angle / ISimpleDistribution.deltaDegree);
            int index2 = (index1 + 1) % _lastBoundPoints.Count;

            double t1 = _distanceFuncs[index1].calcTime(tempDPoint.Radius, _lastBoundPoints[index1].Radius, LifeTime);
            double t2 = _distanceFuncs[index2].calcTime(tempDPoint.Radius, _lastBoundPoints[index2].Radius, LifeTime);

            return t1 > t2 ? t2 : t1;
        }

        #endregion hole method
    }

    public enum HoleStates
    {
        MinRangeSelecting,
        MaxRangeSelecting,
        Normal
    }
}