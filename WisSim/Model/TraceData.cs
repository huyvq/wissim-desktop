﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WisSim.Model
{
    public class TraceData
    {
        private Dictionary<int, Model.NodeData> nodes;
        private Dictionary<int, Model.PacketData> packets;
        private List<Model.EventData> events;
        private List<Model.EventData> simulateEvents;
        private double transitionTime = 0.0129;

        public TraceData()
        {
            nodes = new Dictionary<int, Model.NodeData>();
            packets = new Dictionary<int, Model.PacketData>();
            events = new List<Model.EventData>();
        }

        public Dictionary<int, Model.NodeData> Nodes
        {
            get { return nodes; }
            set { nodes = value; }
        }

        public Dictionary<int, Model.PacketData> Packets
        {
            get { return packets; }
            set { packets = value; }
        }

        public List<Model.EventData> Events
        {
            get { return events; }
            set { events = value; }
        }

        public List<Model.EventData> SimulateEvents
        {
            get { return simulateEvents; }
            set { simulateEvents = value; }
        }

        private List<string> packetTypes;

        public List<string> PacketTypes
        {
            get
            {
                if (packetTypes == null)
                {
                    packetTypes = new List<string>();
                }

                return packetTypes;
            }

            set
            {
                packetTypes = value;
            }
        }

        #region Analyser functions

        #region LifeTime

        // Tinh thoi gian hoat dong cua mang cho den khi co >= x% node trong mang chet
        // Tham so deadthPercent - x% (0 < x <=100).
        // Tra lai thoi gian (s)
        // Neu so nut chet < x% thi tra lai la Max time; default: 500 (s)
        // TODO: Tim gia tri cua Max time
        public double GetNetworkLifeTime(double deadthPercent, double maxTime, List<NodeData> nodeList)
        {
            if ((deadthPercent <= 0) || (deadthPercent > 100))
            {
                throw new Exception("deadthPercent is out of range. It should in range of (0,100]");
            }
            double maxDeathNodes = deadthPercent * nodes.Count;
            double networkLifeTime = 0;
            int numberOfDeadthNodes = 0;

            //CheckDeadNodeByEnergy();

            foreach (NodeData node in nodeList)
            {
                //NodeData node = nodes[nodeId];
                //Kiem tra xem co phai chet khong //co the bao gom ca ly do khac
                if (node.IsDead)
                {
                    numberOfDeadthNodes += 1;
                    networkLifeTime = node.DeadAt;
                }

                if (numberOfDeadthNodes == maxDeathNodes)
                {
                    break;
                }
            }

            if (numberOfDeadthNodes >= maxDeathNodes)
            {
                return networkLifeTime;
            }
            else
            {
                return maxTime;
            }
        }

        //TODO: Co the khong can nua....
        public void CheckDeadNodeByEnergy()
        {
            foreach (int nodeId in nodes.Keys)
            {
                NodeData node = nodes[nodeId];
                //Kiem tra xem co phai chet do het nang luong
                // Mac dinh list engergy cua node la sap xep theo thoi gian tang dan
                if ((node.Energies != null) && (node.Energies.Count > 0))
                {
                    NodeEnergy lastEnergy = node.Energies.Last();
                    if (lastEnergy.Energy <= 0)
                    {
                        //Cap nhat thong tin
                        node.IsDead = true;
                        node.DeadAt = lastEnergy.Time;
                    }
                }
            }
        }

        #endregion LifeTime

        #region SleepTime

        // Tinh thoi gian ngu cua mot node
        // Thoi gian ngu tinh bang cong thuc maxTime - Transitiontime * number_of_transition
        public double GetNodeSleepTime(double maxTime, NodeData node)
        {
            if (node.SleepTime == -1)
            {
                double sleepTime = maxTime;
                sleepTime -= node.NodeEvent.Count * transitionTime;
                node.SleepTime = sleepTime;
            }
            return node.SleepTime;
        }

        //return  [Min, Max, Average sleepTime of node in network]
        public double[] CalculateNetWorkSleepTime(double maxTime, List<NodeData> nodeList)
        {
            double minSleepTime = maxTime;
            double maxSleepTime = 0;
            double totalSleepTime = 0;
            foreach (NodeData node in nodeList)
            {
                double sleepTime = GetNodeSleepTime(maxTime, node);
                if (sleepTime < minSleepTime)
                {
                    minSleepTime = sleepTime;
                }

                if (sleepTime > maxSleepTime)
                {
                    maxSleepTime = sleepTime;
                }

                totalSleepTime += sleepTime;
            }
            double[] result = new double[3];
            result[0] = minSleepTime;
            result[1] = maxTime;
            result[2] = totalSleepTime / nodeList.Count;
            return result;
        }

        #endregion SleepTime

        /*
        // Derived Data: IsMultiCast, IsDrop, SentNode, DestinationNode
        //Xac dinh la MultiCast neu nhu tren pacet nay xuat hien nhieu lan Su kien send
        // C2: do la su dung header o dang sau de xem node dich la id hay la fffffff;
        // Dang su dung cach so 1;
        public void CalculatePacketsDerivedData(int packetID)
        {
            PacketData packet = packets[packetID];
            int numberOfSendEvent = 0;
            packet.IsMultiCast = false;
            foreach(EventData packetEvent in packet.EventList)
            {
                if(packetEvent.Type == EventData.EventType.SEND)
                {
                    numberOfSendEvent += 1;
                }

                if (numberOfSendEvent >=2)
                {
                    packet.IsMultiCast = true;
                    break;
                }
            }
        }
        */

        #region hopCount

        //Hop Count chi tinh duoc cho cac goi tin dinh tuyen binh thuong, va khong phai la multicast
        //Trong chuong trinh, mac dinh goi tin HELLO la goi tin multicast cho hang xom - bo qua.
        //TODO: Co the khong can nua
        public int GetPacketHopCount(int packetID)
        {
            PacketData packet = packets[packetID];
            if (packet.HopCount == -1)
            {
                if (packet.IsMultiCast == false)
                {
                    packet.HopCount = packet.NodeList.Count - 1;
                }
                else
                {
                    packet.HopCount = 0;
                }
            }

            return packet.HopCount;
        }

        //Return [Min, Max, Average] HopCount of network
        // Ko tinh cac goi tin multicast;
        /// Update 17/3/2015 - Huy
        /// get list packet with filter
        public double[] CalculateNetWorkHopCount(PacketFilterOption option)
        {
            List<PacketData> packets = GetListOfPackets(option);
            int minHopCount = Int32.MaxValue;
            int maxHopCount = 0;
            double averageHopCount = 0;
            double variantHopCount = 0;
            int unicastPacket = 0;
            foreach (PacketData packet in packets)
            {
                int hopCount = packet.HopCount;
                if (!packet.IsMultiCast)
                {
                    averageHopCount += hopCount;
                    unicastPacket += 1;
                    if (hopCount > maxHopCount)
                    {
                        maxHopCount = hopCount;
                    }

                    if (hopCount < minHopCount)
                    {
                        minHopCount = hopCount;
                    }
                }
            }
            double[] result = new double[4];
            result[0] = minHopCount;
            result[1] = maxHopCount;
            //result[2] = variantHopCount;
            if (unicastPacket != 0)
            {
                result[2] = averageHopCount / unicastPacket;
            }
            else
            {
                result[2] = 0;
            }
            return result;
        }

        public Dictionary<PacketData, int> CalculatePathHopCount(PacketFilterOption option)
        {
            List<PacketData> packets = GetListOfPackets(option);
            Dictionary<PacketData, int> result = new Dictionary<PacketData, int>();

            foreach (PacketData packet in packets)
            {
                if (!packet.IsDrop)
                {
                    int hopcount = 0;
                    foreach (var e in packet.EventList)
                    {
                        if (e.Layer == "RTR" && e.Type == EventData.EventType.RECEIVE)
                        {
                            hopcount++;
                        }
                    }
                    result.Add(packet, hopcount);
                }
            }
            return result;
        }

        #endregion hopCount

        #region latency

        //TODO: Co the khong can nua
        public double GetPacketLatency(int packetID)
        {
            PacketData packet = packets[packetID];
            if (!packet.IsMultiCast)
            {
                EventData first = packet.EventList.First();
                EventData last = packet.EventList.Last();
                packet.Latency = last.HappendTime - first.HappendTime;
            }
            return packet.Latency;
        }

        //Return [Min, Max, Average] HopCount of network
        // Ko tinh cac goi tin multicast;
        public double[] CalculateNetworkLatencty(PacketFilterOption option)
        {
            List<PacketData> packetList = GetListOfPackets(option);

            double minLatency = double.MaxValue;
            double maxLatency = 0;
            double averageLatency = 0;
            double unicastPacket = 0;
            foreach (PacketData packet in packetList)
            {
                double latency = packet.Latency;
                if (!packet.IsMultiCast)
                {
                    averageLatency += latency;
                    unicastPacket += 1;
                    if (latency > maxLatency)
                    {
                        maxLatency = latency;
                    }

                    if (maxLatency < minLatency)
                    {
                        minLatency = latency;
                    }
                }
            }

            double[] result = new double[4];
            result[0] = minLatency;
            result[1] = maxLatency;
            //result[2] = variantHopCount;
            if (unicastPacket != 0)
            {
                result[2] = averageLatency / unicastPacket;
            }
            else
            {
                result[2] = 0;
            }
            return result;
        }

        #endregion latency

        #region Efficiency

        public double CalculateDroppedRatio(PacketFilterOption option)
        {
            List<PacketData> packetList = GetListOfPackets(option);

            double unicastPacket = 0;
            int droppedPacket = 0;
            foreach (PacketData packet in packetList)
            {
                if (!packet.IsMultiCast)
                {
                    unicastPacket += 1;
                    if (packet.IsDrop)
                    {
                        droppedPacket += 1;
                    }
                }
            }

            if (unicastPacket != 0)
            {
                return droppedPacket / unicastPacket;
            }
            else
            {
                return 1;
            }
        }

        #endregion Efficiency

        #region throughput

        public double GetNetworkThroughput(double startTime, double endTime, PacketFilterOption option)
        {
            List<PacketData> packetList = GetListOfPackets(option);

            if (endTime <= startTime)
            {
                throw new Exception("Invalid parameter!");
            }
            double throughput = 0;
            foreach (PacketData packet in packetList)
            {
                //if(!packet.IsMultiCast)
                //{
                if ((packet.EventList.First().HappendTime < endTime)
                    && (packet.EventList.Last().HappendTime > startTime))
                {
                    throughput += packet.PacketSize;
                }
                //}
            }
            return throughput / (endTime - startTime);
        }

        #endregion throughput

        #region engergy

        //return  [Min, Max, Average LastEnergy of node in network]
        public double[] CalculateNetWorkEnergy(List<NodeData> nodeList)
        {
            double minEnergy = 1000;
            double maxEnergy = 0;
            double totalEnergy = 0;
            foreach (NodeData node in nodeList)
            {
                if (node.LastEnergy < minEnergy)
                {
                    minEnergy = node.LastEnergy;
                }

                if (node.LastEnergy > maxEnergy)
                {
                    maxEnergy = node.LastEnergy;
                }

                totalEnergy += node.LastEnergy;
            }
            double[] result = new double[3];
            result[0] = minEnergy;
            result[1] = maxEnergy;
            result[2] = totalEnergy / nodes.Keys.Count;
            return result;
        }

        #endregion engergy

        #endregion Analyser functions

        //Group of node in format nodeid1|nodeid2|...|nodeidn
        public List<Model.NodeData> GetGroupOfNodes(string group)
        {
            List<Model.NodeData> result = new List<Model.NodeData>();
            var groupNodes = Regex.Split(group, "\\|");
            foreach (string nodeid in groupNodes)
            {
                try
                {
                    int id = System.Convert.ToInt32(nodeid);
                    result.Add(nodes[id]);
                }
                catch (Exception ex)
                {
                    //do nothing
                }
            }
            return result;
        }

        public List<Model.PacketData> GetListOfPackets(PacketFilterOption filterOption)
        {
            List<Model.PacketData> result = new List<PacketData>();
            if (filterOption != null)
            {
                if ((String.IsNullOrEmpty(filterOption.Type)) &&
                    (String.IsNullOrEmpty(filterOption.MaxSize)) &&
                    (String.IsNullOrEmpty(filterOption.MinSize)) &&
                    (String.IsNullOrEmpty(filterOption.SourceGroup)) &&
                    (String.IsNullOrEmpty(filterOption.DestinationGroup)) &&
                    (String.IsNullOrEmpty(filterOption.Layer)) &&
                    (String.IsNullOrEmpty(filterOption.StartTime)) &&
                    (String.IsNullOrEmpty(filterOption.EndTime)))
                {
                    //Filter nothing
                    for (int offset = filterOption.Offset; offset < filterOption.Offset + filterOption.Limit; offset++)
                    {
                        try
                        {
                            result.Add(packets.ElementAt(offset).Value);
                        }
                        catch (Exception ex)
                        {
                            //Do nothing
                            Console.WriteLine("Exception at getListOfPackets: " + ex.Message);
                            break;
                        }
                    }
                }
                else
                {
                    int offset = -1;
                    foreach (Model.PacketData packet in packets.Values)
                    {
                        if ((!String.IsNullOrEmpty(filterOption.Type)) && (packet.PacketType != filterOption.Type))
                        {
                            continue;
                        }
                        if ((!String.IsNullOrEmpty(filterOption.MaxSize)) && (packet.PacketSize > System.Convert.ToInt32(filterOption.MaxSize)))
                        {
                            continue;
                        }
                        if ((!String.IsNullOrEmpty(filterOption.MinSize)) && (packet.PacketSize < System.Convert.ToInt32(filterOption.MinSize)))
                        {
                            continue;
                        }
                        if ((!String.IsNullOrEmpty(filterOption.Layer)) && (packet.SourceLayer != filterOption.Layer))
                        {
                            continue;
                        }
                        if (!String.IsNullOrEmpty(filterOption.SourceGroup))
                        {
                            string sourceGroup = "|" + filterOption.SourceGroup + "|";
                            if (sourceGroup.IndexOf("|" + packet.SourceNode.ID.ToString() + "|") < 0)
                            {
                                continue;
                            }
                        }

                        if (!String.IsNullOrEmpty(filterOption.DestinationGroup))
                        {
                            if (packet.DestinationNode != null)
                            {
                                string destGroup = "|" + filterOption.DestinationGroup + "|";
                                if (destGroup.IndexOf("|" + packet.DestinationNode.ID.ToString() + "|") < 0)
                                {
                                    continue;
                                }
                            }
                            else { continue; }
                        }
                        if ((!String.IsNullOrEmpty(filterOption.StartTime)) && (packet.StartTime < System.Convert.ToDouble(filterOption.StartTime)))
                        {
                            continue;
                        }
                        if ((!String.IsNullOrEmpty(filterOption.EndTime)) && (packet.EndTime > System.Convert.ToDouble(filterOption.EndTime)))
                        {
                            continue;
                        }
                        //Pass all conditional
                        offset += 1;
                        if ((offset >= filterOption.Offset) && (offset < filterOption.Offset + filterOption.Limit))
                        {
                            result.Add(packet);
                        }

                        if (offset == filterOption.Offset + filterOption.Limit)
                        {
                            break;
                        }
                    }
                }
            }

            return result;
        }
    }
}