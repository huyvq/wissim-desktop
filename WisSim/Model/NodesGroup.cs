﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisSim.Model
{
    public class NodesGroup
    {
        public NodesGroup()
        {
            this.nodes = new List<int>();
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }

        }

        private List<int> nodes;
        public List<int> Nodes
        {
            get { return nodes; }
            set { nodes = value; }
        }
    }

    public partial class NodesGroupsChangedEventArgs: EventArgs
    {
        public Dictionary<string,List<Node>> GroupList;
        public NodesGroupsChangedEventArgs(Dictionary<string,List<Node>> groupList)
        {
            this.GroupList = groupList;
        }

        public List<NodesGroup> getNodeGroups()
        {
            List<NodesGroup> nodeGroups = new List<NodesGroup>();
            foreach(string groupName in GroupList.Keys)
            {
                NodesGroup nodeGroup = new NodesGroup();
                nodeGroup.Name = groupName;
                foreach (Node node in GroupList[groupName])
                {
                    nodeGroup.Nodes.Add(node.ID);
                }
                nodeGroups.Add(nodeGroup);
            }

            return nodeGroups;
        }
    }

    public partial class NodesGroupEventArgs : EventArgs
    {
        public List<Node> Group;
        public string Name;
        public NodesGroupEventArgs(List<Node> group, string Name)
        {
            this.Group = group;
            this.Name = Name;
        }

        public NodesGroupEventArgs(NodesGroup nodeGroup)
        {
            this.nodeGroup = nodeGroup;
        }

        private NodesGroup nodeGroup;

        public NodesGroup getNodeGroup()
        {
            if (nodeGroup == null)
            {
                this.nodeGroup = new NodesGroup();
                nodeGroup.Name = this.Name;
                foreach (Node node in this.Group)
                {
                    nodeGroup.Nodes.Add(node.ID);
                }
            }
            return nodeGroup;
        }
    }
}
