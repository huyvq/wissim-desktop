﻿using System;
using System.Linq;

namespace WisSim.Model
{
    /// <summary>
    /// State of node
    /// <author>Trong Nguyen</author>
    /// </summary>
    public enum NodeStates
    {
        Normal,     // normal node
        Stuck,      // stuck node
        Dead,       // deal node

        //      Start,      // node is seted as source
        //     Stop,       // node is seted as destination
        Source,     // node was seted as source

        Dest,        // node was seted as destination
        Drop,       // a packet is dropped at this node
        Alarm,
    }
}