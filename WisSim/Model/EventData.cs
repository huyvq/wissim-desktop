﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisSim.Model
{
    public class EventData
    {
        public int Id { get; set; }

        public enum EventType
        {
            SEND, RECEIVE, FORWARD, DROP, ENERGY, UNKNOWN,
            SIM_NODEOFF, SIM_ADJACENTHOLE, SIM_HOLE // event for simulate only
        };

        private EventType type;

        public EventType Type
        {
            get { return type; }
            set { type = value; }
        }

        private double happendTime;

        public double HappendTime
        {
            get { return happendTime; }
            set { happendTime = value; }
        }

        private NodeData node;

        public NodeData Node
        {
            get { return node; }
            set { node = value; }
        }

        private NodeData previousNode;

        public NodeData PreviousNode
        {
            get { return previousNode; }
            set { previousNode = value; }
        }

        private PacketData packet;

        public PacketData Packet
        {
            get { return packet; }
            set { packet = value; }
        }

        private string layer;

        public string Layer
        {
            get { return layer; }
            set { layer = value; }
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private List<string> headers;

        //public List<string> Headers
        //{
        //    get {
        //        if(headers == null)
        //        {
        //            headers = new List<string>();
        //        }
        //        return headers;
        //    }
        //}

        //Comment --> may not used
        //public void AddHeader(string header)
        //{
        //    if(headers == null)
        //    {
        //        headers = new List<string>();
        //    }
        //    headers.Add(header);
        //}
        //public string TransportHeader
        //{
        //    get
        //    {
        //        return headers.First() + "|" + headers.Last();
        //    }
        //}
    }
}