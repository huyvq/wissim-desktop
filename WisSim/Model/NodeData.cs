﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WisSim.NetworkComponents;

namespace WisSim.Model
{
    public class NodeData
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private int groupID;

        public int GroupID
        {
            get { return groupID; }
            set { groupID = value; }
        }

        private double x;

        public double X
        {
            get { return x; }
            set { x = value; }
        }

        private double y;

        public double Y
        {
            get { return y; }
            set { y = value; }
        }

        private double offTime;

        public double OffTime
        {
            get { return offTime; }
            set { offTime = value; }
        }

        private double range;

        public double Range
        {
            get { return range; }
            set { range = value; }
        }

        private List<NodeData> neighborList;

        public List<NodeData> NeighborList
        {
            get { return neighborList; }
            set { neighborList = value; }
        }

        //Dictionary from time to energy
        /*
        private Dictionary<double, double> energy;
        public Dictionary<double, double> Energy
        {
            get { return energy; }
        }
        */
        private List<NodeEnergy> energies;

        public List<NodeEnergy> Energies
        {
            get { return energies; }
        }

        private List<EventData> nodeEvent;

        public List<EventData> NodeEvent
        {
            get { return nodeEvent; }
        }

        public NodeData(int ID, double X, double Y)
        {
            this.id = ID;
            this.x = X;
            this.y = Y;
            neighborList = new List<NodeData>();
            nodeEvent = new List<EventData>();
            //energy = new Dictionary<double, double>();
            energies = new List<NodeEnergy>();
            packetList = null;
        }

        #region Derived data

        private Boolean isDead;
        private double deadAt = -1;

        public bool IsDead
        {
            get
            {
                if (isDead == null)
                {
                    CheckDeadNodeByEnergy();
                    if (isDead == false && offTime != -1)
                    {
                        isDead = true;
                        deadAt = offTime;
                    }
                }
                return isDead;
            }
            set { isDead = value; }
        }

        //Thoi diem ma nut chet //Duoc thong bao la chet trong tracefile hoac la do het nang luong
        public double DeadAt
        {
            get
            {
                if (deadAt == null)
                {
                    CheckDeadNodeByEnergy();
                    if (isDead == false && offTime != -1)
                    {
                        isDead = true;
                        deadAt = offTime;
                    }
                }
                return deadAt;
            }
            set { deadAt = value; }
        }

        public double SleepTime = -1;

        private List<PacketData> packetList;

        public List<PacketData> PacketList
        {
            get
            {
                if (packetList == null)
                {
                    packetList = new List<PacketData>();
                    foreach (EventData e in nodeEvent)
                    {
                        if (!packetList.Contains(e.Packet))
                        {
                            packetList.Add(e.Packet);
                        }
                    }
                }
                return packetList;
            }
        }

        public int NonDropCBRPacketCount
        {
            get
            {
                int nonDropCount = 0;
                if (PacketList != null)
                {
                    foreach (var p in PacketList)
                    {
                        if (p.PacketType == "cbr" && !p.IsDrop)
                        {
                            nonDropCount++;
                        }
                    }
                }
                return nonDropCount;
            }
        }

        public int DropCBRPacketCount
        {
            get
            {
                int dropCount = 0;
                if (PacketList != null)
                {
                    foreach (var p in PacketList)
                    {
                        if (p.PacketType == "cbr" && p.IsDrop)
                        {
                            dropCount++;
                        }
                    }
                }
                return dropCount;
            }
        }

        private double lastEnergy = -1;

        public double LastEnergy
        {
            get
            {
                if (lastEnergy == -1)
                {
                    if (energies.Count > 0)
                    {
                        lastEnergy = energies.Last().Energy;
                    }
                }

                return lastEnergy;
            }
        }

        #endregion Derived data

        #region method

        public void CheckDeadNodeByEnergy()
        {
            //Kiem tra xem co phai chet do het nang luong
            // Mac dinh list engergy cua node la sap xep theo thoi gian tang dan
            if ((Energies != null) && (Energies.Count > 0))
            {
                NodeEnergy lastEnergy = this.Energies.Last();
                if (lastEnergy.Energy <= 0)
                {
                    //Cap nhat thong tin
                    isDead = true;
                    deadAt = lastEnergy.Time;
                }
                else
                {
                    isDead = false;
                    deadAt = -1;
                }
            }
            else
            {
                isDead = false;
                deadAt = -1;
            }
        }

        #endregion method
    }

    public class NodeEnergy
    {
        private double time;
        private double energy;
        public double Time { get { return time; } set { time = value; } }
        public double Energy { get { return energy; } set { energy = value; } }

        public NodeEnergy(double time, double energy)
        {
            this.time = time;
            this.energy = energy;
        }
    }
}