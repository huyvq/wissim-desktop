﻿using System.Collections;

namespace WisSim.NetworkComponents
{
    /// <summary>
    /// Interface for Network
    /// <author>Trong Nguyen</author>
    /// </summary>
    public interface INetwork
    {            
        // simulation Time     
        double SimulationTime { get; }

        // list of nodes in Network
        IList NodeList { get; }             

        //// size of Network	    
        double Width { get; }

        double Height { get; }
    } 
}
