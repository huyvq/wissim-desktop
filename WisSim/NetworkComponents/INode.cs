﻿using System.Collections.Generic;

namespace WisSim.NetworkComponents
{
    /// <summary>
    /// Interface for node
    /// <author>Trong Nguyen</author>
    /// </summary>
    public interface INode
    {
        int ID { get; }        

        // position of node	    
        double X { get; }
        double Y { get; }

        double OffTime { get; }

	    // radio range	    
        int Range { get; }

	    // list of neighbors
        IList<INode> NeighborList { get; }
    }
}
