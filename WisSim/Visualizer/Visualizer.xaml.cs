﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using WisSim.Controller.Parser;
using WisSim.Model;

namespace WisSim.Visualizer
{
    /// <summary>
    /// Interaction logic for Visualizer.xaml
    /// </summary>
    public partial class Visualizer : UserControl
    {
        public Visualizer()
        {
            InitializeComponent();

            Network.SetToViewOnlyMode();

            Network.ShowEdges = false;
            chbToggleEdges.IsChecked = false;
        }

        //private MainWindow mainWindow;

        //public MainWindow Main
        //{
        //    get
        //    {
        //        return mainWindow;
        //    }
        //    set
        //    {
        //        mainWindow = value;
        //        networkHeight.Value = Project.Network.Height.ToString();
        //        networkWidth.Value = Project.Network.Width.ToString();
        //    }
        //}

        public static readonly DependencyProperty MainProperty = DependencyProperty.Register(
            "Main", typeof(MainWindow), typeof(Visualizer), new PropertyMetadata(default(MainWindow)));

        public MainWindow Main
        {
            get { return (MainWindow)GetValue(MainProperty); }
            set { SetValue(MainProperty, value); }
        }

        private TraceData traceData;

        public string RoutingProtocol { get; set; }

        #region Binding data

        public void BindingData(TraceData data)
        {
            Console.WriteLine("Binding data to visualizer");
            Console.WriteLine(DateTime.Now.ToString("hh.mm.ss.ffffff"));
            this.traceData = data;
            Network.Reset();
            //Add node
            if (this.traceData != null)
            {
                foreach (NodeData node in traceData.Nodes.Values)
                {
                    Network.AddNode(node.ID, node.X, node.Y, node.OffTime);
                }
            }
            Console.WriteLine(DateTime.Now.ToString("hh.mm.ss.ffffff"));

            Network.ShowEdges = false;
            chbToggleEdges.IsChecked = false;
            // update timer's maximum
            Timer.Maximum = traceData.Events.Last().HappendTime;
            // update network size
            double EXTRA_DISTANCE = 10;
            double width = traceData.Nodes.Max(p => p.Value.X) + EXTRA_DISTANCE;
            double height = traceData.Nodes.Max(p => p.Value.Y) + EXTRA_DISTANCE;

            if (width > Network.Width) Network.Width = width;
            if (height > Network.Height) Network.Height = height;
        }

        public void ShowPacketPath(int packetId)
        {
            Network.ClearPacketPaths();
            if (traceData != null)
            {
                if (traceData.Packets != null && traceData.Packets.Keys.Contains(packetId))
                {
                    Network.AddPacketPath(traceData.Packets[packetId], UcPacket.UcPacketCreateType.NORMAL);

                    VisualizeParallelogram(new int[] { packetId });
                }
                else
                {
                    MessageBox.Show("Packet " + packetId.ToString() + " is invalid");
                }
            }
        }

        public void ShowMultiPacketPath(List<int> packetId)
        {
            Network.ClearPacketPaths();

            if (traceData != null)
            {
                if (traceData.Packets != null)
                {
                    foreach (var id in packetId)
                    {
                        if (traceData.Packets.Keys.Contains(id))
                        {
                            Network.AddPacketPath(traceData.Packets[id], UcPacket.UcPacketCreateType.NORMAL);
                        }
                        VisualizeParallelogram(packetId.ToArray());
                    }
                }
            }
        }

        #endregion Binding data

        #region EventHander method

        private void TimerChange(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double time = (sender as Slider).Value;
            if (RoutingProtocol == "GRIDDYNAMIC")
            {
                // filter hole
                string BOUNDHOLE_TAG = "BOUNDHOLE";
                // remove all hole
                for (int i = 0; i < Network.VisualizeHoles.Count; i++)
                {
                    var shape = Network.VisualizeHoles[i];
                    if (shape.GetType() == typeof(Path))
                    {
                        Path p = shape as Path;
                        if (p.Tag == BOUNDHOLE_TAG)
                        {
                            Network.VisualizeHoles.Remove(shape);
                            i--;
                        }
                    }
                }
                // filter simulate events
                var events = traceData.SimulateEvents.Where(p => p.HappendTime <= time);
                var alarmNodes = new List<Node>();
                var adjacentNodes = new List<Node>();
                foreach (EventData evt in events)
                {
                    if (evt.Type == EventData.EventType.SIM_HOLE)
                    {
                        List<NodeData> nodes = DynamicGridHole[evt.HappendTime];
                        Network.AddPolygon(1, nodes, Colors.Red, BOUNDHOLE_TAG);

                        foreach (Node n in Network.NodeList)
                        {
                            if (n.ID == evt.Node.ID) { alarmNodes.Add(n); break; }
                        }
                    }
                    else if (evt.Type == EventData.EventType.SIM_ADJACENTHOLE)
                    {
                        foreach (Node n in Network.NodeList)
                        {
                            if (n.ID == evt.Node.ID) { adjacentNodes.Add(n); break; }
                        }
                    }
                }

                Network.CurrentTime = time;

                foreach (Node n in adjacentNodes)
                {
                    if (n.States != NodeStates.Dead)
                        n.States = NodeStates.Alarm;
                }

                foreach (Node n in alarmNodes)
                {
                    if (n.States != NodeStates.Dead)
                        n.States = NodeStates.Source;
                }
            }

            if (networkAnimation.IsRunning) return;

            Network.ClearPacketPaths();
        }

        private void chbToggleEdges_Click(object sender, RoutedEventArgs e)
        {
            bool showEdges = (bool)chbToggleEdges.IsChecked;
            if (showEdges)
            {
                //if (Network.Edges == null || Network.Edges.Count == 0)
                if (!Network.HasEdges)
                {
                    //Todo: get radioRange from traceData, tcl files;
                    Network.UpdateNeighbors(40);
                }
            }
            Network.ShowEdges = showEdges;
        }

        private void btnLoadExtraTraceFile_Click(object sender, RoutedEventArgs e)
        {
            ParseExtraDataFiles("", "", "");
        }

        #endregion EventHander method

        #region visualize Extra info

        private void ParseExtraDataFiles(string boundHole, string adjacentHole, string parallelogram)
        {
            ImportExtraDataWindow w = new ImportExtraDataWindow();
            w.BoundHoleFile = boundHole;
            w.AdjacentHoleFile = adjacentHole;
            w.ParallelogramFile = parallelogram;
            if (w.ShowDialog() == true)
            {
                // parse file
                RoutingProtocol = w.RoutingProtocol;
                Console.WriteLine("protocol: " + RoutingProtocol);
                string[] traceFiles = new string[3];
                traceFiles[0] = NS2Parser.OPTDATA_FILE_PREFIX + "-" + RoutingProtocol + "-" + NS2Parser.OPT_BOUNDHOLE + "-" + w.BoundHoleFile;
                traceFiles[1] = NS2Parser.OPTDATA_FILE_PREFIX + "-" + RoutingProtocol + "-" + NS2Parser.OPT_ADJACENTHOLE + "-" + w.AdjacentHoleFile;
                traceFiles[2] = NS2Parser.OPTDATA_FILE_PREFIX + "-" + RoutingProtocol + "-" + NS2Parser.OPT_PARALLELOGRAM + "-" + w.ParallelogramFile;

                Main.parseFilesBackground(traceFiles, FinishParseFiles);
            }
        }

        private void FinishParseFiles(object sender, RunWorkerCompletedEventArgs e)
        {
            Main.unsetParseWorkerCallback(FinishParseFiles);

            if (!e.Cancelled)
            {
                NS2Parser ns2Parser = (NS2Parser)e.Result;
                Dictionary<string, object> data = ns2Parser.GetOptData();

                Network.VisualizeHoles.Clear();

                if (data.ContainsKey(NS2Parser.OPT_BOUNDHOLE))
                {
                    // normal bound hole
                    if (RoutingProtocol != "GRIDDYNAMIC")
                    {
                        Dictionary<int, List<NodeData>> boundhole = (Dictionary<int, List<NodeData>>)data[NS2Parser.OPT_BOUNDHOLE];
                        foreach (int id in boundhole.Keys)
                        {
                            if (RoutingProtocol == "BEHDS")
                            {
                                NodeData center = boundhole[id][0];
                                Network.AddCircleBoundHole(id, center.X, center.Y, center.Range, center.Range);
                            }
                            else
                            {
                                Network.AddBoundHole(id, boundhole[id]);
                            }
                        }
                    }
                }

                if (data.ContainsKey(NS2Parser.OPT_PARALLELOGRAM))
                {
                    // parallelogram
                    if (RoutingProtocol == "ELBARGRIDOFFLINE")
                    {
                        Parallelogram = (Dictionary<int, Dictionary<int, List<NodeData>>>)data[NS2Parser.OPT_PARALLELOGRAM];
                    }
                }

                if (RoutingProtocol == "GRIDDYNAMIC")
                {
                    if (traceData != null && traceData.SimulateEvents != null)
                    {
                        List<EventData> simulateEvents = ns2Parser.GetSimulateEvents();
                        // update node location
                        foreach (EventData evt in simulateEvents)
                        {
                            evt.Node = traceData.Nodes.First(p => p.Key == evt.Node.ID).Value;
                        }
                        traceData.SimulateEvents.AddRange(simulateEvents);
                        traceData.SimulateEvents.Sort((e1, e2) => e1.HappendTime.CompareTo(e2.HappendTime));
                    }
                    if (data.ContainsKey(NS2Parser.OPT_BOUNDHOLE))
                    {
                        DynamicGridHole = (Dictionary<double, List<NodeData>>)data[NS2Parser.OPT_BOUNDHOLE];
                    }
                    if (data.ContainsKey(NS2Parser.OPT_ADJACENTHOLE))
                    {
                        AdjacentHole = (Dictionary<double, int>)data[NS2Parser.OPT_ADJACENTHOLE];
                    }
                }
            }
        }

        private void VisualizeParallelogram(int[] packetIds)
        {
            Random randomGen = new Random();

            // filter parallogram
            string PARALLELOGRAM_TAG = "parallelogram";
            // remove all parallelogram
            for (int i = 0; i < Network.VisualizeHoles.Count; i++)
            {
                var shape = Network.VisualizeHoles[i];
                if (shape.GetType() == typeof(Path))
                {
                    Path p = shape as Path;
                    if (p.Tag == PARALLELOGRAM_TAG)
                    {
                        Network.VisualizeHoles.Remove(shape);
                        i--;
                    }
                }
            }
            // add parallelogram
            foreach (int packetId in packetIds)
            {
                if (Parallelogram != null && Parallelogram.ContainsKey(packetId))
                {
                    var paralls = Parallelogram[packetId];
                    foreach (int nodeId in paralls.Keys)
                    {
                        KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
                        KnownColor randomColorName = names[randomGen.Next(names.Length)];
                        System.Drawing.Color randomColor = System.Drawing.Color.FromKnownColor(randomColorName);

                        System.Windows.Media.Color newColor = System.Windows.Media.Color.FromArgb(randomColor.A, randomColor.R, randomColor.G, randomColor.B);
                        Network.AddPolygon(-1, paralls[nodeId], newColor, PARALLELOGRAM_TAG);
                    }
                }
            }
        }

        #endregion visualize Extra info

        #region group

        public event EventHandler<NodesGroupEventArgs> GroupDeleted;

        public event EventHandler<NodesGroupEventArgs> GroupAdded;

        public void OnGroupDeleted(object sender, NodesGroupEventArgs e)
        {
            if (this.GroupDeleted != null)
            {
                GroupDeleted(this, e);
            }
        }

        public void OnGroupAdded(object sender, NodesGroupEventArgs e)
        {
            if (this.GroupAdded != null)
            {
                GroupAdded(this, e);
            }
        }

        public void UpdateGroupVisulaize()
        {
            groupGrid.ItemsSource = this.Main.Groups.Values.ToList();
            groupGrid.Items.Refresh();
        }

        private void DeleteGroup_Click(object sender, RoutedEventArgs e)
        {
            if (this.GroupDeleted != null)
            {
                NodesGroup deletedGroup = (NodesGroup)groupGrid.SelectedItem;
                if (deletedGroup != null)
                {
                    GroupDeleted(this, new NodesGroupEventArgs(deletedGroup));
                }
            }
        }

        private void chbEditMode_Click(object sender, RoutedEventArgs e)
        {
            if (Network != null)
            {
                if ((sender as CheckBox).IsChecked == true)
                {
                    Network.NodeCanvas.EditingMode = InkCanvasEditingMode.Select;
                }
                else
                {
                    Network.NodeCanvas.EditingMode = InkCanvasEditingMode.None;
                }
            }
        }

        private void groupGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NodesGroup selectedGroup = (NodesGroup)groupGrid.SelectedItem;
            if (selectedGroup != null)
            {
                Network.SelectGroup(selectedGroup);
                chbEditMode.IsChecked = true;
                Network.NodeCanvas.EditingMode = InkCanvasEditingMode.Select;
            }
        }

        #endregion group

        #region play animation

        private int index = 0;
        private NetworkAnimation networkAnimation = new NetworkAnimation();

        /// <summary>
        /// call from analyzer
        /// </summary>
        /// <param name="na"></param>
        public void PlayAnimation(NetworkAnimation na)
        {
            Network.ClearPacketPaths();
            playAnimation(na);
        }

        /// <summary>
        /// play animation on whole network
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPlayAnimation_Click(object sender, RoutedEventArgs e)
        {
            if (traceData == null)
                return;

            btnPlayAnimtion.IsEnabled = false;
            btnPauseAnimation.IsEnabled = true;
            btnStopAnimation.IsEnabled = true;

            NetworkAnimation na = new NetworkAnimation();
            var time = Timer.Value;

            na.Events = traceData.Events.Where(o => o.HappendTime >= time).ToList();
            Network.ClearPacketPaths();

            if (na.Events.Count == 0)
            {
                return;
            }
            playAnimation(na);
        }

        /// <summary>
        /// change speed of animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboAnimationSpeed_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem selected = (sender as ComboBox).SelectedItem as ComboBoxItem;
            string text = selected.Content.ToString();
            switch (text)
            {
                case "1x":
                    networkAnimation.Speed = 1; break;
                case "2x":
                    networkAnimation.Speed = 2; break;
                case "0.5x":
                    networkAnimation.Speed = 0.5; break;
            }
        }

        /// <summary>
        /// stop play animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStopAnimation_Click(object sender, RoutedEventArgs e)
        {
            networkAnimation.IsRunning = false;
            networkAnimation = new NetworkAnimation();
            networkAnimation.IsRunning = false;

            tbStartTime.Text = "0";
            tbEndTime.Text = "0";
            tbCurrentTime.Text = "0";
            SimulatePercent.Text = "0%";
            Timer.Value = 0;
            Timer.IsEnabled = true;

            btnPlayAnimtion.IsEnabled = true;
            btnPauseAnimation.IsEnabled = false;
            btnStopAnimation.IsEnabled = false;

            Network.ClearPacketPaths();
        }

        /// <summary>
        /// pause play animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPauseAnimation_Click(object sender, RoutedEventArgs e)
        {
            networkAnimation.IsRunning = !networkAnimation.IsRunning;
            if (networkAnimation.IsRunning)
                playAnimation();
        }

        /// <summary>
        /// handle animation player
        /// </summary>
        /// <param name="na"></param>
        private void playAnimation(NetworkAnimation na)
        {
            index = 0;

            comboAnimationSpeed_SelectionChanged(comboAnimationSpeed, null);
            na.Speed = networkAnimation.Speed;
            networkAnimation = na;

            //
            // binding data to view
            //
            tbStartTime.Text = na.StartTime.ToString();
            tbEndTime.Text = na.EndTime.ToString();
            Timer.Value = na.StartTime;
            Timer.IsEnabled = false; // turn off slider

            na.IsRunning = true;

            btnPlayAnimtion.IsEnabled = false;
            btnPauseAnimation.IsEnabled = true;
            btnStopAnimation.IsEnabled = true;

            playAnimation();
        }

        private void playAnimation()
        {
            if (!networkAnimation.IsRunning)
                return;

            if (index >= networkAnimation.Events.Count)
            { // play done
                Timer.IsEnabled = true;
                return;
            }

            var e = networkAnimation.Events[index++];
            tbCurrentTime.Text = e.HappendTime.ToString();
            Timer.Value = e.HappendTime;
            SimulatePercent.Text = String.Format("{0:0.00}", (((double)index / (double)(networkAnimation.Events.Count)) * 100)) + "%";

            if (Parallelogram.Keys.Contains(e.Packet.Id))
            {
                if (Parallelogram[e.Packet.Id].ContainsKey(e.Node.ID))
                {
                    VisualizeParallelogram(new int[] { e.Packet.Id });
                }
            }

            var ele = Network.AddPacketPath(e.Packet, UcPacket.UcPacketCreateType.PLAY_ANIMATION);

            if (ele.GenerateEventAnimation(e, networkAnimation.Speed) == false)
            {
                playAnimation();
                return;
            }

            ele.OnAnimationCompleted -= OnAnimationCompleted;
            ele.OnAnimationCompleted += OnAnimationCompleted;

            ele.PlayAnimation();
        }

        private void OnAnimationCompleted(EventData e, PacketData packet)
        {
            //if (packet.EventList[packet.EventList.Count - 1] == e)
            //{
            //    Network.RemovePacketPath(packet.Id);
            //}
            playAnimation();
        }

        #endregion play animation

        public Dictionary<int, Dictionary<int, List<NodeData>>> Parallelogram { get; set; }

        public Dictionary<double, List<NodeData>> DynamicGridHole { get; set; }

        public Dictionary<double, int> AdjacentHole { get; set; }
    }
}