﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WisSim.Model;

namespace WisSim.Visualizer
{
    public class NetworkAnimation
    {
        private List<EventData> _events;

        public List<EventData> Events
        {
            get { return _events; }
            set
            {
                StartTime = value[0].HappendTime;
                EndTime = value[value.Count - 1].HappendTime;
                _events = value;
            }
        }

        private double _startTime;
        private double _endTime;
        private double _currentTime;

        public double StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        public double EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        public double CurrentTime
        {
            get { return _currentTime; }
            set { _currentTime = value; }
        }

        private double _speed;

        public double Speed
        {
            get
            {
                if (_speed == 0) return 1;
                return _speed;
            }
            set { _speed = value; }
        }

        private bool _isRunning;

        public bool IsRunning
        {
            get { return _isRunning; }
            set { _isRunning = value; }
        }
    }
}