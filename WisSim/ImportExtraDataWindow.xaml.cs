﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace WisSim
{
    /// <summary>
    /// Interaction logic for ImportData.xaml
    /// </summary>
    public partial class ImportExtraDataWindow : Window
    {
        private string boundHoleFile;

        public string BoundHoleFile
        {
            get { return boundHoleFile; }
            set { boundHoleFile = value; }
        }

        private int routingProtocolIndex = 0;

        public int RoutingProtocolIndex
        {
            get { return routingProtocolIndex; }
            set { routingProtocolIndex = value; }
        }

        private string adjacentHoleFile;

        public string AdjacentHoleFile
        {
            get { return adjacentHoleFile; }
            set { adjacentHoleFile = value; }
        }

        private string parallelogramFile;

        public string ParallelogramFile
        {
            get { return parallelogramFile; }
            set { parallelogramFile = value; }
        }

        public string RoutingProtocol
        {
            get
            {
                XmlElement elem = (XmlElement)chkBox_routingProtocol.SelectedItem;
                return elem.GetAttribute("type");
            }
        }

        private Microsoft.Win32.OpenFileDialog dlg;

        public ImportExtraDataWindow()
        {
            InitializeComponent();
        }

        public ImportExtraDataWindow(string defaultFolder)
        {
            InitializeComponent();
            boundHoleFile = defaultFolder;
            adjacentHoleFile = defaultFolder;
            tb_BoundHoleFilePath.Text = boundHoleFile;
            tb_AdjacentHoleFilePath.Text = adjacentHoleFile;
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_BoundHoleFilePath_Click(object sender, RoutedEventArgs e)
        {
            Nullable<bool> result = browserTraceFile();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                boundHoleFile = filename;
                tb_BoundHoleFilePath.Text = filename;
            }
        }

        private void btn_Next_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void btn_AdjacentHoleFilePath_Click(object sender, RoutedEventArgs e)
        {
            Nullable<bool> result = browserTraceFile();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                adjacentHoleFile = filename;
                tb_AdjacentHoleFilePath.Text = filename;
            }
        }

        private void btn_ParallelogramFilePath_Click(object sender, RoutedEventArgs e)
        {
            Nullable<bool> result = browserTraceFile();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                parallelogramFile = filename;
                tb_ParallelogramFilePath.Text = filename;
            }
        }

        private Nullable<bool> browserTraceFile()
        {
            if (dlg == null)
            {
                dlg = new Microsoft.Win32.OpenFileDialog();
                // Set filter for file extension and default file extension
                if (System.IO.Directory.Exists(adjacentHoleFile))
                {
                    dlg.InitialDirectory = adjacentHoleFile;
                }
            }
            else
            {
                dlg.InitialDirectory = dlg.FileName;
            }

            dlg.DefaultExt = ".tr";
            dlg.Filter = "trace Files (*.tr)|*.tr|All Files (*.*)|*.*";

            // Display OpenFileDialog by calling ShowDialog method
            return dlg.ShowDialog();
        }

        private void chkBox_routingProtocol_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            string selected = ((XmlElement)cb.SelectedItem).Attributes["type"].Value;
            stack_BoundHoleFile.Visibility = Visibility.Collapsed;
            stack_NeighboringFile.Visibility = Visibility.Collapsed;
            stack_ParallelogramFile.Visibility = Visibility.Collapsed;

            if (selected != "GPSR")
            {
                stack_BoundHoleFile.Visibility = Visibility.Visible;
                if (selected == "GRIDDYNAMIC")
                {
                    stack_NeighboringFile.Visibility = Visibility.Visible;
                }
                else if (selected == "ELBARGRIDOFFLINE")
                {
                    stack_ParallelogramFile.Visibility = Visibility.Visible;
                }
            }
        }
    }
}