﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WisSim
{
    /// <summary>
    /// Interaction logic for ImportData.xaml
    /// </summary>
    public partial class ImportDataWindow : Window
    {
        private string nodeFile;

        public string NodeFile
        {
            get { return nodeFile; }
            set { nodeFile = value; }
        }

        private string traceFile;

        public string TraceFile
        {
            get { return traceFile; }
            set { traceFile = value; }
        }

        private Microsoft.Win32.OpenFileDialog dlg;

        public ImportDataWindow()
        {
            InitializeComponent();
            //nodeFile = System.IO.Directory.GetCurrentDirectory();
            //traceFile = System.IO.Directory.GetCurrentDirectory();
            //tb_NodeFilePath.Text = nodeFile;
            //tb_TraceFilePath.Text = traceFile;
        }

        public ImportDataWindow(string defaultFolder)
        {
            InitializeComponent();
            nodeFile = defaultFolder;
            traceFile = defaultFolder;
            tb_NodeFilePath.Text = nodeFile;
            tb_TraceFilePath.Text = traceFile;
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_NodeFilePath_Click(object sender, RoutedEventArgs e)
        {
            if (dlg == null)
            {
                dlg = new Microsoft.Win32.OpenFileDialog();
                // Set filter for file extension and default file extension
                if (System.IO.Directory.Exists(nodeFile))
                {
                    dlg.InitialDirectory = nodeFile;
                }
            }
            else
            {
                dlg.InitialDirectory = dlg.FileName;
            }

            dlg.DefaultExt = ".tr";
            dlg.Filter = "trace Files (*.tr)|*.tr|All Files (*.*)|*.*";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                nodeFile = filename;
                tb_NodeFilePath.Text = filename;
            }
        }

        private void btn_TraceFilePath_Click(object sender, RoutedEventArgs e)
        {
            if (dlg == null)
            {
                dlg = new Microsoft.Win32.OpenFileDialog();
                // Set filter for file extension and default file extension
                if (System.IO.Directory.Exists(traceFile))
                {
                    dlg.InitialDirectory = traceFile;
                }
            }
            else
            {
                dlg.InitialDirectory = dlg.FileName;
            }

            dlg.DefaultExt = ".tr";
            dlg.Filter = "trace Files (*.tr)|*.tr|All Files (*.*)|*.*";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                traceFile = filename;
                tb_TraceFilePath.Text = filename;
            }
        }

        private void btn_Next_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}