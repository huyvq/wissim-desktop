﻿using Main.Controller.Connector;
using Microsoft.Win32;
using Ookii.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WisSim.Controller.Parser;
using WisSim.Model;
using WisSim.Visualizer;
using WissimConnector;

namespace WisSim
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ClientServerConnector clientServerConnector = new ClientServerConnector();

        public MainWindow()
        {
            //MessageBox.Show(Assembly.GetExecutingAssembly().GetName().Version.ToString());

            // OpenFile Wissim setting to get server information...
            GetConnection();

            InitializeComponent();

            // Initialize Project
            string[] commandLineArgs = Environment.GetCommandLineArgs();
            if (commandLineArgs.Length == 2)
            {
                try
                {
                    //MessageBox.Show(commandLineArgs[1]);
                    Project = Project.OpenFile(commandLineArgs[1]);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Open project false");
                    Project = new Project();
                }
            }
            else
            {
                Project = new Project();
            }
            Project.PropertyChanged += Project_PropertyChanged;
            //MessageBox.Show("", "Finish loading",MessageBoxButton.OKCancel);

            // Initialize Editor
            Editor.Editor editor = new Editor.Editor(this);
            editor.GroupAdded += OnGroupAdded;
            editor.GroupDeleted += OnGroupDeleted;
            editor.GroupChanged += OnGroupChanged;
            EditorTab.Children.Add(editor);

            //MessageBox.Show("", "Finish Editor");

            // Initialize Visualizer
            this.visualzier.Main = this;
            this.analyser.Main = this;
            this.visualzier.GroupAdded += OnGroupAdded;
            this.visualzier.GroupDeleted += OnGroupDeleted;
            this.Groups = new Dictionary<string, NodesGroup>();

            foreach (string groupName in this.Project.Network.GroupsList.Keys)
            {
                List<Node> aGroup = this.Project.Network.GroupsList[groupName];
                NodesGroup groupV = new NodesGroup();
                foreach (Node aNode in aGroup)
                {
                    groupV.Nodes.Add(aNode.ID);
                }
                groupV.Name = groupName;
                OnGroupAdded(this, new NodesGroupEventArgs(groupV));
            }

            //MessageBox.Show("", "Finish Visualizer");
            this.WindowState = WindowState.Maximized;
            this.Title = "WisSim | " + Project.Name;

            // GUI
            enableMenuItemOnStart();
            //contentTab.Visibility = Visibility.Hidden;
            statusBarMessage.Text = "Welcome to wissim";
            //MessageBox.Show("Finish GUI 1", "Finish GUI 1");
            // Backgound worker to call Simulation funtion form server
            runSimulationWorker.DoWork += RunSimulation;
            runSimulationWorker.RunWorkerCompleted += RunSimulationFeedBack;

            //Background worker to call Parser function from NS2Parser component;
            parseWorker.DoWork += ParseFiles;
            //ns2Parser = new NS2Parser();
            //MessageBox.Show("Finish GUI ", "Finish GUI");
        }

        private void Project_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.Title = "WisSim | " + Project.Name + " *";
        }

        public Project Project { get; private set; }

        public string WorkingDirectory
        {
            get { return Project.WorkingDirectory; }
            private set { Project.WorkingDirectory = value; }
        }

        public TraceData traceData;
        public Dictionary<string, NodesGroup> Groups;

        #region EventHHander Method

        private void menuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Project.IsModify == true)
            {
                if (SaveProjectAsWithAsking() == false)
                {
                    e.Cancel = true;
                    return;
                }
            }
            this.clientServerConnector.Close();
            Environment.Exit(Environment.ExitCode);
        }

        private void menuItemNewProject_Click(object sender, RoutedEventArgs e)
        {
            NewInstance();
        }

        private void menuItemOpenProject_Click(object sender, RoutedEventArgs e)
        {
            //DoAction here
            OpenProject();
        }

        private void menuItemSaveProject_Click(object sender, RoutedEventArgs e)
        {
            SaveProject();
        }

        private void menuItemSaveProjectAs_Click(object sender, RoutedEventArgs e)
        {
            SaveProjectAsWisFile(String.IsNullOrEmpty(Project.Name) ? "" : Project.Name);
        }

        private void menuItemImportTraceData_Click(object sender, RoutedEventArgs e)
        {
            enableMenuItemOnHasProject();

            contentTab.Visibility = Visibility.Visible;

            //If project is enable --> enable EditorView
            //else disable
            ParseTraceFile("", "");
        }

        private void menuItemExport_Click(object sender, RoutedEventArgs e)
        {
            InputDialog i = new InputDialog();
            i.MainInstruction = "Please enter session ID";
            i.WindowTitle = "Get trace files";
            if (!String.IsNullOrEmpty(Project.SectionId))
            {
                i.Input = Project.SectionId;
            }
            if (i.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!String.IsNullOrEmpty(i.Input))
                {
                    GetTraceFiles(i.Input);
                }
            }
        }

        private void menuItemcheckStatus_Click(object sender, RoutedEventArgs e)
        {
            InputDialog i = new InputDialog();
            i.MainInstruction = "Please enter session ID";
            i.WindowTitle = "Get Running Status";
            if (!String.IsNullOrEmpty(Project.SectionId))
            {
                i.Input = Project.SectionId;
            }
            if (i.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!String.IsNullOrEmpty(i.Input))
                {
                    CheckSessionStatus(i.Input);
                }
            }
        }

        private void menuItemViewEditor_Click(object sender, RoutedEventArgs e)
        {
            tabItemEditor.IsSelected = true;
        }

        private void menuItemViewVisualizer_Click(object sender, RoutedEventArgs e)
        {
            tabItemVisualizer.IsSelected = true;
        }

        private void menuItemViewAnalyzer_Click(object sender, RoutedEventArgs e)
        {
            selectAnalyzerTab();
        }

        private void contentTab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (tabItemEditor.IsSelected == true)
            //{
            //    menuItemViewEditor.IsChecked = true;
            //    menuItemViewAnalyzer.IsChecked = false;
            //    menuItemViewVisualizer.IsChecked = false;
            //}
            //if (tabItemVisualizer.IsSelected == true)
            //{
            //    menuItemViewEditor.IsChecked = false;
            //    menuItemViewAnalyzer.IsChecked = false;
            //    menuItemViewVisualizer.IsChecked = true;
            //}

            //if (tabItemAnalyzer.IsSelected == true)
            //{
            //    menuItemViewEditor.IsChecked = false;
            //    menuItemViewAnalyzer.IsChecked = true;
            //    menuItemViewVisualizer.IsChecked = false;
            //}
        }

        private void menuItemOpenWorkingDirectory_Click(object sender, RoutedEventArgs e)
        {
            Process process = new Process();
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.FileName = Project.WorkingDirectory;
            process.Start();
        }

        #endregion EventHHander Method

        #region Private Method

        private void enableMenuItemOnHasProject()
        {
            menuItemNewProject.IsEnabled = false;
            menuItemOpenProject.IsEnabled = false;
            menuItemImportTraceData.IsEnabled = true;
            menuItemSaveProject.IsEnabled = true;
            menuItemDownloadTraceData.IsEnabled = true;
            menuItemView.IsEnabled = true;
        }

        private void enableMenuItemOnStart()
        {
            menuItemNewProject.IsEnabled = true;
            menuItemOpenProject.IsEnabled = true;
            menuItemSaveProject.IsEnabled = true;
            menuItemDownloadTraceData.IsEnabled = true;
            menuItemImportTraceData.IsEnabled = true;
            menuItemView.IsEnabled = false;
        }

        private void selectAnalyzerTab()
        {
            tabItemAnalyzer.IsSelected = true;
        }

        private void selectVisualiserTab()
        {
            tabItemVisualizer.IsSelected = true;

            visualzier.networkHeight.Value = Project.Network.Height.ToString();
            visualzier.networkWidth.Value = Project.Network.Width.ToString();
        }

        #endregion Private Method

        #region Public Method

        public string StatusBarMessage
        {
            get { return statusBarMessage.Text; }
            set
            {
                this.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    (Action<string>)((s) => { statusBarMessage.Text = s; }),
                    value);
            }
        }

        public string GetSimulationSetting()
        {
            //clientServerConnector.connectToServer();
            AbtractResult result = clientServerConnector.getNetworkSetting();
            return result.Result;
        }

        public string GetThreshold(string parameter)
        {
            // todo: Run Threshold
            /* Process p = Runtime.getRuntime().exec(
             *      <NS2Path> +
             *      "/ns-2.35/indep-utils/propagation/threshold -m " +
             *      <parameter>
             *      );
             */
            AbtractResult result = clientServerConnector.getNetworkSetting();
            return result.Result;
        }

        #endregion Public Method

        #region Run simulation

        private readonly BackgroundWorker runSimulationWorker = new BackgroundWorker();

        public void RunSimulation(params string[] filesPath)
        {
            runSimulationWorker.RunWorkerAsync(filesPath);
        }

        private void RunSimulation(object sender, DoWorkEventArgs e)
        {
            try
            {
                AbtractResult result = clientServerConnector.runSimulation(WorkingDirectory, (string[])e.Argument, ((string[])e.Argument)[0]);
                if (result.StatusCode == 0)
                {
                    e.Result = result.Result;
                }
                else
                {
                    MessageBox.Show(result.Result, "Running simulation failure!");
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                StatusBarMessage = "Running simulation failure, " + ex.Message;
                MessageBox.Show(ex.Message, "Running simulation failure!");
                e.Cancel = true;
            }
        }

        private void RunSimulationFeedBack(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == false)
            {
                this.Project.SectionId = e.Result.ToString();
                SaveProject();

                MessageBoxResult messageBoxResult = MessageBox.Show("Do you want to save this session and work on new project simulation?", "Session: " + this.Project.SectionId, MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    this.Project.SectionId = "";
                    SaveProjectAsWisFile("");
                    return;
                }

                // Other cases: Continue to check status.
                CheckSessionStatus(this.Project.SectionId);
            }
        }

        #endregion Run simulation

        #region CheckSessionStatus

        public void CheckSessionStatus(string sessionID)
        {
            // Disable user to do action
            mainPannel.IsEnabled = false;

            //Update status and Progress bar
            statusBarMessage.Text = "Get status of session " + sessionID;
            statusBarProgress.Visibility = Visibility.Visible;
            statusBarProgress.IsIndeterminate = true;

            BackgroundWorker checkSessionStatusWorker = new BackgroundWorker();
            checkSessionStatusWorker.DoWork += CheckSessionStatus;
            checkSessionStatusWorker.ProgressChanged += checkSessionStatusWorker_ProgressChanged;
            checkSessionStatusWorker.WorkerReportsProgress = true;
            checkSessionStatusWorker.RunWorkerCompleted += FinishCheckSessionStatus;
            checkSessionStatusWorker.RunWorkerAsync(sessionID);
        }

        private void CheckSessionStatus(object sender, DoWorkEventArgs e)
        {
            DateTime start = DateTime.Now;
            BackgroundWorker worker = (BackgroundWorker)sender;
            string sessionId = e.Argument.ToString();
            int waittingTime = 2000; //(milisecond)
            while (true)
            {
                //Getting status
                try
                {
                    AbtractResult absResult = clientServerConnector.getSessionStatus(sessionId);
                    string sessionStatus = absResult.Result;

                    if (sessionStatus == "FINISH")
                    {
                        e.Result = sessionId;
                        break;
                    }

                    DateTime now = DateTime.Now;
                    worker.ReportProgress(0, now.Subtract(start).TotalSeconds);
                    System.Threading.Thread.Sleep(waittingTime);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    e.Cancel = true;
                    break;
                }
            }
        }

        private void checkSessionStatusWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0)
            {
                statusBarMessage.Text = "Get status of session " + this.Project.SectionId + ": " + e.UserState.ToString();
            }
        }

        private void FinishCheckSessionStatus(object sender, RunWorkerCompletedEventArgs e)
        {
            // Enable user action
            mainPannel.IsEnabled = true;
            statusBarProgress.Visibility = Visibility.Hidden;

            if (!e.Cancelled)
            {
                GetTraceFiles(e.Result.ToString());
            }
        }

        private void GetTraceFiles(string sessioId)
        {
            //Update status and Progress bar
            statusBarMessage.Text = "Get trace files of session " + sessioId;
            statusBarProgress.Visibility = Visibility.Visible;
            statusBarProgress.IsIndeterminate = true;

            BackgroundWorker getTraceFileStatusWorker = new BackgroundWorker();
            getTraceFileStatusWorker.DoWork += getTraceFileStatusWorker_Run;
            getTraceFileStatusWorker.RunWorkerCompleted += getTraceFileStatusWorker_Finished;
            getTraceFileStatusWorker.RunWorkerAsync(sessioId);
        }

        private void getTraceFileStatusWorker_Run(object sender, DoWorkEventArgs e)
        {
            string sessionId = e.Argument.ToString();
            string start_time = DateTime.Now.ToString("hh_mm_ss.fff");
            Console.WriteLine(start_time);
            try
            {
                AbtractResult result = clientServerConnector.getTraceFile(sessionId, WorkingDirectory);
                e.Result = WorkingDirectory + @"\trace\" + sessionId + @"\";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                e.Cancel = true;
            }

            string end_time = DateTime.Now.ToString("hh_mm_ss.fff");
            Console.WriteLine(end_time);
        }

        private void getTraceFileStatusWorker_Finished(object sender, RunWorkerCompletedEventArgs e)
        {
            // Enable user action
            mainPannel.IsEnabled = true;
            statusBarProgress.Visibility = Visibility.Hidden;
            if (e.Cancelled == false)
            {
                string directory = e.Result.ToString();
                Debug.WriteLine("[Debug]" + directory);
                //TODO: Debug tai sao directory lai khong tro dung vao working folder
                string[] traceFilePath = new string[2];
                string neighbourFile = System.IO.Path.Combine(directory, "Neighbors.tr");
                string mainfile = System.IO.Path.Combine(directory, "Trace.tr");
                if (System.IO.File.Exists(neighbourFile) && System.IO.File.Exists(mainfile))
                {
                    traceFilePath[0] = NS2Parser.NODE_FILE_PREFIX + "-" + neighbourFile;
                    traceFilePath[1] = NS2Parser.TRACE_FILE_PREFIX + "-" + mainfile;
                    ParseFiles(traceFilePath);
                }
                else
                {
                    ParseTraceFile(System.IO.Path.Combine(directory, "Neighbors.tr"), System.IO.Path.Combine(directory, "Trace.tr"));
                }
            }
        }

        #endregion CheckSessionStatus

        #region Parse Trace File

        public void ClearTraceData()
        {
            this.analyser.ClearTraceData();
            traceData.Nodes.Clear();
            traceData.Packets.Clear();
            traceData.Events.Clear();
            traceData = null;
            GC.Collect();
        }

        private readonly BackgroundWorker parseWorker = new BackgroundWorker { WorkerReportsProgress = true };

        public void setParseWorkerCallback(RunWorkerCompletedEventHandler handler)
        {
            parseWorker.RunWorkerCompleted += handler;
        }

        public void unsetParseWorkerCallback(RunWorkerCompletedEventHandler handler)
        {
            parseWorker.RunWorkerCompleted -= handler;

            //Update status and Progress bar
            statusBarMessage.Text = "Finish parsing trace files";
            statusBarProgress.Visibility = Visibility.Hidden;
            statusBarProgress.IsIndeterminate = false;
            // Enable user action
            mainPannel.IsEnabled = true;
        }

        public void ParseTraceFile(string mainfile, string neighbourFile)
        {
            ImportDataWindow w = new ImportDataWindow();
            w.NodeFile = neighbourFile;
            w.TraceFile = mainfile;
            if (w.ShowDialog() == true)
            {
                //Parse files
                string[] traceFilePath = new string[2];
                traceFilePath[0] = NS2Parser.NODE_FILE_PREFIX + "-" + w.NodeFile;
                traceFilePath[1] = NS2Parser.TRACE_FILE_PREFIX + "-" + w.TraceFile;
                ParseFiles(traceFilePath);
            }
        }

        public void ParseFiles(params string[] traceFilePath)
        {
            // Disable user to do action
            mainPannel.IsEnabled = false;

            //Clear current data
            //ClearTraceData();

            parseFilesBackground(traceFilePath, FinishParseFiles);
        }

        public void parseFilesBackground(string[] traceFiles, RunWorkerCompletedEventHandler handler)
        {
            setParseWorkerCallback(handler);
            //Update status and Progress bar
            statusBarMessage.Text = "Start parsing trace files";
            statusBarProgress.Visibility = Visibility.Visible;
            statusBarProgress.IsIndeterminate = true;
            parseWorker.RunWorkerAsync(traceFiles);
        }

        public void ParseFiles(object sender, DoWorkEventArgs e)
        {
            try
            {
                NS2Parser ns2parser = new NS2Parser();
                ns2parser.Reset();
                ns2parser.Parse((string[])e.Argument);

                e.Result = ns2parser;
                e.Cancel = false;
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void FinishParseFiles(object sender, RunWorkerCompletedEventArgs e)
        {
            unsetParseWorkerCallback(FinishParseFiles);

            // get result
            if (!e.Cancelled)
            {
                NS2Parser ns2Parser = (NS2Parser)e.Result;
                traceData = new TraceData();
                traceData.Nodes = ns2Parser.GetNodes();
                traceData.Packets = ns2Parser.GetPackets();
                traceData.Events = ns2Parser.GetEvents();
                traceData.SimulateEvents = ns2Parser.GetSimulateEvents();
                traceData.PacketTypes = ns2Parser.GetPacketTypes();
                //ns2Parser.Reset();
                ns2Parser = null;
                //Binding data for Visualiser and Analyser
                analyser.BindingData(this.traceData);
                visualzier.BindingData(this.traceData);
            }

            // Change to Visualizer and Analyzer
            selectVisualiserTab();
        }

        public TraceData GetTraceData()
        {
            return traceData;
        }

        #endregion Parse Trace File

        #region Create Connection

        private void GetConnection()
        {
            //clientServerConnector.ServerAddress = "winsim.cloudapp.net";
            //clientServerConnector.ServerAddress = "10.42.0.1";
            //clientServerConnector.ServerPort = 58250;

            BackgroundWorker getConnectionwWorker = new BackgroundWorker();
            getConnectionwWorker.DoWork += (sender, arg) =>
            {
                string url = @"http://sedic.soict.hust.edu.vn/wissim/Config.xml";
                XmlDocument xml = new XmlDocument();
                xml.Load(url);

                foreach (XmlNode node in xml.DocumentElement.GetElementsByTagName("server"))
                {
                    clientServerConnector.ServerAddress = node.Attributes["ip"].Value;
                    clientServerConnector.ServerPort = int.Parse(node.Attributes["port"].Value);
                    try
                    {
                        // try connecting to server
                        clientServerConnector.connectToServer();
                        return;
                    }
                    catch (Exception e)
                    {
                        // next server
                    }
                }
            };
            getConnectionwWorker.RunWorkerAsync();
        }

        #endregion Create Connection

        #region Analyser and Visualizer interoperation

        public void ShowPacketPath(int packetId)
        {
            visualzier.ShowPacketPath(packetId);
            selectVisualiserTab();
        }

        public void ShowMultiPacketPath(List<int> packetIds)
        {
            visualzier.ShowMultiPacketPath(packetIds);
            selectVisualiserTab();
        }

        public void PlayAnimation(NetworkAnimation na)
        {
            visualzier.PlayAnimation(na);
            selectVisualiserTab();
        }

        #region groupList

        public void OnGroupDeleted(object sender, NodesGroupEventArgs e)
        {
            NodesGroup group = e.getNodeGroup();
            if (this.Groups.Keys.Contains(group.Name))
            {
                Groups.Remove(group.Name);
                analyser.UpdateGroup();
                visualzier.UpdateGroupVisulaize();
            }
        }

        public void OnGroupChanged(object sender, NodesGroupsChangedEventArgs e)
        {
            this.Groups.Clear();
            foreach (NodesGroup group in e.getNodeGroups())
            {
                Groups.Add(group.Name, group);
                analyser.UpdateGroup();
                visualzier.UpdateGroupVisulaize();
            }
        }

        public void OnGroupAdded(object sender, NodesGroupEventArgs e)
        {
            int i = 0;
            NodesGroup group = e.getNodeGroup();
            string newName = group.Name;
            while (this.Groups.Keys.Contains(newName))
            {
                i++;
                newName = group.Name + i.ToString();
            }
            if (newName != group.Name)
            {
                MessageBox.Show("Group name '" + group.Name + "' is available  Add new group with name '" + newName + "'");
                group.Name = newName;
            }
            Groups.Add(newName, group);
            analyser.UpdateGroup();
            visualzier.UpdateGroupVisulaize();
        }

        #endregion groupList

        #endregion Analyser and Visualizer interoperation

        #region project .wis file

        public void SaveProjectAsWisFile(string defaultFileName)
        {
            SaveFileDialog sdl = new SaveFileDialog();
            sdl.InitialDirectory = this.WorkingDirectory;

            sdl.DefaultExt = ".wis";
            sdl.Filter = "wissim Files (*.wis)|*.wis|All Files (*.*)|*.*";
            sdl.FilterIndex = 1;
            sdl.FileName = defaultFileName;

            if (sdl.ShowDialog() == true)
            {
                Project.Save(sdl.FileName);
                this.Title = "WisSim | " + Project.Name;
                StatusBarMessage = "Saved";
            }
        }

        public void SaveProject()
        {
            if (String.IsNullOrEmpty(Project.Name) || Project.Name == "new Project")
            {
                //Never save before

                SaveProjectAsWisFile("");
            }
            else
            {
                if (Project.IsModify == null)
                {
                    //do not edit anything
                    SaveProjectAsWisFile(Project.Name);
                }
                else
                {
                    Project.Save(Project.ProjectNamePath);
                    this.Title = "WisSim | " + Project.Name;
                }
            }
        }

        public bool SaveProjectAsWithAsking()
        {
            MessageBoxResult isSave = MessageBox.Show("Do you want to save current project", "Save project", MessageBoxButton.YesNoCancel);
            if (isSave == MessageBoxResult.Yes)
            {
                SaveProjectAsWisFile(String.IsNullOrEmpty(Project.Name) ? "" : Project.Name);
            }
            else if (isSave == MessageBoxResult.Cancel)
            {
                return false;
            }
            return true;
        }

        public void OpenProject()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            //dlg.InitialDirectory = Directory.GetCurrentDirectory();
            dlg.DefaultExt = ".wis";
            dlg.Filter = "wissim Files (*.wis)|*.wis|All Files (*.*)|*.*";
            dlg.FilterIndex = 1;

            if (dlg.ShowDialog() == true)
            {
                NewInstance(dlg.FileName);
                if (Project.IsModify == null)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        /// <summary>
        /// Create new Instance of Wissim
        /// </summary>
        /// <param name="argument">argument of new instance</param>
        private void NewInstance(string argument = null)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.FileName = System.Reflection.Assembly.GetExecutingAssembly().Location;
                if (argument != null)
                {
                    p.StartInfo.Arguments = argument;
                }
                p.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Cannot open new window");
            }
        }

        //private void ManualLoad()
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.OpenFile(dlg.FileName);
        //    XmlElement rootElement = doc.DocumentElement;
        //    //2. TODO: Clear old project data

        //    //3. OpenFile new project info
        //    //3.1 Network
        //    XmlElement networkElement = (XmlElement)rootElement.GetElementsByTagName("Network")[0];
        //    this.Editor.setNetworkHeight(networkElement.GetAttribute("Height"));
        //    this.Editor.setNetworkWidth(networkElement.GetAttribute("Width"));
        //    this.Editor.Network.Range = System.Convert.ToInt32(networkElement.GetAttribute("NodeRange"));

        //    //3.2 Nodes
        //    XmlNodeList nodeElements = networkElement.GetElementsByTagName("Node");
        //    foreach (XmlNode node in nodeElements)
        //    {
        //        XmlElement nodeElement = (XmlElement)node;
        //        double x = System.Convert.ToDouble(nodeElement.GetAttribute("X"));
        //        double y = System.Convert.ToDouble(nodeElement.GetAttribute("Y"));
        //        this.Editor.Network.AddNode(x, y);
        //    }

        //    //3.3 Connection
        //    XmlElement connectionElement = (XmlElement)rootElement.GetElementsByTagName("Connection")[0];

        //    //3.4. Setting

        //    //3.5 Simulation information
        //}

        //private int ManualSave(string defaultFileName)
        //{
        //    Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
        //    if (this.workingDirectory != null)
        //    {
        //        dlg.InitialDirectory = this.workingDirectory;
        //    }
        //    else
        //    {
        //        dlg.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
        //    }
        //    dlg.DefaultExt = ".wis";
        //    dlg.Filter = "wissim Files (*.wis)|*.wis|All Files (*.*)|*.*";
        //    dlg.FilterIndex = 1;
        //    dlg.FileName = defaultFileName; //System.IO.Path.Combine(dlg.InitialDirectory, defaultFileName);
        //    if (dlg.ShowDialog() == true)
        //    {
        //        //Save default working directory
        //        string defaultDirectory = System.IO.Path.GetDirectoryName(dlg.FileName);

        //        //Save to .wis file and then continue.
        //        XmlDocument doc = new XmlDocument();
        //        XmlElement bodyElement = doc.CreateElement(string.Empty, "Body", string.Empty);
        //        doc.AppendChild(bodyElement);
        //        //1.Network information
        //        XmlElement networkElement = doc.CreateElement(string.Empty, "Network", string.Empty);
        //        networkElement.SetAttribute("Height", Editor.Network.Height.ToString());
        //        networkElement.SetAttribute("Width", Editor.Network.Width.ToString());
        //        networkElement.SetAttribute("NodeRange", Editor.Network.Range.ToString());
        //        networkElement.SetAttribute("NodeCount", Editor.Network.NodeList.Count.ToString());
        //        bodyElement.AppendChild(networkElement);

        //        //2. Nodes information
        //        foreach (WisSim.NetworkComponents.INode node in Editor.Network.NodeList)
        //        {
        //            XmlElement nodeElement = doc.CreateElement(string.Empty, "Node", string.Empty);
        //            nodeElement.SetAttribute("ID", node.ID.ToString());
        //            nodeElement.SetAttribute("X", node.X.ToString());
        //            nodeElement.SetAttribute("Y", node.Y.ToString());
        //            networkElement.AppendChild(nodeElement);
        //        }

        //        //3. Connection information
        //        XmlElement connectionElement = doc.CreateElement(string.Empty, "Connection", string.Empty);
        //        bodyElement.AppendChild(connectionElement);
        //        foreach (UcConnect connection in Editor.Network.Connects)
        //        {
        //            XmlElement connectElement = doc.CreateElement(string.Empty, "Connect", string.Empty);
        //            connectElement.SetAttribute("Source", connection.node1.ID.ToString());
        //            connectElement.SetAttribute("Destination", connection.node2.ID.ToString());
        //            connectionElement.AppendChild(connectElement);
        //        }

        //        //3. Setting information
        //        //<setting element> See Editor > DefaultSetting for more information
        //        XmlNode settingElement = doc.ImportNode(Editor.SettingData.Document.DocumentElement, true);
        //        bodyElement.AppendChild(settingElement);

        //        //4. Tcl information
        //        /*
        //        XmlElement tclElement = doc.CreateElement(string.Empty, "Tcl", string.Empty);
        //        tclElement.SetAttribute("simulation", System.IO.Path.Combine(WorkingDirectory, @"/simulate.tcl"));
        //        */

        //        //5. Simulation information
        //        XmlElement sessionElement = doc.CreateElement(string.Empty, "Simulation", string.Empty);
        //        sessionElement.SetAttribute("WorkingFolder", defaultDirectory);
        //        sessionElement.SetAttribute("SimulationTime", Editor.Network.SimulationTime.ToString());
        //        sessionElement.SetAttribute("sessionID", this.simulationSessionID);
        //        sessionElement.SetAttribute("server", this.clientServerConnector.ServerAddress);
        //        sessionElement.SetAttribute("port", this.clientServerConnector.ServerPort.ToString());
        //        bodyElement.AppendChild(sessionElement);
        //        //Save file
        //        doc.Save(dlg.FileName);
        //        return 1;
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}

        #endregion project .wis file
    }
}