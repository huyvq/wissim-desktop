﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Model
{
    public class BoundPoint
    {
        private double _radius;
        private double _degree;
        private double _x;
        private double _y;

        public double X
        {
            get { return _x; }
            set { _x = value; }
        }

        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public BoundPoint(double a1, double a2, CoordinateType type)
        {
            if (type == CoordinateType.POLAR)
            {
                this._degree = a1;
                this._radius = a2;
                this._x = this._radius * Math.Cos(this._degree);
                this._y = this._radius * Math.Sin(this._degree);
            }
            else if (type == CoordinateType.CARTESIAN)
            {
                this._x = a1;
                this._y = a2;
                this._radius = Math.Sqrt(this._x * this._x + this._y * this._y);
                this._degree = Math.Asin(_y / _radius);
                if (_x < 0) this._degree = Math.PI - this._degree;
                if (this._degree < 0)
                {
                    this._degree += 2 * Math.PI;
                }
            }
        }

        public void addRadius(double amount)
        {
            this._radius += amount;
            this._x = this._radius * Math.Cos(this._degree);
            this._y = this._radius * Math.Sin(this._degree);
        }

        public void setRadius(double radius)
        {
            this._radius = radius;
            this._x = this._radius * Math.Cos(this._degree);
            this._y = this._radius * Math.Sin(this._degree);
        }

        public void multiRadius(double ratio)
        {
            this._radius *= ratio;
            this._x = this._radius * Math.Cos(this._degree);
            this._y = this._radius * Math.Sin(this._degree);
        }

        public double Degree
        {
            get
            {
                return _degree;
            }
        }

        public double Radius
        {
            get
            {
                return _radius;
            }
        }

        public double DistanceTo(BoundPoint bp)
        {
            double a = _x - bp.X;
            double b = _y - bp.Y;
            return Math.Sqrt(a * a + b * b);
        }

        public String toString()
        {
            return _degree + ", " + _radius;
        }

        public double getX()
        {
            return _x;
        }

        public double getY()
        {
            return _y;
        }

        public enum CoordinateType
        {
            CARTESIAN,
            POLAR
        }
    }
}