﻿using HoleGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Helper
{
    public class MathHelper
    {
        /// <summary>
        /// Tests if two line segments intersect or not.
        /// The orientation of each line to other line's endpoints is used to determine
        /// the intersection.
        /// </summary>
        /// <returns>True if the lines intersect each other.</returns>
        public static bool IsSegmentsIntersect(BoundPoint a1, BoundPoint a2, BoundPoint b1, BoundPoint b2)
        {
            bool lines_intersect, segments_intersect;
            BoundPoint intersect;
            FindIntersect(a1, a2, b1, b2, out lines_intersect, out segments_intersect, out intersect);
            return segments_intersect;
        }

        private static void FindIntersect(
                BoundPoint p1, BoundPoint p2, BoundPoint p3, BoundPoint p4,
                out bool lines_intersect, out bool segments_intersect,
                out BoundPoint intersection)
        {
            // Get the segments' parameters.
            double dx12 = p2.X - p1.X;
            double dy12 = p2.Y - p1.Y;
            double dx34 = p4.X - p3.X;
            double dy34 = p4.Y - p3.Y;

            // Solve for t1 and t2
            double denominator = (dy12 * dx34 - dx12 * dy34);

            double t1 =
                ((p1.X - p3.X) * dy34 + (p3.Y - p1.Y) * dx34)
                    / denominator;
            if (double.IsInfinity(t1))
            {
                // The lines are parallel (or close enough to it).
                lines_intersect = false;
                segments_intersect = false;
                intersection = new DPoint(double.NaN, double.NaN);
                return;
            }
            lines_intersect = true;

            double t2 =
                ((p3.X - p1.X) * dy12 + (p1.Y - p3.Y) * dx12)
                    / -denominator;

            // Find the point of intersection.
            intersection = new DPoint(p1.X + dx12 * t1, p1.Y + dy12 * t1);

            // The segments intersect if t1 and t2 are between 0 and 1.
            segments_intersect =
                ((t1 >= 0) && (t1 <= 1) &&
                 (t2 >= 0) && (t2 <= 1));
        }
    }
}