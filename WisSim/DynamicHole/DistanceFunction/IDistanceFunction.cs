﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.DistanceFunction
{
    public abstract class IDistanceFunction
    {
        //public abstract double calcDistance(double time, double limitTime, double limitDistance);

        public abstract double calcTime(double distance, double limitDistance, double limitTime);
    }
}