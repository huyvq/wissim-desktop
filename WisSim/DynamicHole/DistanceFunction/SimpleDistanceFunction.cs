﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.DistanceFunction
{
    // y = kx
    public class SimpleDistanceFunction : IDistanceFunction
    {
        //public override double calcDistance(double time, double limitTime, double limitDistance)
        //{
        //    double r = time / limitTime * limitDistance;
        //    if (r > limitDistance) return limitDistance;
        //    else return r;
        //}

        public override double calcTime(double distance, double limitDistance, double limitTime)
        {
            if (distance > limitDistance) return double.NaN;

            return distance / limitDistance * limitTime;
        }
    }
}