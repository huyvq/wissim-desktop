﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.RadiusCaculate
{
    public abstract class IRadiusCalculate
    {
        public abstract double calc_r_byTime(double time);

        public abstract double calc_R_byTime(double time);

        public abstract double last_r(double time);

        protected double _timeLimit;
        protected double _deltaTime;

        public double TimeLimit
        {
            get { return _timeLimit; }
            private set { _timeLimit = value; }
        }

        public double DeltaTime
        {
            get { return _deltaTime; }
            private set { _deltaTime = value; }
        }
    }
}