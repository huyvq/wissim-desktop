﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.RadiusCaculate
{
    public class LinearRadiusByTime : IRadiusCalculate
    {
        private double r;
        private double R;

        public LinearRadiusByTime(double r, double R, double totalTime, double deltaTime)
        {
            this.r = r;
            this.R = R;
            _timeLimit = totalTime;
            _deltaTime = deltaTime;
        }

        public override double calc_r_byTime(double time)
        {
            //if (time >= _timeLimit) return r;
            return r * time / _timeLimit;
        }

        public override double calc_R_byTime(double time)
        {
            //if (time >= _timeLimit) return R;
            return R * time / _timeLimit;
        }

        public override double last_r(double time)
        {
            if (time > _timeLimit) return r;
            return r * (time - _deltaTime) / _timeLimit;
        }
    }
}