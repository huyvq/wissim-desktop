﻿using HoleGenerator.DistanceFunction;
using HoleGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WisSim.DynamicHole.DataModel
{
    public abstract class IHole
    {
        public double LifeTime { get; set; }

        public double _deltaTime = 1;

        public double DeltaTime
        {
            get { return _deltaTime; }
            set { _deltaTime = value; }
        }

        // last bound hole (at end of lifetime)
        protected List<BoundPoint> _lastBoundPoints = new List<BoundPoint>();

        public List<BoundPoint> LastBoundPoints { get { return _lastBoundPoints; } }

        // list distance function for each point on bound hole
        protected List<SimpleDistanceFunction> _distanceFuncs = new List<SimpleDistanceFunction>();

        public List<SimpleDistanceFunction> DistanceFuncs { get { return _distanceFuncs; } }

        // hole's center
        public DPoint Center { get; set; }

        /// <summary>
        /// check if hole cover point p at CurrentTime
        /// </summary>
        /// <param name="p"></param>
        public abstract bool IsCoverPoint(DPoint p, double time);

        /// <summary>
        /// Get current bound hole
        /// </summary>
        /// <returns></returns>
        public abstract List<BoundPoint> GetCurrentBoundHole();

        /// <summary>
        /// Get time that point p go off
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public abstract double PointOffTime(DPoint p);
    }
}