﻿using HoleGenerator.Distribution;
using HoleGenerator.Model;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace WisSim.DynamicHole.DataModel
{
    public class HoleViewModel
    {
        #region Properties

        private List<List<BoundPoint>> boundData;
        private Distribution _distribution;
        private double _timeLimit;
        private double _deltaTime;

        private DPoint _center;

        #endregion Properties

        public HoleViewModel(DPoint center, Distribution distribution)
        {
            _center = center;
            _distribution = distribution;
            _timeLimit = distribution.TimeLimit;
            _deltaTime = distribution.DeltaTime;

            GenerateData();
        }

        public double PointOffTime(DPoint p)
        {
            DPoint convert = new DPoint(p.getX() - _center.getX(), p.getY() - _center.getY());
            double time = 1;
            foreach (var list in boundData)
            {
                if (f(p, list))
                    return time / 10;
                time += _deltaTime;
            }
            return double.NaN;
        }

        private void GenerateData()
        {
            boundData = new List<List<BoundPoint>>();
            for (double i = 0; i < _timeLimit; i += _deltaTime)
            {
                _distribution.generate(i + _deltaTime);

                List<BoundPoint> boundList = new List<BoundPoint>();
                foreach (var p in _distribution.getBoundPoints())
                {
                    boundList.Add(new BoundPoint(p.Degree, p.Radius, BoundPoint.CoordinateType.POLAR));
                }
                boundData.Add(boundList);
            }
        }

        /// <summary>
        /// check if point is private inside triangle private created from O(0,0) and 2 other points using Barycentric coordinate system
        /// </summary>
        /// <param name="p"></param>
        /// <param name="boundPoints"></param>
        /// <returns></returns>
        private bool f(DPoint p, List<BoundPoint> boundPoints)
        {
            BoundPoint p1 = boundPoints[0];
            BoundPoint p2 = boundPoints[1];
            BoundPoint p3 = new BoundPoint(0, 0, BoundPoint.CoordinateType.CARTESIAN);
            for (int i = 0; i < boundPoints.Count - 1; i++)
            {
                p1 = boundPoints[i];
                p2 = boundPoints[i + 1];
                if (p1.Degree <= p.Degree && p.Degree <= p2.Degree)
                    break;
            }
            // barycentric algorithm
            double alpha = ((p2.getY() - p3.getY()) * (p.getX() - p3.getX()) + (p3.getX() - p2.getX()) * (p.getY() - p3.getY())) /
            ((p2.getY() - p3.getY()) * (p1.getX() - p3.getX()) + (p3.getX() - p2.getX()) * (p1.getY() - p3.getY()));
            double beta = ((p3.getY() - p1.getY()) * (p.getX() - p3.getX()) + (p1.getX() - p3.getX()) * (p.getY() - p3.getY())) /
            ((p2.getY() - p3.getY()) * (p1.getX() - p3.getX()) + (p3.getX() - p2.getX()) * (p1.getY() - p3.getY()));
            double gamma = 1.0f - alpha - beta;
            return gamma >= 0 && alpha >= 0 && beta >= 0;
        }
    }
}