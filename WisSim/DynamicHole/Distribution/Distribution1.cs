﻿using HoleGenerator.Model;
using HoleGenerator.RadiusCaculate;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Distribution
{
    public class Distribution1 : Distribution
    {
        public Distribution1(IRadiusCalculate radiusCalculate)
            : base(radiusCalculate)
        {
            this.random = new Random();
            this.radiusCalculate = radiusCalculate;
            boundPoints = new List<BoundPoint>();
            for (double i = 0; i < IDistribution.TOTAL_DEGREE; i += IDistribution.deltaDegree)
            {
                BoundPoint bp = new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR);
                boundPoints.Add(bp);
            }
        }

        public override void generate(double time)
        {
            double r = radiusCalculate.calc_r_byTime(time);
            double R = radiusCalculate.calc_R_byTime(time);
            double delta = r - radiusCalculate.last_r(time);
            double extra;
            foreach (BoundPoint bp in boundPoints)
            {
                double weight = Math.Abs(Normal.Sample(0.0, 1.0 / 3));
                if (weight > 1) weight = 1;
                else if (weight < 0.6) weight = 0;
                extra = (weight * (R - r)) + delta;
                if (extra + bp.Radius < r) extra = r - bp.Radius;
                else if (extra + bp.Radius > R) extra = R - bp.Radius;
                bp.addRadius(extra);
                writeToLog(time, bp);
            }
        }
    }
}