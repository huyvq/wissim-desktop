﻿using HoleGenerator.Model;
using HoleGenerator.RadiusCaculate;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Distribution
{
    public class Distribution4 : Distribution
    {
        // store average of bound points' value
        private double old_avg;

        public Distribution4(IRadiusCalculate radiusCalculate)
            : base(radiusCalculate)
        {
            this.random = new Random();
            this.radiusCalculate = radiusCalculate;
            this.old_avg = 0;
            boundPoints = new List<BoundPoint>();
            for (double i = 0; i < IDistribution.TOTAL_DEGREE; i += IDistribution.deltaDegree)
            {
                BoundPoint bp = new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR);
                boundPoints.Add(bp);
            }
        }

        public override void generate(double time)
        {
            double r = radiusCalculate.calc_r_byTime(time);
            double R = radiusCalculate.calc_R_byTime(time);
            double delta = r - radiusCalculate.last_r(time);
            double extra;
            double new_avg = 0;
            foreach (BoundPoint bp in boundPoints)
            {
                double lower, upper;
                double weight = Math.Abs(Normal.Sample(0.0, 1.0 / 2));
                if (weight > 1) weight = 1;
                else if (weight < 0.6) weight = 0;
                // calc lower: get the highest lower
                if (delta == 0 && bp.Radius > r)
                    lower = bp.Radius;
                else
                    lower = r;
                // calc upper: get the upper base on avg
                if (old_avg < bp.Radius)
                    upper = lower + (R - lower) / 4;
                else if (bp.Radius < (R + r) / 2)
                    upper = lower + (R - lower) / 2;
                else
                    upper = R;
                if (r > bp.Radius) delta = r - bp.Radius;
                extra = weight * (upper - lower) + delta;
                if (extra + bp.Radius > R) extra = R - bp.Radius;
                bp.addRadius(extra);
                writeToLog(time, bp);
                new_avg += bp.Radius;
            }
            old_avg = new_avg / boundPoints.Count;
        }
    }
}