﻿using HoleGenerator.Model;
using HoleGenerator.RadiusCaculate;
using MathNet.Numerics;
using System;
using System.Collections.Generic;

namespace HoleGenerator.Distribution
{
    public abstract class IDistribution
    {
        protected IRadiusCalculate radiusCalculate;
        protected Random random;
        protected List<BoundPoint> boundPoints;

        public static double TOTAL_DEGREE = Constants.Pi2;
        public static double deltaDegree = Constants.Pi / 18;

        public abstract void generate(double time);

        public abstract List<BoundPoint> getBoundPoints();

        public double TimeLimit
        {
            get { return radiusCalculate.TimeLimit; }
        }

        public double DeltaTime
        {
            get { return radiusCalculate.DeltaTime; }
        }
    }
}