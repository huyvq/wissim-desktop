﻿using HoleGenerator.Model;
using HoleGenerator.RadiusCaculate;
using System;
using System.Collections.Generic;

namespace HoleGenerator.Distribution
{
    public abstract class Distribution : IDistribution
    {
        //private const static Logger LOGGER = Logger.getLogger(Distribution.class.getName());
        public Distribution(IRadiusCalculate radiusCalculate)
        {
            //MyLogger.setup();
            this.random = new Random();
            this.radiusCalculate = radiusCalculate;
            boundPoints = new List<BoundPoint>();
            for (double i = 0; i < IDistribution.TOTAL_DEGREE; i += IDistribution.deltaDegree)
            {
                BoundPoint bp = new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR);
                boundPoints.Add(bp);
            }
        }

        public override List<BoundPoint> getBoundPoints()
        {
            return boundPoints;
        }

        protected void writeToLog(double time, BoundPoint bp)
        {
            //MyLogger.writeToLog(time + ", " + bp.toString());
        }

        public override void generate(double time)
        {
            throw new NotImplementedException();
        }
    }
}