﻿using HoleGenerator.Model;
using HoleGenerator.RadiusCaculate;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Distribution
{
    /// <summary>
    /// this distribution generate a endPos (as a prediction) and then init velocity to use in generate bound points in time
    /// </summary>
    public class Distribution6 : Distribution
    {
        /// <summary>
        /// generate a endPos list base on endTime to simulate
        /// </summary>
        private List<double> velocity;

        private List<BoundPoint> endPos;
        private double endTime = 0;
        private double step = 1000;

        private int oldMaxIndex = -1;

        public Distribution6(IRadiusCalculate radiusCalculate)
            : base(radiusCalculate)
        {
            this.random = new Random();
            this.radiusCalculate = radiusCalculate;

            boundPoints = new List<BoundPoint>();
            velocity = new List<double>();
            endPos = new List<BoundPoint>();
            for (double i = 0; i < TOTAL_DEGREE; i += deltaDegree)
            {
                BoundPoint bp = new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR);
                boundPoints.Add(bp);
                endPos.Add(new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR));
                velocity.Add(0);
            }
        }

        public override void generate(double time)
        {
            double extra;
            double weight;
            double r, R;
            int iMax, iMin;

            /* when time reach to endTime, generate new endPos and recalculate velocity list */
            if (endTime <= time)
            {
                while (endTime <= time)
                    endTime += step;
                double temp_r = radiusCalculate.calc_r_byTime(endTime);
                double temp_R = radiusCalculate.calc_R_byTime(endTime);

                for (int i = 0; i < endPos.Count; i++)
                {
                    BoundPoint bp = endPos[i];
                    weight = Math.Abs(Normal.Sample(0.0, 1.0 / 2)) - 0.05;
                    if (weight < 0)
                        weight = 0;
                    if (weight > 1)
                        weight = 1;

                    extra = weight * (temp_R - temp_r) + temp_r;
                    /* ensure that minimize of endPos radius is larger than current boundpoint */
                    if (extra < boundPoints[i].Radius) extra = boundPoints[i].Radius;
                    velocity[i] = (extra - boundPoints[i].Radius) / (endTime - time + 1);
                    bp.setRadius(extra);

                    writeToLog(time, bp);
                }
                endPos.RemoveAt(endPos.Count - 1);
                endPos.Add(endPos[0]);

                int index = indexOfMax(endPos);
                // set oldMax if not set
                if (oldMaxIndex < 0)
                {
                    oldMaxIndex = index;
                }
                int distance = index - oldMaxIndex;
                rotateList(endPos, distance);
                validateList(endPos, boundPoints);
                index = indexOfMax(endPos);
            }

            r = radiusCalculate.calc_r_byTime(time);
            R = radiusCalculate.calc_R_byTime(time);
            iMax = iMin = 0;

            if (boundPoints[0].Radius == 0)
            { /* init first time */
                for (int i = 0; i < boundPoints.Count; i++)
                {
                    BoundPoint bp = boundPoints[i];
                    if (i == boundPoints.Count - 1)
                    {
                        bp.setRadius(boundPoints[0].Radius);
                    }

                    weight = Math.Abs(Normal.Sample(0.0, 1.0 / 2)) - 0.1;
                    if (weight < 0) weight = 0;
                    if (weight > 1) weight = 1;

                    extra = weight * (R - r) + r;
                    bp.addRadius(extra);
                    writeToLog(time, bp);

                    // find max, min
                    if (bp.Radius > boundPoints[iMax].Radius)
                    {
                        iMax = i;
                    }
                    if (bp.Radius < boundPoints[iMin].Radius)
                    {
                        iMin = i;
                    }
                }
            }
            else
            {
                for (int i = 0; i < boundPoints.Count; i++)
                {
                    BoundPoint bp = boundPoints[i];
                    if (i == boundPoints.Count - 1)
                    {
                        bp.setRadius(boundPoints[0].Radius);
                    }

                    weight = Normal.Sample(0.0, 1.0 / 2);
                    // limit weight with abs(weight) = 1
                    if (Math.Abs(weight) > 1) weight = Math.Sign(weight);

                    extra = (1 + weight / 3) * velocity[i]; // extra ~ 0.7*v -> 1.3*v
                    if (extra + bp.Radius > endPos[i].Radius)
                        extra = 0;
                    bp.addRadius(extra);

                    // recalculate velocity
                    velocity[i] = (endPos[i].Radius - bp.Radius) / (endTime - time);
                    if (velocity[i] < 0) velocity[i] = 0;

                    // find max, min
                    if (bp.Radius > boundPoints[iMax].Radius)
                    {
                        iMax = i;
                    }
                    if (bp.Radius < boundPoints[iMin].Radius)
                    {
                        iMin = i;
                    }
                }
            }
        }

        private void validateList(List<BoundPoint> list, List<BoundPoint> compareList)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Radius < compareList[i].Radius)
                {
                    list[i].setRadius(compareList[i].Radius);
                }
            }
        }

        private void rotateList(List<BoundPoint> endPos, int increase)
        {
            // except the last point, because it's radius is equal to the first point's
            double[] tempRadius = new double[endPos.Count - 1];
            for (int i = 0; i < endPos.Count - 1; i++)
            {
                int newPos = (i + increase + endPos.Count - 1) % (endPos.Count - 1);
                tempRadius[i] = endPos[newPos].Radius;
            }
            for (int i = 0; i < endPos.Count - 1; i++)
            {
                endPos[i].setRadius(tempRadius[i]);
            }
            endPos[endPos.Count - 1].setRadius(endPos[0].Radius);
        }

        private int indexOfMax(List<BoundPoint> endPos)
        {
            int max = 0;
            for (int i = 0; i < endPos.Count; i++)
            {
                if (endPos[i].Radius > endPos[max].Radius)
                    max = i;
            }
            return max;
        }
    }
}