﻿using HoleGenerator.Model;
using HoleGenerator.RadiusCaculate;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Distribution
{
    public class Distribution3 : Distribution
    {
        // store average of bound points' value
        private double old_avg;

        public Distribution3(IRadiusCalculate radiusCalculate)
            : base(radiusCalculate)
        {
            this.random = new Random();
            this.radiusCalculate = radiusCalculate;
            this.old_avg = 0;
            boundPoints = new List<BoundPoint>();
            for (double i = 0; i < IDistribution.TOTAL_DEGREE; i += IDistribution.deltaDegree)
            {
                BoundPoint bp = new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR);
                boundPoints.Add(bp);
            }
        }

        public override void generate(double time)
        {
            double r = radiusCalculate.calc_r_byTime(time);
            double R = radiusCalculate.calc_R_byTime(time);
            double delta;
            if (boundPoints[0].Radius == 0)
            { // time = first time
                delta = r;
            }
            else
                delta = 0;
            double new_avg = 0;
            foreach (BoundPoint bp in boundPoints)
            {
                double lower, upper, extra;
                // reduce probability weight reach to 1
                double weight = Math.Abs(Normal.Sample(0.0, 1.0 / 2));
                // calc lower: get the highest lower
                if (delta == 0 && bp.Radius > r)
                    lower = bp.Radius;
                else
                    lower = r;
                // calc upper: get the upper base on avg
                if (old_avg < bp.Radius)
                    upper = lower + (R - lower) / 2;
                else
                    upper = R;
                if (r > bp.Radius) delta = r - bp.Radius;
                extra = weight * (upper - lower) + delta;
                // if (extra < 0) extra = 0;
                // limit extra in range [r, R]
                // if (extra < r - bp.Radius) extra = r - bp.Radius;
                // else
                if (extra > R - bp.Radius)
                {
                    extra = R - bp.Radius;
                }
                bp.addRadius(extra);
                writeToLog(time, bp);
            }
            old_avg = new_avg / boundPoints.Count;
        }
    }
}