﻿using HoleGenerator.Model;
using HoleGenerator.RadiusCaculate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Distribution
{
    public class Distribution2 : Distribution
    {
        public Distribution2(IRadiusCalculate radiusCalculate)
            : base(radiusCalculate)
        {
            this.radiusCalculate = radiusCalculate;
            random = new Random();
            boundPoints = new List<BoundPoint>();
            for (double i = 0; i < IDistribution.TOTAL_DEGREE; i += IDistribution.deltaDegree)
            {
                BoundPoint bp = new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR);
                boundPoints.Add(bp);
            }
        }

        public override void generate(double time)
        {
            double r = radiusCalculate.calc_r_byTime(time);
            double R = radiusCalculate.calc_R_byTime(time);
            double delta;
            if (boundPoints[0].Radius == 0) // time = first time
                delta = r;
            else
                delta = -radiusCalculate.last_r(time);
            foreach (BoundPoint bp in boundPoints)
            {
                bp.addRadius(random.NextDouble() * (R - r) + delta);
            }
        }
    }
}