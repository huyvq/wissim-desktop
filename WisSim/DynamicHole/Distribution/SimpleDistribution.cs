﻿using HoleGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Distribution
{
    public class SimpleDistribution : ISimpleDistribution
    {
        protected Random random;
        protected List<BoundPoint> boundPoints;

        public SimpleDistribution()
        {
            this.random = new Random();
            boundPoints = new List<BoundPoint>();
            for (double i = 0; i < ISimpleDistribution.TOTAL_DEGREE; i += ISimpleDistribution.deltaDegree)
            {
                BoundPoint bp = new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR);
                boundPoints.Add(bp);
            }
        }

        public override void generateHole(double minR, double maxR)
        {
            foreach (BoundPoint bp in boundPoints)
            {
                bp.addRadius(random.NextDouble() * (maxR - minR) + minR);
            }
            boundPoints[boundPoints.Count - 1].setRadius(boundPoints[0].Radius);
        }

        public override List<BoundPoint> getBoundPoints()
        {
            return boundPoints;
        }
    }
}