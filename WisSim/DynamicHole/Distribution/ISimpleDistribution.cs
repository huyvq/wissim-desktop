﻿using HoleGenerator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Distribution
{
    public abstract class ISimpleDistribution
    {
        public static double TOTAL_DEGREE = 2 * Math.PI;
        public static double deltaDegree = Math.PI / 18;

        public abstract void generateHole(double minR, double maxR);

        public abstract List<BoundPoint> getBoundPoints();
    }
}