﻿using HoleGenerator.Model;
using HoleGenerator.RadiusCaculate;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Distribution
{
    internal class Distribution5 : Distribution
    {
        private double avg;

        public Distribution5(IRadiusCalculate radiusCalculate)
            : base(radiusCalculate)
        {
            this.random = new Random();
            this.radiusCalculate = radiusCalculate;
            boundPoints = new List<BoundPoint>();
            for (double i = 0; i < IDistribution.TOTAL_DEGREE; i += IDistribution.deltaDegree)
            {
                BoundPoint bp = new BoundPoint(i, 0, BoundPoint.CoordinateType.POLAR);
                boundPoints.Add(bp);
            }
            this.avg = 0;
        }

        public override void generate(double time)
        {
            double r = radiusCalculate.calc_r_byTime(time);
            double R = radiusCalculate.calc_R_byTime(time);
            double last_r = radiusCalculate.last_r(time);
            double extra;
            double weight;
            double lower, upper, new_avg = 0;
            if (boundPoints[0].Radius == 0) // first time
            {
                foreach (BoundPoint bp in boundPoints)
                {
                    double choice = Math.Abs(Normal.Sample(0.0, 1.0 / 2));
                    weight = Math.Abs(Normal.Sample(0.0, 1.0)); // d = 1 to increase probability of max _radius
                    if (weight > 1) weight = 1;
                    if (choice <= 0.5)
                    { // chosen to be min _radius
                        weight = Math.Abs(Normal.Sample(0.0, 1.0 / 3)); // d = 1/3 de co gang dat duoc mat do weight trong khoang [-1,1]
                        if (weight < 0.3) weight = 0; // 50% to be min _radius
                    }
                    extra = (weight * (R - r)) + r;
                    if (extra + bp.Radius < r) extra = r - bp.Radius;
                    else if (extra + bp.Radius > R) extra = R - bp.Radius;
                    bp.addRadius(extra);
                    writeToLog(time, bp);
                    new_avg += bp.Radius;
                }
            }
            else
            {
                foreach (BoundPoint bp in boundPoints)
                {
                    upper = R;
                    lower = r;
                    if (bp.Radius == last_r || bp.Radius < r)
                    {
                        weight = Math.Abs(Normal.Sample(0.0, 1.0 / 3));
                        if (weight < 0.7) weight = 0;
                        else
                        {
                            upper = (R + r) / 2;
                        }
                    }
                    else
                    {
                        weight = Math.Abs(Normal.Sample(0.0, 1.0 / 2));
                    }
                    if (weight > 1) weight = 1;
                    extra = (weight * (upper - lower)) + r;
                    if (extra < bp.Radius) extra = bp.Radius;
                    bp.setRadius(extra);
                    writeToLog(time, bp);
                    new_avg += bp.Radius;
                }
            }
            avg = new_avg / boundPoints.Count;
        }
    }
}