﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoleGenerator.Point
{
    public class BoundPoint
    {
        private double _radius;
        private double _degree;
        private double _x;
        private double _y;

        public BoundPoint(double a1, double a2, CoordinateType type)
        {
            if (type == CoordinateType.POLAR)
            {
                _degree = a1;
                _radius = a2;
                _x = _radius * MathNet.Numerics.Trig.Cos(_degree);
                _y = _radius * MathNet.Numerics.Trig.Sin(_degree);
            }
            else if (type == CoordinateType.CARTESIAN)
            {
                _x = a1;
                _y = a2;
                _radius = Math.Sqrt(_x * _x + _y * _y);
                _degree = Math.Atan2(_y, _x);
                if (_degree < -0) _degree += Math.PI * 2;
            }
        }

        public void addRadius(double amount)
        {
            _radius += amount;
            _x = _radius * MathNet.Numerics.Trig.Cos(_degree);
            _y = _radius * MathNet.Numerics.Trig.Sin(_degree);
        }

        public void setRadius(double radius)
        {
            _radius = radius;
            _x = _radius * MathNet.Numerics.Trig.Cos(_degree);
            _y = _radius * MathNet.Numerics.Trig.Sin(_degree);
        }

        public void multiRadius(double ratio)
        {
            _radius *= ratio;
            _x = _radius * MathNet.Numerics.Trig.Cos(_degree);
            _y = _radius * MathNet.Numerics.Trig.Sin(_degree);
        }

        public double getDegree()
        {
            return _degree;
        }

        public double getRadius()
        {
            return _radius;
        }

        public override String ToString()
        {
            return _degree + ", " + _radius;
        }

        public double getX()
        {
            return _x;
        }

        public double getY()
        {
            return _y;
        }

        public enum CoordinateType
        {
            CARTESIAN,
            POLAR
        }
    }
}