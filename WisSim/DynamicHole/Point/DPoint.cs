﻿using System;

namespace HoleGenerator.Point
{
    public class DPoint : BoundPoint
    {
        public DPoint(double x, double y)
            : base(x, y, CoordinateType.CARTESIAN)
        {
        }

        public override String ToString()
        {
            return "DPoint: (" + this.getX() + ", " + this.getY() + ")";
        }
    }
}