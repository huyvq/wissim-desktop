namespace WisSim.GraphAlgorithms.Voronoi.Geo
{
    public enum Winding
    {
        NONE = 0, CLOCKWISE, COUNTERCLOCKWISE
    }
}