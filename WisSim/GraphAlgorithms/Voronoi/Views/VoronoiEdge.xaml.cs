﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WisSim.GraphAlgorithms.Voronoi.Views
{
    /// <summary>
    /// Interaction logic for VoronoiEdge.xaml
    /// </summary>
    public partial class VoronoiEdge : UserControl
    {
        public VoronoiEdge(Point p1, Point p2)
        {
            InitializeComponent();

            MainCanvas.Children.Add(new Path
            {
                Data = new LineGeometry(new Point(p1.X, p1.Y), new Point(p2.X, p2.Y)),
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                StrokeThickness = 1,
                Stroke = new SolidColorBrush(Colors.Black)
            });
        }
    }
}