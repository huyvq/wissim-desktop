using System.Windows;

namespace WisSim.GraphAlgorithms.Voronoi
{
    public interface ICoord
    {
        Point Coord
        {
            get;
        }
    }
}