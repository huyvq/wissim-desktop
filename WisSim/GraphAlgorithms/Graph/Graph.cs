﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WisSim.Model;

namespace WisSim.GraphAlgorithms.Graph
{
    public class Graph
    {
        private Dictionary<Node, Dictionary<Node, double>> graph;

        public Graph(List<Node> nodeList)
        {
            graph = new Dictionary<Node, Dictionary<Node, double>>();

            foreach (Node n in nodeList)
            {
                foreach (Node nn in n.NeighborList)
                {
                    AddEdge(n, nn);
                }
            }
        }

        public List<Node> Dijkstra(Node source, Node dest)
        {
            List<Node> list = new List<Node>();

            if (!graph.ContainsKey(source) || !graph.ContainsKey(dest))
            {
                return null;
            }

            Dictionary<Node, double> distance = new Dictionary<Node, double>();
            Dictionary<Node, double> t = new Dictionary<Node, double>();
            Dictionary<Node, Node> path = new Dictionary<Node, Node>();

            Dictionary<Node, double> sn;
            graph.TryGetValue(source, out sn);
            foreach (var n in graph.Keys)
            {
                double tmp_d = GetWeight(source, n);
                distance.Add(n, n == source ? 0 : tmp_d);
                path.Add(n, source);
                t.Add(n, tmp_d);
            }

            t.Remove(source);
            while (t.Count > 0)
            {
                var min_node = t.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
                t.Remove(min_node);
                foreach (var n in t.ToList())
                {
                    double min_node_d;
                    distance.TryGetValue(min_node, out min_node_d);
                    double new_d = min_node_d + GetWeight(min_node, n.Key);
                    if (distance[n.Key] > new_d)
                    {
                        distance[n.Key] = new_d;
                        path[n.Key] = min_node;
                        t[n.Key] = new_d;
                    }
                }
            }

            list.Add(dest);
            var pre_node = path[dest];
            while (pre_node != source)
            {
                list.Add(pre_node);
                pre_node = path[pre_node];
            }
            list.Add(source);
            list.Reverse();

            return list;
        }

        public double DijkstraDistance(Node source, Node dest)
        {
            List<Node> path = Dijkstra(source, dest);
            double distance = 0;

            for (int i = 0; i < path.Count - 1; i++)
            {
                distance += Math.Sqrt((path[i].X - path[i + 1].X) * (path[i].X - path[i + 1].X) + (path[i].Y - path[i + 1].Y) * (path[i].Y - path[i + 1].Y));
            }
            return distance;
        }

        private void AddVertex(Node n)
        {
            if (!graph.ContainsKey(n))
            {
                graph.Add(n, new Dictionary<Node, double>());
            }
        }

        private void AddEdge(Node n1, Node n2)
        {
            Dictionary<Node, double> n1_neighbors;
            Dictionary<Node, double> n2_neighbors;
            // double weight = Math.Sqrt((n1.X - n2.X) * (n1.X - n2.X) + (n1.Y - n2.Y) * (n1.Y - n2.Y));
            double weight = 1;
            if (graph.TryGetValue(n1, out n1_neighbors))
            {
                n1_neighbors.Add(n2, weight);
            }
            else
            {
                graph.Add(n1, new Dictionary<Node, double>() { { n2, weight } });
            }

            //if (graph.TryGetValue(n2, out n2_neighbors))
            //{
            //    n2_neighbors.Add(n1, weight);
            //}
            //else
            //{
            //    graph.Add(n2, new Dictionary<Node, double>() { { n1, weight } });
            //}
        }

        private double GetWeight(Node n1, Node n2)
        {
            Dictionary<Node, double> nn;
            if (graph.TryGetValue(n1, out nn))
            {
                double weight;
                if (nn.TryGetValue(n2, out weight))
                {
                    return weight;
                }
            }
            return Double.PositiveInfinity;
        }
    }
}