﻿//using Microsoft.Office.Interop.Excel;
using Ookii.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WisSim.Model;
using WisSim.Visualizer;

namespace WisSim.Analyzer
{
    /// <summary>
    /// Interaction logic for Analyzer.xaml
    /// </summary>
    public partial class Analyzer : UserControl
    {
        public Analyzer()
        {
            InitializeComponent();
            comboNodeFilter.SelectedIndex = 0;
            comboPacketFilterType.SelectedIndex = 0;
        }

        private MainWindow mainWindow;

        public MainWindow Main
        {
            get
            {
                return mainWindow;
            }
            set
            {
                mainWindow = value;
            }
        }

        private TraceData traceData;

        #region Binding data

        public void BindingData(TraceData data)
        {
            this.traceData = data;
            BindingNodesData(traceData.Nodes.Values.ToList());

            comboPacketFilterType.Items.Clear();
            ComboBoxItem item = new ComboBoxItem();
            item.Content = "Any type";
            comboPacketFilterType.Items.Add(item);
            foreach (string packetType in traceData.PacketTypes)
            {
                item = new ComboBoxItem();
                item.Content = packetType;
                comboPacketFilterType.Items.Add(item);
            }
            comboPacketFilterType.SelectedIndex = 0;
        }

        public void BindingNodesData(List<NodeData> nodeList)
        {
            nodeGrid.ItemsSource = nodeList;
            nodeGrid.Items.Refresh();
            packetGrid.ItemsSource = new List<PacketData>();
            packetGroupBox.Header = "---";
            packetGrid.Items.Refresh();
            eventGrid.ItemsSource = new List<EventData>();
            eventGroupBox.Header = "---";
            eventGrid.Items.Refresh();
        }

        private void nodeGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NodeData selectedNode = (NodeData)nodeGrid.SelectedItem;
            if (selectedNode != null)
            {
                packetGrid.ItemsSource = selectedNode.PacketList;
                packetGrid.Items.Refresh();
                packetGroupBox.Header = "Packets go through node " + selectedNode.ID.ToString();
                eventGrid.ItemsSource = selectedNode.NodeEvent;
                eventGrid.Items.Refresh();
                eventGroupBox.Header = "Event of node " + selectedNode.ID.ToString();
            }
        }

        private void packetGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (packetGrid.SelectedItem != null)
            {
                PacketData selectedPacket = (PacketData)packetGrid.SelectedItem;
                nodeGrid.ItemsSource = selectedPacket.NodeList;
                nodeGrid.Items.Refresh();
                nodeGroupBox.Header = "Nodes of packet " + selectedPacket.Id.ToString();

                eventGrid.ItemsSource = selectedPacket.EventList;
                eventGrid.Items.Refresh();
                eventGroupBox.Header = "Events of packet " + selectedPacket.Id.ToString();
            }
        }

        #endregion Binding data

        #region Filter Node

        private void btnNodeFilter_Click(object sender, RoutedEventArgs e)
        {
            if (comboNodeFilter.SelectedIndex == 0) //Network
            {
                BindingNodesData(this.traceData.Nodes.Values.ToList());
                return;
            }

            if (comboNodeFilter.SelectedIndex >= 1) //Selected Node
            {
                if (String.IsNullOrEmpty(tbNodeFilter.Text))
                {
                    MessageBox.Show("Please enter node group in format nodeid1|nodeid2|...|nodeidn");
                }
                else
                {
                    BindingNodesData(this.traceData.GetGroupOfNodes(tbNodeFilter.Text));
                }
                return;
            }
        }

        private void comboNodeFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboNodeFilter.Items.Count > 0)
            {
                if (comboNodeFilter.SelectedIndex == 0) //Network
                {
                    tbNodeFilter.IsEnabled = false;
                }
                else
                {
                    tbNodeFilter.IsEnabled = true;
                    if (comboNodeFilter.SelectedValue != null)
                    {
                        string groupName = ((ComboBoxItem)comboNodeFilter.SelectedItem).Content.ToString();
                        if (!String.IsNullOrEmpty(groupName))
                        {
                            if (Main.Groups.ContainsKey(groupName))
                            {
                                tbNodeFilter.Text = String.Join("|", Main.Groups[groupName].Nodes.ToArray());
                            }
                            else
                            {
                                tbNodeFilter.Text = "";
                            }
                        }
                    }
                    tbNodeFilter.Focus();
                }
            }
        }

        #endregion Filter Node

        #region Filter Packet

        private void btnPacketFilter_Click(object sender, RoutedEventArgs e)
        {
            //Filter packet
            //TODO: Background worker here
            PacketFilterOption option = readFilter();
            ComboBoxItem cbi = (ComboBoxItem)packetFilterLimit.SelectedItem;
            try
            {
                option.Limit = System.Convert.ToInt32(cbi.Content);
            }
            catch (Exception ex)
            {
                option.Limit = Int16.MaxValue;
            }
            packetGrid.ItemsSource = this.traceData.GetListOfPackets(option);
            packetGrid.Items.Refresh();
            packetGroupBox.Header = packetGrid.Items.Count.ToString() + " packets from offset " + packetFilterOffset.Value.ToString();
        }

        private void comboDestSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboPacketFilterDest.Items.Count > 0)
            {
                if (comboPacketFilterDest.SelectedValue != null)
                {
                    if (comboPacketFilterDest.SelectedIndex > 1) //Network
                    {
                        string groupName = ((ComboBoxItem)comboPacketFilterDest.SelectedItem).Content.ToString();
                        if (!String.IsNullOrEmpty(groupName))
                        {
                            if (Main.Groups.ContainsKey(groupName))
                            {
                                packetFilterDest.Value = String.Join("|", Main.Groups[groupName].Nodes.ToArray());
                            }
                            else
                            {
                                packetFilterDest.Value = "";
                            }
                        }
                    }
                    else if (comboPacketFilterDest.SelectedIndex == 0)
                    {
                        if (packetFilterDest != null) packetFilterDest.Value = "";
                    }
                }
            }
        }

        private void comboSourceSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboPacketFilterSource.Items.Count > 0)
            {
                if (comboPacketFilterSource.SelectedValue != null)
                {
                    if (comboPacketFilterSource.SelectedIndex > 1) //Network
                    {
                        string groupName = ((ComboBoxItem)comboPacketFilterSource.SelectedItem).Content.ToString();
                        if (!String.IsNullOrEmpty(groupName))
                        {
                            if (Main.Groups.ContainsKey(groupName))
                            {
                                packetFilterSource.Value = String.Join("|", Main.Groups[groupName].Nodes.ToArray());
                            }
                            else
                            {
                                packetFilterSource.Value = "";
                            }
                        }
                    }
                    else if (comboPacketFilterSource.SelectedIndex == 0)
                    {
                        if (packetFilterSource != null) packetFilterSource.Value = "";
                    }
                }
            }
        }

        private void comboTypeSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboPacketFilterType.Items.Count > 0)
            {
                if (comboPacketFilterType.SelectedValue != null)
                {
                    if (comboPacketFilterType.SelectedIndex >= 1) // Not Any type
                    {
                        packetFilterType.Value = ((ComboBoxItem)comboPacketFilterType.SelectedItem).Content.ToString();
                    }
                    else if (comboPacketFilterType.SelectedIndex == 0)
                    {
                        if (packetFilterType != null) packetFilterType.Value = "";
                    }
                }
            }
        }

        #endregion Filter Packet

        #region analyser functions

        private void getNetWorkLifeTime(object sender, RoutedEventArgs e)
        {
            double deadthPercent = 100;
            double maxTime = 500;

            InputDialog i1 = new InputDialog();
            i1.MainInstruction = "Input network's death ratio";
            i1.WindowTitle = "Death Ratio";
            if (i1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    deadthPercent = System.Convert.ToDouble(i1.Input);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Death ratio is in invalid format");
                    return;
                }
            }
            else
            {
                return;
            }

            InputDialog i2 = new InputDialog();
            i2.MainInstruction = "Input network's simulation time";
            i2.WindowTitle = "Simulation Time";
            if (i2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    maxTime = System.Convert.ToDouble(i2.Input);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Simulation time is in invalid format");
                    return;
                }
            }
            else
            {
                return;
            }

            double lifetime = traceData.GetNetworkLifeTime(deadthPercent, maxTime, (List<NodeData>)nodeGrid.ItemsSource);

            //Show result
            rtbAnalysisResult.AppendText("Network life time (death ratio = " + deadthPercent + "%): " + lifetime.ToString() + " s\r");
            rtbAnalysisResult.AppendText("---\r");
            rtbAnalysisResult.ScrollToEnd();
        }

        private void getNetworkSleepTime(object sender, RoutedEventArgs e)
        {
            double maxTime = 500;

            InputDialog i2 = new InputDialog();
            i2.MainInstruction = "Input network's simulation time";
            i2.WindowTitle = "Simulation Time";
            if (i2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    maxTime = System.Convert.ToDouble(i2.Input);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Simulation time is in invalid format");
                    return;
                }
            }
            else
            {
                return;
            }

            double[] sleeptime = traceData.CalculateNetWorkSleepTime(maxTime, (List<NodeData>)nodeGrid.ItemsSource);
            rtbAnalysisResult.AppendText("Network sleep time:\r");
            rtbAnalysisResult.AppendText("Min: " + sleeptime[0].ToString() + "\r");
            rtbAnalysisResult.AppendText("Max: " + sleeptime[1].ToString() + "\r");
            rtbAnalysisResult.AppendText("Average: " + sleeptime[2].ToString() + "\r");
            rtbAnalysisResult.AppendText("---\r");
            rtbAnalysisResult.ScrollToEnd();
        }

        private void getHopCount(object sender, RoutedEventArgs e)
        {
            PacketFilterOption option = readFilter();
            ComboBoxItem cbi = (ComboBoxItem)packetFilterLimit.SelectedItem;
            try
            {
                option.Limit = System.Convert.ToInt32(cbi.Content);
            }
            catch (Exception ex)
            {
                option.Limit = Int16.MaxValue;
            }
            double[] hopCount = traceData.CalculateNetWorkHopCount(option);
            rtbAnalysisResult.AppendText("Network hop count:\r");
            if (hopCount[0] == int.MaxValue)
            {
                rtbAnalysisResult.AppendText("Min: 0 \r");
            }
            else
            {
                rtbAnalysisResult.AppendText("Min: " + hopCount[0].ToString() + "\r");
            }
            rtbAnalysisResult.AppendText("Max: " + hopCount[1].ToString() + "\r");
            rtbAnalysisResult.AppendText("Average: " + hopCount[2].ToString() + "\r");
            rtbAnalysisResult.AppendText("---\r");
            rtbAnalysisResult.ScrollToEnd();
        }

        private void getLatency(object sender, RoutedEventArgs e)
        {
            PacketFilterOption option = readFilter();
            ComboBoxItem cbi = (ComboBoxItem)packetFilterLimit.SelectedItem;
            try
            {
                option.Limit = System.Convert.ToInt32(cbi.Content);
            }
            catch (Exception ex)
            {
                option.Limit = Int16.MaxValue;
            }

            double[] latency = traceData.CalculateNetworkLatencty(option);
            rtbAnalysisResult.AppendText("Network hop latency:\r");
            if (latency[0] == double.MaxValue)
            {
                rtbAnalysisResult.AppendText("Min: 0 \r");
            }
            else
            {
                rtbAnalysisResult.AppendText("Min: " + latency[0].ToString() + "\r");
            }

            rtbAnalysisResult.AppendText("Max: " + latency[1].ToString() + "\r");
            rtbAnalysisResult.AppendText("Average: " + latency[2].ToString() + "\r");
            rtbAnalysisResult.AppendText("---\r");
            rtbAnalysisResult.ScrollToEnd();
        }

        private void getEfficiency(object sender, RoutedEventArgs e)
        {
            PacketFilterOption option = readFilter();
            ComboBoxItem cbi = (ComboBoxItem)packetFilterLimit.SelectedItem;
            try
            {
                option.Limit = System.Convert.ToInt32(cbi.Content);
            }
            catch (Exception ex)
            {
                option.Limit = Int16.MaxValue;
            }
            double dropRatio = traceData.CalculateDroppedRatio(option);

            //Show result
            rtbAnalysisResult.AppendText("Packets dropped ratio: " + dropRatio.ToString() + "\r");
            rtbAnalysisResult.AppendText("---\r");
            rtbAnalysisResult.ScrollToEnd();
        }

        private void getThroughput(object sender, RoutedEventArgs e)
        {
            double maxTime = 500;

            InputDialog i2 = new InputDialog();
            i2.MainInstruction = "Input network's simulation time";
            i2.WindowTitle = "Simulation Time";
            if (i2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    maxTime = System.Convert.ToDouble(i2.Input);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Simulation time is in invacomboNodeFilterlid format");
                    return;
                }
            }
            else
            {
                return;
            }

            PacketFilterOption option = readFilter();
            ComboBoxItem cbi = (ComboBoxItem)packetFilterLimit.SelectedItem;
            try
            {
                option.Limit = System.Convert.ToInt32(cbi.Content);
            }
            catch (Exception ex)
            {
                option.Limit = Int16.MaxValue;
            }

            double throughput = traceData.GetNetworkThroughput(0, maxTime, option);

            //Show result
            rtbAnalysisResult.AppendText("Network throughput: " + throughput.ToString() + "\r");
            rtbAnalysisResult.AppendText("---\r");
            rtbAnalysisResult.ScrollToEnd();
        }

        private void getLastEnergy(object sender, RoutedEventArgs e)
        {
            double[] energy = traceData.CalculateNetWorkEnergy((List<NodeData>)nodeGrid.ItemsSource);
            rtbAnalysisResult.AppendText("Network last energy:\r");
            rtbAnalysisResult.AppendText("Min: " + energy[0].ToString() + "\r");
            rtbAnalysisResult.AppendText("Max: " + energy[1].ToString() + "\r");
            rtbAnalysisResult.AppendText("Average: " + energy[2].ToString() + "\r");
            rtbAnalysisResult.AppendText("---\r");
            rtbAnalysisResult.ScrollToEnd();
        }

        private PacketFilterOption readFilter()
        {
            PacketFilterOption option = new PacketFilterOption();
            option.Type = packetFilterType.Value;
            option.MaxSize = packetFilterMaxSize.Value;
            option.MinSize = packetFilterMinSize.Value;
            option.Layer = packetFilterLayer.Value;
            option.SourceGroup = packetFilterSource.Value;
            option.DestinationGroup = packetFilterDest.Value;
            option.Offset = System.Convert.ToInt32(packetFilterOffset.Value);
            option.StartTime = packetFilterStartTime.Value;
            option.EndTime = packetFilterEndTime.Value;

            return option;
        }

        #endregion analyser functions

        #region Export data

        private void exportToExcel(object sender, RoutedEventArgs e)
        {
            //TODO: mix data mode?
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                if (xlApp == null)
                {
                    MessageBox.Show("Excel is not properly installed!!");
                    return;
                }
                //2. Open file to write
                object misValue = System.Reflection.Missing.Value;
                Microsoft.Office.Interop.Excel.Workbook WB = xlApp.Workbooks.Add(misValue);
                Microsoft.Office.Interop.Excel.Worksheet WS = (Microsoft.Office.Interop.Excel.Worksheet)WB.Worksheets.get_Item(1);
                //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                //3. Create header
                int column = 1;
                foreach (var dataGridColumn in this.packetGrid.Columns.Skip(1))
                {
                    WS.Rows[1].Cells[column] = dataGridColumn.Header;
                    //WS.Rows[1].Cells[column].CellFormat.ShrinkToFit = true;
                    //WS.Rows[1].Cells[column].CellFormat.VerticalAlignment = VerticalAlignment.Center;
                    //WS.Rows[1].Cells[column].CellFormat.Alignment = HorizontalAlignment.Center;
                    column++;
                }

                //4. Insert data
                int row = 2;
                column = 1;
                foreach (PacketData packet in packetGrid.ItemsSource)
                {
                    WS.Rows[row].Cells[column] = packet.Id;
                    WS.Rows[row].Cells[++column] = packet.PacketType;
                    {
                    }
                    if (ContextMenuSize.IsChecked)
                    {
                        WS.Rows[row].Cells[++column] = packet.PacketSize;
                        WS.Rows[1].Cells[column] = "Size";
                    }
                    if (ContextMenuSource.IsChecked)
                    {
                        WS.Rows[row].Cells[++column] = packet.SourceNode.ID;
                        WS.Rows[1].Cells[column] = "Source";
                    }
                    if (ContextMenuDestination.IsChecked)
                    {
                        if (packet.DestinationNode != null)
                        {
                            WS.Rows[row].Cells[++column] = packet.DestinationNode.ID;
                        }
                        else
                        {
                            WS.Rows[row].Cells[++column] = "";
                        }
                        WS.Rows[1].Cells[column] = "Destination";
                    }

                    if (ContextMenuLayer.IsChecked)
                    {
                        WS.Rows[row].Cells[++column] = packet.SourceLayer;
                        WS.Rows[1].Cells[column] = "Layer";
                    }
                    //WS.Rows[row].Cells[++column] = packet.IsMultiCast;
                    if (ContextMenuIsDropped.IsChecked)
                    {
                        WS.Rows[row].Cells[++column] = packet.IsDrop;
                        WS.Rows[1].Cells[column] = "Is Dropped";
                    }
                    if (ContextMenuHopCount.IsChecked)
                    {
                        WS.Rows[row].Cells[++column] = packet.HopCount;
                        WS.Rows[1].Cells[column] = "Hop Count";
                    }
                    if (ContextMenuLatency.IsChecked)
                    {
                        WS.Rows[row].Cells[++column] = packet.Latency;
                        WS.Rows[1].Cells[column] = "Latency";
                    }
                    if (ContextMenuStartTime.IsChecked)
                    {
                        WS.Rows[row].Cells[++column] = packet.StartTime;
                        WS.Rows[1].Cells[column] = "Start time";
                    }
                    if (ContextMenuEndTime.IsChecked)
                    {
                        WS.Rows[row].Cells[++column] = packet.EndTime;
                        WS.Rows[1].Cells[column] = "End time";
                    }
                    column = 1;
                    row += 1;
                }

                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
                dlg.DefaultExt = ".xlsx";
                dlg.Filter = "trace Files (*.xlsx)|*.xlsx|All Files (*.*)|*.*";
                dlg.FilterIndex = 2;
                dlg.RestoreDirectory = true;
                // Display OpenFileDialog by calling ShowDialog method

                if (dlg.ShowDialog() == true)
                {
                    WB.SaveAs(dlg.FileName, misValue, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, misValue, misValue, misValue, misValue, misValue);
                    WB.Close(true, misValue, misValue);
                    xlApp.Quit();
                    releaseObject(WS);
                    releaseObject(WB);
                    releaseObject(xlApp);
                    /*
                        *xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();

                    releaseObject(xlWorkSheet);
                    releaseObject(xlWorkBook);
                    releaseObject(xlApp);
                        */
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
                return;
            }
            System.Windows.MessageBox.Show("Finish export");
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        #endregion Export data

        #region ClearData

        public void ClearTraceData()
        {
            this.nodeGrid.ItemsSource = null;
            this.packetGrid.ItemsSource = null;
            this.eventGrid.ItemsSource = null;
            this.nodeGrid.Items.Refresh();
            this.packetGrid.Items.Refresh();
            this.eventGrid.Items.Refresh();
            nodeGroupBox.Header = "---";
            packetGroupBox.Header = "---";
            eventGroupBox.Header = "---";

            traceData = null;
        }

        #endregion ClearData

        #region interaction between Visualizer and Analyser

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            if (packetGrid.SelectedItem != null)
            {
                PacketData selectedPacket = (PacketData)packetGrid.SelectedItem;
                this.Main.ShowPacketPath(selectedPacket.Id);
            }
        }

        public void UpdateGroup()
        {
            //Filter Nodes
            comboNodeFilter.Items.Clear();
            ComboBoxItem item = new ComboBoxItem();
            item.Content = "All nodes";
            comboNodeFilter.Items.Add(item);

            item = new ComboBoxItem();
            item.Content = "Selected node";
            comboNodeFilter.Items.Add(item);

            Separator sep = new Separator();
            sep.IsEnabled = false;
            sep.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            comboNodeFilter.Items.Add(sep);

            foreach (NodesGroup group in Main.Groups.Values)
            {
                item = new ComboBoxItem();
                item.Content = group.Name;
                comboNodeFilter.Items.Add(item);
            }

            comboNodeFilter.SelectedIndex = 0;

            //Filter Packets
            comboPacketFilterSource.Items.Clear();
            item = new ComboBoxItem();
            item.Content = "All nodes";
            comboPacketFilterSource.Items.Add(item);

            PropertyEntry entry = new PropertyEntry();
            entry.Name = "packetFilterSource";
            entry.Label = "Other";
            entry.Value = "";
            comboPacketFilterSource.Items.Add(entry);
            foreach (NodesGroup group in Main.Groups.Values)
            {
                item = new ComboBoxItem();
                item.Content = group.Name;
                comboPacketFilterSource.Items.Add(item);
            }
            comboPacketFilterSource.SelectedIndex = 0;

            comboPacketFilterDest.Items.Clear();
            item = new ComboBoxItem();
            item.Content = "All nodes";
            comboPacketFilterDest.Items.Add(item);

            entry = new PropertyEntry();
            entry.Label = "Other";
            entry.Value = "";
            entry.Name = "packetFilterDest";
            comboPacketFilterDest.Items.Add(entry);
            foreach (NodesGroup group in Main.Groups.Values)
            {
                item = new ComboBoxItem();
                item.Content = group.Name;
                comboPacketFilterDest.Items.Add(item);
            }
            comboPacketFilterDest.SelectedIndex = 0;
        }

        #endregion interaction between Visualizer and Analyser

        #region show-hide column in datagrid function

        private void Node_DeadTime_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuDeadTime.IsChecked = !ContextMenuDeadTime.IsChecked;
            if (ContextMenuDeadTime.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuDeadTime.Header;
                c.Binding = new System.Windows.Data.Binding("DeadAt");
                c.Binding.StringFormat = "0.000";
                nodeGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(nodeGrid, ContextMenuDeadTime.Header.ToString());
            }
        }

        private void Node_LastEnergy_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuLastEnergy.IsChecked = !ContextMenuLastEnergy.IsChecked;
            if (ContextMenuLastEnergy.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuLastEnergy.Header;
                c.Binding = new System.Windows.Data.Binding("LastEnergy");
                c.Binding.StringFormat = "0.000";
                nodeGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(nodeGrid, ContextMenuLastEnergy.Header.ToString());
            }
        }

        private void RemoveGridColumn(DataGrid grid, string colHeader)
        {
            foreach (DataGridColumn col in grid.Columns)
            {
                try
                {
                    if (col.Header.ToString() == colHeader)
                    {
                        grid.Columns.Remove(col);
                        break;
                    }
                }
                catch (NullReferenceException) { }
            }
        }

        private void Packet_Size_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuSize.IsChecked = !ContextMenuSize.IsChecked;
            if (ContextMenuSize.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuSize.Header;
                c.Binding = new System.Windows.Data.Binding("PacketSize");
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuSize.Header.ToString());
            }
        }

        private void Packet_Source_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuSource.IsChecked = !ContextMenuSource.IsChecked;
            if (ContextMenuSource.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuSource.Header;
                c.Binding = new System.Windows.Data.Binding("SourceNode.ID");
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuSource.Header.ToString());
            }
        }

        private void Packet_Destination_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuDestination.IsChecked = !ContextMenuDestination.IsChecked;
            if (ContextMenuDestination.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuDestination.Header;
                c.Binding = new System.Windows.Data.Binding("DestinationNode.ID");
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuDestination.Header.ToString());
            }
        }

        private void Packet_Layer_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuLayer.IsChecked = !ContextMenuLayer.IsChecked;
            if (ContextMenuLayer.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuLayer.Header;
                c.Binding = new System.Windows.Data.Binding("SourceLayer");
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuLayer.Header.ToString());
            }
        }

        private void Packet_IsDropped_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuIsDropped.IsChecked = !ContextMenuIsDropped.IsChecked;
            if (ContextMenuIsDropped.IsChecked)
            {
                //Add columns
                DataGridCheckBoxColumn c = new DataGridCheckBoxColumn();
                c.Header = ContextMenuIsDropped.Header;
                c.Binding = new System.Windows.Data.Binding("IsDrop");
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuIsDropped.Header.ToString());
            }
        }

        private void Packet_HopCount_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuHopCount.IsChecked = !ContextMenuHopCount.IsChecked;
            if (ContextMenuHopCount.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuHopCount.Header;
                c.Binding = new System.Windows.Data.Binding("HopCount");
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuHopCount.Header.ToString());
            }
        }

        private void Packet_Latency_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuLatency.IsChecked = !ContextMenuLatency.IsChecked;
            if (ContextMenuLatency.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuLatency.Header;
                c.Binding = new System.Windows.Data.Binding("Latency");
                c.Binding.StringFormat = "0.000";
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuLatency.Header.ToString());
            }
        }

        private void btnClearConsole_Click(object sender, RoutedEventArgs e)
        {
            rtbAnalysisResult.Document.Blocks.Clear();
        }

        private void Packet_StartTime_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuStartTime.IsChecked = !ContextMenuStartTime.IsChecked;
            if (ContextMenuStartTime.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuStartTime.Header;
                c.Binding = new System.Windows.Data.Binding("StartTime");
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuStartTime.Header.ToString());
            }
        }

        private void Packet_EndTime_Click(object sender, RoutedEventArgs e)
        {
            ContextMenuEndTime.IsChecked = !ContextMenuEndTime.IsChecked;
            if (ContextMenuEndTime.IsChecked)
            {
                //Add columns
                DataGridTextColumn c = new DataGridTextColumn();
                c.Header = ContextMenuEndTime.Header;
                c.Binding = new System.Windows.Data.Binding("EndTime");
                packetGrid.Columns.Add(c);
            }
            else
            {
                RemoveGridColumn(packetGrid, ContextMenuEndTime.Header.ToString());
            }
        }

        #endregion show-hide column in datagrid function

        #region View on Visualizer

        private void CheckBoxVisualizer_CheckedAll(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.CheckBox cb = sender as System.Windows.Controls.CheckBox;
            if (cb.IsChecked == true)
            {
                packetGrid.Items.OfType<PacketData>().ToList().ForEach(x => x.IsVisualization = true);
            }
            else
            {
                packetGrid.Items.OfType<PacketData>().ToList().ForEach(x => x.IsVisualization = false);
            }
        }

        private void OtherCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (packetGrid.Items.OfType<PacketData>().All(x => x.IsVisualization == true))
            {
                cbAll.IsChecked = true;
            }
            else if (packetGrid.Items.OfType<PacketData>().All(x => x.IsVisualization == false))
            {
                cbAll.IsChecked = false;
            }
            else
            {
                cbAll.IsChecked = null;
            }
        }

        private void viewOnVisualizer(object sender, RoutedEventArgs e)
        {
            List<int> ids = new List<int>();
            foreach (PacketData item in packetGrid.Items)
            {
                if (item.IsVisualization)
                {
                    ids.Add(item.Id);
                }
            }
            this.Main.ShowMultiPacketPath(ids);
        }

        #endregion View on Visualizer

        private void playAnimation(object sender, RoutedEventArgs e)
        {
            NetworkAnimation na = new NetworkAnimation();
            List<EventData> events = new List<EventData>();

            // filter list packet and its events
            foreach (PacketData item in packetGrid.Items)
            {
                if (item.IsVisualization)
                {
                    events.AddRange(item.EventList);
                }
            }
            events = events.OrderBy(o => o.HappendTime).ToList();

            // create animation object with option
            na.Events = events;
            na.StartTime = events[0].HappendTime;
            na.EndTime = events[events.Count - 1].HappendTime;

            // call play animation method
            this.Main.PlayAnimation(na);
        }

        private void debugStretch(object sender, RoutedEventArgs e)
        {
            PacketFilterOption option = readFilter();
            ComboBoxItem cbi = (ComboBoxItem)packetFilterLimit.SelectedItem;
            try
            {
                option.Limit = System.Convert.ToInt32(cbi.Content);
            }
            catch (Exception ex)
            {
                option.Limit = Int16.MaxValue;
            }
            Dictionary<PacketData, int> paths = traceData.CalculatePathHopCount(option);

            rtbAnalysisResult.Document.Blocks.Clear();
            foreach (var p in paths)
            {
                rtbAnalysisResult.AppendText(p.Key.SourceNode.ID + "\t" + p.Key.DestinationNode.ID + "\t" + p.Value + "\r");
            }
            rtbAnalysisResult.ScrollToEnd();
        }

        /// <summary>
        /// export to a excel file the number of packet go through each node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exportNumberPacketOfNode(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                if (xlApp == null)
                {
                    MessageBox.Show("Excel is not properly installed!!");
                    return;
                }

                //2. Open file to write
                object misValue = System.Reflection.Missing.Value;
                Microsoft.Office.Interop.Excel.Workbook WB = xlApp.Workbooks.Add(misValue);
                Microsoft.Office.Interop.Excel.Worksheet WS = (Microsoft.Office.Interop.Excel.Worksheet)WB.Worksheets.get_Item(1);
                //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                //3. Create header - no header
                //4. Insert data
                int row = 1;
                int column = 1;
                foreach (var n in traceData.Nodes)
                {
                    WS.Rows[row].Cells[column] = n.Value.ID;
                    WS.Rows[row].Cells[++column] = n.Value.X;
                    WS.Rows[row].Cells[++column] = n.Value.Y;
                    WS.Rows[row].Cells[++column] = n.Value.NonDropCBRPacketCount;
                    WS.Rows[row].Cells[++column] = n.Value.DropCBRPacketCount;
                    WS.Rows[row].Cells[++column] = n.Value.DropCBRPacketCount + n.Value.NonDropCBRPacketCount;

                    column = 1;
                    row += 1;
                }

                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
                dlg.DefaultExt = ".xlsx";
                dlg.Filter = "trace Files (*.xlsx)|*.xlsx|All Files (*.*)|*.*";
                dlg.FilterIndex = 2;
                dlg.RestoreDirectory = true;
                // Display OpenFileDialog by calling ShowDialog method

                if (dlg.ShowDialog() == true)
                {
                    WB.SaveAs(dlg.FileName, misValue, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, misValue, misValue, misValue, misValue, misValue);
                    WB.Close(true, misValue, misValue);
                    xlApp.Quit();
                    releaseObject(WS);
                    releaseObject(WB);
                    releaseObject(xlApp);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
                return;
            }
            System.Windows.MessageBox.Show("Finish export");
        }
    }
}