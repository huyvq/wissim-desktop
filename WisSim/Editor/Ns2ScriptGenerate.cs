﻿using System;
using System.Diagnostics;
using System.Text;
using System.Xml;
using WisSim.Model;
using WisSim.NetworkComponents;

namespace WisSim.Editor
{
    public class Ns2ScriptGenerate : ScriptGenerator
    {
        private const string SimulateScript = "simulate.tcl"; // The name of files, which also appear as the name of tabs
        private const string TopoDataScript = "topo_data.tcl";
        private const string TrafficsScript = "cbr.tcl";
        private const string NodesOffScript = "nodeoff.tcl";
        private const string SinkScript = "nodesink.tcl";

        public Ns2ScriptGenerate()
            : base("Ns2")                    // name of this Generator
        {
            Scripts.Add(SimulateScript, new StringBuilder());
            Scripts.Add(TopoDataScript, new StringBuilder());
            Scripts.Add(TrafficsScript, new StringBuilder());
            Scripts.Add(NodesOffScript, new StringBuilder());
            Scripts.Add(SinkScript, new StringBuilder());
        }

        public override void Generate(Project project)
        {
            try
            {
                #region simulate.tcl

                StringBuilder sw = Scripts[SimulateScript];
                sw.Clear();

                sw.AppendLine("# Script for WisSim simulator. Last edit " + DateTime.Now);
                sw.AppendLine();
                sw.AppendLine("set opt(x)\t" + project.Network.Width + "\t;# X dimension of the topography");
                sw.AppendLine("set opt(y)\t" + project.Network.Height + "\t;# Y dimension of the topography");
                sw.AppendLine("set opt(stop)\t" + project.Network.SimulationTime + "\t;# simulation time");
                sw.AppendLine("set opt(nn)\t" + project.Network.NodeList.Count + "\t;# number of nodes");
                sw.AppendLine("set opt(tr)	Trace.tr	;# trace file");
                sw.AppendLine("set opt(nam)	nam.out.tr");
                sw.AppendLine();
                sw.AppendLine("set opt(ifqlen)\t" + project.Network.IfqLen + "\t;# max packet in ifq");
                sw.AppendLine("set opt(dump_at)\t" + project.Network.DumpAt + "\t;# time to dump broadcast energy & broadcast region (CORBAL only)");

                foreach (XmlNode chil in project.Setting.GetElementsByTagName("setting")[0].ChildNodes)
                {
                    try
                    {
                        sw.AppendLine("set opt(" + chil.Name + ")\t" + chil.ChildNodes[int.Parse(chil.Attributes["choose"].Value)].Attributes["type"].Value);
                    }
                    catch (NullReferenceException)
                    {
                        // Add nothing
                    }
                }
                sw.AppendLine();

                var energy = project.Energy.GetElementsByTagName("energy")[0];
                sw.AppendLine(@"set opt(energymodel)	 " + energy.Attributes["EnergyModel"].Value);
                sw.AppendLine(@"set opt(radiomodel)      " + energy.Attributes["RadioModel"].Value);
                sw.AppendLine(@"set opt(initialenergy)   " + energy.Attributes["InitialEnergy"].Value);
                sw.AppendLine(@"set opt(idlePower) 	     " + energy.Attributes["IdlePower"].Value);
                sw.AppendLine(@"set opt(rxPower) 	     " + energy.Attributes["ReceivePower"].Value);
                sw.AppendLine(@"set opt(txPower) 	     " + energy.Attributes["TransportPower"].Value);
                sw.AppendLine(@"set opt(sleepPower) 	 " + energy.Attributes["SleepPower"].Value);
                sw.AppendLine(@"set opt(transitionPower) " + energy.Attributes["TransitionPower"].Value);
                sw.AppendLine(@"set opt(transitionTime)  " + energy.Attributes["TransitionTime"].Value);

                sw.AppendLine();
                sw.AppendLine(@"# ======================================================================");
                sw.AppendLine();

                foreach (XmlNode chil in project.Setting.GetElementsByTagName("setting")[0].ChildNodes)
                {
                    XmlNode node = chil.ChildNodes[int.Parse(chil.Attributes["choose"].Value)];

                    string prefix = "";
                    if (chil.Attributes["prefix"] != null)
                    {
                        prefix = chil.Attributes["prefix"].Value + "/";
                    }
                    prefix += node.Attributes["type"].Value;

                    if (node.FirstChild != null)
                    {
                        foreach (XmlNode par in node.FirstChild.ChildNodes)
                        {
                            sw.AppendLine(prefix + " set " + par.Attributes["property"].Value + " " + par.Attributes["value"].Value);
                        }
                    }
                    sw.AppendLine();
                }

                sw.AppendLine(@"# ======================================================================");
                sw.AppendLine();
                sw.AppendLine(@"#");
                sw.AppendLine(@"# Initialize Global Variables");
                sw.AppendLine(@"#");
                sw.AppendLine();
                sw.AppendLine(@"# set start time");
                sw.AppendLine(@"set startTime [clock seconds]");
                sw.AppendLine();
                sw.AppendLine(@"# set up ns simulator and nam trace");
                sw.AppendLine(@"set ns_		[new Simulator]");
                sw.AppendLine(@"set chan	[new $opt(chan)]");
                sw.AppendLine(@"set prop	[new $opt(prop)]");
                sw.AppendLine(@"set topo	[new Topography]");
                sw.AppendLine();
                sw.AppendLine(@"set tracefd	[open $opt(tr) w]");
                sw.AppendLine(@"#set namtrace	[open $opt(nam) w]");
                sw.AppendLine();
                sw.AppendLine(@"# run the simulator");
                sw.AppendLine(@"$ns_ trace-all $tracefd ");
                sw.AppendLine(@"#$ns_ namtrace-all-wireless $namtrace $opt(x) $opt(y) ");
                sw.AppendLine();
                sw.AppendLine(@"$topo load_flatgrid $opt(x) $opt(y) ");
                sw.AppendLine(@"$prop topography $topo");
                sw.AppendLine();
                sw.AppendLine(@"set god_ [create-god $opt(nn)]");
                sw.AppendLine();
                sw.AppendLine(@"# configure the nodes");
                sw.AppendLine(@"$ns_ node-config -adhocRouting $opt(rp) \");
                sw.AppendLine(@"		 -llType $opt(ll) \");
                sw.AppendLine(@"		 -macType $opt(mac) \");
                sw.AppendLine(@"		 -ifqType $opt(ifq) \");
                sw.AppendLine(@"		 -ifqLen $opt(ifqlen) \");
                sw.AppendLine(@"		 -antType $opt(ant) \");
                sw.AppendLine(@"		 -propType $opt(prop) \");
                sw.AppendLine(@"		 -phyType $opt(netif) \");
                sw.AppendLine(@"		 -channel [new $opt(chan)] \");
                sw.AppendLine(@"		 -topoInstance $topo \");
                sw.AppendLine(@"		 -agentTrace ON \");
                sw.AppendLine(@"		 -routerTrace ON \");
                sw.AppendLine(@"		 -macTrace OFF \");

                if (energy.Attributes["EnergyModel"].Value == "EnergyModel")
                {
                    sw.AppendLine(@"		 -movementTrace OFF \");
                    sw.AppendLine(@"		 -energyModel $opt(energymodel) \");
                    sw.AppendLine(@"		 -idlePower $opt(idlePower) \");
                    sw.AppendLine(@"		 -rxPower $opt(rxPower) \");
                    sw.AppendLine(@"		 -txPower $opt(txPower) \");
                    sw.AppendLine(@"		 -sleepPower $opt(sleepPower) \");
                    sw.AppendLine(@"		 -transitionPower $opt(transitionPower) \");
                    sw.AppendLine(@"		 -transitionTime $opt(transitionTime) \");
                    sw.AppendLine(@"		 -initialEnergy $opt(initialenergy)");
                }
                else
                {
                    sw.AppendLine(@"		 -movementTrace OFF");
                }

                sw.AppendLine();
                sw.AppendLine(@"puts ""Routing Protocol: $opt(rp)""");
                sw.AppendLine(@"$defaultRNG seed 0");
                sw.AppendLine();
                sw.AppendLine(@"# set up nodes");
                sw.AppendLine(@"for {set i 0} {$i < $opt(nn)} {incr i} {");
                sw.AppendLine(@"	set mnode_($i) [$ns_ node]");
                sw.AppendLine(@"}");
                sw.AppendLine();
                sw.AppendLine(@"# set up node position");
                sw.AppendLine(@"source ./topo_data.tcl");
                sw.AppendLine();
                sw.AppendLine(@"for {set i 0} {$i < $opt(nn)} { incr i } {");
                sw.AppendLine(@"	$ns_ initial_node_pos $mnode_($i) 5");
                sw.AppendLine(@"}");
                sw.AppendLine();
                sw.AppendLine(@"# telling nodes when the simulator ends");
                sw.AppendLine(@"for {set i 0} {$i < $opt(nn)} {incr i} {");
                sw.AppendLine(@"	$ns_ at [expr $opt(stop) - 0.000000001] ""$mnode_($i) off""");
                sw.AppendLine(@"	$ns_ at $opt(dump_at) ""[$mnode_($i) set ragent_] dumpEnergy""");
                sw.AppendLine(@"	$ns_ at $opt(dump_at) ""[$mnode_($i) set ragent_] dumpBroadcast""");
                sw.AppendLine(@"	$ns_ at $opt(stop) ""[$mnode_($i) set ragent_] dump""");
                sw.AppendLine(@"	$ns_ at $opt(stop).000000001 ""$mnode_($i) reset""");
                sw.AppendLine(@"}");
                sw.AppendLine();
                sw.AppendLine(@"source ./cbr.tcl");
                sw.AppendLine();
                sw.AppendLine(@"source ./nodeoff.tcl");
                sw.AppendLine();
                sw.AppendLine(@"source ./nodesink.tcl");
                sw.AppendLine();
                sw.AppendLine(@"# ending nam and the simulation");
                sw.AppendLine(@"#$ns_ at $opt(stop) ""$ns_ nam-end-wireless $opt(stop)"" ");
                sw.AppendLine(@"$ns_ at $opt(stop) ""stop"" ");
                sw.AppendLine();
                sw.AppendLine(@"proc stop {} {");
                sw.AppendLine(@"	global ns_ tracefd startTime	;# namtrace");
                sw.AppendLine(@"	$ns_ flush-trace");
                sw.AppendLine(@"	close $tracefd");
                sw.AppendLine(@"	#close $namtrace");
                sw.AppendLine();
                sw.AppendLine(@"	puts ""end simulation""");
                sw.AppendLine();
                sw.AppendLine(@"	set runTime [clock second]");
                sw.AppendLine(@"	set runTime [expr $runTime - $startTime]");
                sw.AppendLine();
                sw.AppendLine(@"	set s [expr $runTime % 60];	set runTime [expr $runTime / 60];");
                sw.AppendLine(@"	set m [expr $runTime % 60];	set runTime [expr $runTime / 60];");
                sw.AppendLine();
                sw.AppendLine(@"	puts ""Runtime: $runTime hours, $m minutes, $s seconds""");
                sw.AppendLine();
                sw.AppendLine(@"	$ns_ halt");
                sw.AppendLine(@"	exit 0");
                sw.AppendLine(@"}");
                sw.AppendLine();
                sw.AppendLine(@"$ns_ run");
                sw.AppendLine();
                sw.AppendLine(@"########### end script #####################");

                #endregion simulate.tcl

                #region node location data

                sw = Scripts[TopoDataScript];
                sw.Clear();

                sw.AppendLine("# node location data");
                for (int i = 0; i < project.Network.NodeList.Count; i++)
                {
                    sw.Append("$mnode_(" + i + ") set X_ " + ((INode)project.Network.NodeList[i]).X + " ;\t");
                    sw.Append("$mnode_(" + i + ") set Y_ " + ((INode)project.Network.NodeList[i]).Y + " ;\t");
                    sw.AppendLine("$mnode_(" + i + ") set Z_ 0");
                }

                #endregion node location data

                #region node off data

                sw = Scripts[NodesOffScript];
                sw.Clear();

                sw.AppendLine("# node off data");

                int j = 0;
                foreach (INode node in project.Network.NodeList)
                {
                    if (node.OffTime > 0)
                    {
                        sw.AppendLine("set offv(" + j + ") " + node.ID + "\t;\tset offt(" + j + ") " + node.OffTime);
                        j++;
                    }
                }

                sw.AppendLine("set opt(offn) " + j);
                sw.AppendLine();
                sw.AppendLine("for {set i 0} {$i < $opt(offn)} {incr i} {");
                sw.AppendLine("\t$ns_ at $offt($i) \"$mnode_($offv($i)) off\"");
                sw.AppendLine();
                sw.AppendLine("\tfor {set j 0} {$j < $opt(tn)} {incr j} {");
                sw.AppendLine("\t\tif {$s($j) == $offv($i)} {");
                sw.AppendLine("\t\t\t$ns_ at $offt($i) \"$cbr_($j) stop\"");
                sw.AppendLine("\t\t}");
                sw.AppendLine("\t}");
                sw.AppendLine("}");

                #endregion node off data

                #region cbr data

                sw = Scripts[TrafficsScript];
                sw.Clear();

                sw.AppendLine("# udp data");
                sw.AppendLine();
                sw.AppendLine("set opt(tn) " + project.Network.Connects.Count);

                var cbrSetting = project.Setting.SelectSingleNode("/setting/apps/app[@type='CBR']").FirstChild;
                sw.AppendLine("set opt(interval_1) " + cbrSetting.SelectSingleNode("descendant::param[@property='interval_1_']").Attributes["value"].Value);
                sw.AppendLine("set opt(interval) " + cbrSetting.SelectSingleNode("descendant::param[@property='interval_']").Attributes["value"].Value);
                sw.AppendLine("set opt(cbr_start_1_) " + cbrSetting.SelectSingleNode("descendant::param[@property='cbr_start_1_']").Attributes["value"].Value);
                sw.AppendLine("set opt(cbr_start_) " + cbrSetting.SelectSingleNode("descendant::param[@property='cbr_start_']").Attributes["value"].Value);
                sw.AppendLine();

                int index = 0;
                foreach (UcConnect c in project.Network.Connects)
                {
                    sw.AppendLine("set s(" + index + ")\t" + c.SourceNode.ID + "\t;\tset d(" + index + ")\t" + c.DestNode.ID);
                    index++;
                }

                string t = @"for {set i 0} {$i < $opt(tn)} {incr i} {
    $mnode_($s($i)) setdest [$mnode_($d($i)) set X_] [$mnode_($d($i)) set Y_] 0

    set sink_($i) [new Agent/Null]
    set udp_($i) [new Agent/UDP]
    $ns_ attach-agent $mnode_($d($i)) $sink_($i)
    $ns_ attach-agent $mnode_($s($i)) $udp_($i)
    $ns_ connect $udp_($i) $sink_($i)
    $udp_($i) set fid_ 2

    #Setup a CBR over UDP connection
    set cbr_($i) [new Application/Traffic/CBR]
    $cbr_($i) attach-agent $udp_($i)
    $cbr_($i) set type_ CBR
    $cbr_($i) set packet_size_ 50
    $cbr_($i) set rate_ 0.1Mb
    $cbr_($i) set interval_ $opt(interval_1)
    #$cbr set random_ false

    $ns_ at [expr $opt(cbr_start_1_) + [expr $i - 1] * $opt(interval_1) / $opt(tn)] ""$cbr_($i) start""
    $ns_ at [expr $opt(cbr_start_1_) + 99] ""$cbr_($i) stop""

    # Setup a CBR over UDP connection
    set cbr2_($i) [new Application/Traffic/CBR]
    $cbr2_($i) attach-agent $udp_($i)
    $cbr2_($i) set type_ CBR
    $cbr2_($i) set packet_size_ 50
    $cbr2_($i) set rate_ 0.1Mb
    $cbr2_($i) set interval_ $opt(interval)

    $ns_ at [expr $opt(cbr_start_) + [expr $i - 1] * $opt(interval) / $opt(tn)] ""$cbr2_($i) start""
    $ns_ at [expr $opt(stop) - 5] ""$cbr2_($i) stop""
}";
                sw.AppendLine();
                sw.AppendLine(t);
                //sw.AppendLine("for {set i 0} {$i < $opt(tn)} {incr i} {");
                //sw.AppendLine("\t$mnode_($s($i)) setdest [$mnode_($d($i)) set X_] [$mnode_($d($i)) set Y_] 0");
                //sw.AppendLine();
                //sw.AppendLine("\tset sink_($i) [new Agent/Null]");
                //sw.AppendLine("\tset udp_($i) [new Agent/UDP]	");
                //sw.AppendLine("\t$ns_ attach-agent $mnode_($d($i)) $sink_($i)");
                //sw.AppendLine("\t$ns_ attach-agent $mnode_($s($i)) $udp_($i)");
                //sw.AppendLine("\t$ns_ connect $udp_($i) $sink_($i)");
                //sw.AppendLine("\t$udp_($i) set fid_ 2");
                //sw.AppendLine();
                //sw.AppendLine("\t#Setup a CBR over UDP connection");
                //sw.AppendLine("\tset cbr_($i) [new Application/Traffic/CBR]");
                //sw.AppendLine("\t$cbr_($i) attach-agent $udp_($i)");
                //sw.AppendLine("\t$cbr_($i) set type_ CBR");
                //sw.AppendLine("\t$cbr_($i) set packet_size_ 50");
                //sw.AppendLine("\t$cbr_($i) set rate_ 0.1Mb");
                //sw.AppendLine("\t$cbr_($i) set interval_ $opt(interval)");
                //sw.AppendLine("\t#$cbr set random_ false");
                //sw.AppendLine();
                //sw.AppendLine("\t$ns_ at [expr 100 + [expr $i - 1] * $opt(interval) / $opt(tn)] \"$cbr_($i) start\"");
                //sw.AppendLine("\t$ns_ at [expr $opt(stop) - 5] \"$cbr_($i) stop\"");
                //sw.AppendLine("}");

                #endregion cbr data
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }
    }
}