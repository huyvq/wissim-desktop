﻿using System;
using System.Linq;
using System.Windows;

namespace WisSim.Editor
{
    /// <summary>
    /// Interaction logic for AddNodesWindow.xaml
    /// <author>Trong Nguyen</author>
    /// </summary>
    public partial class GenHole : Window
    {
        private bool result = false;

        public GenHole()
        {
            InitializeComponent();
        }

        public Nullable<bool> ShowDialog()
        {
            base.ShowDialog();
            return result;
        }

        public int NumNode
        {
            get { return int.Parse(NodeNumTexbox.Text); }
        }

        private void OnGenerateButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            result = true;
            this.Close();
        }

        private void OnCancelButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            result = false;
            this.Close();
        }
    }
}