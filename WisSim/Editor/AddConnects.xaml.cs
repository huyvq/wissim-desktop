﻿using System.Windows;

namespace WisSim.Editor
{
    /// <summary>
    /// Interaction logic for AddConnects.xaml
    /// </summary>
    public partial class AddConnects
    {
        private bool result;

        public AddConnects()
        {
            InitializeComponent();
        }

        public bool? ShowDialog()
        {
            base.ShowDialog();
            return result;
        }

        public int Type { get { return TabControl.SelectedIndex; } }

        # region

        public string SourceKey { get { return SourceBox.Text; } }

        public string DestKey { get { return DestBox.Text; } }

        public int NumberConnection { get { return int.Parse(NumberOfConnections.Text); } }

        # endregion

        # region

        public int SourceDistance { get { return int.Parse(sourceDist.Text); } }

        public int DestDistance { get { return int.Parse(destDist.Text); } }

        public int SourceMargin { get { return int.Parse(sourceMargin.Text); } }

        public int DestMargin { get { return int.Parse(destMargin.Text); } }

        # endregion

        private void OnGenerateButtonClick(object sender, RoutedEventArgs e)
        {
            result = true;
            Close();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            result = false;
            Close();
        }
    }
}
