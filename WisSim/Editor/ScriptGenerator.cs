﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using WisSim.Model;

namespace WisSim.Editor
{
    /// <summary>
    /// Interface for script generator
    /// <author>Trong Nguyen</author>
    /// </summary>
    public abstract class ScriptGenerator
    {
        public string Name { get; set; }

        public Dictionary<string, StringBuilder> Scripts { get; private set; }

        protected ScriptGenerator(string name)
        {
            Name = name;
            Scripts = new Dictionary<string, StringBuilder>();
        }

        public abstract void Generate(Project project);

        public bool SaveFiles(Project project)
        {
            Generate(project);

            if (project.WorkingDirectory != null)
            {
                try
                {
                    foreach (var key in Scripts.Keys)
                    {
                        var sw = new StreamWriter(project.WorkingDirectory + "/" + key);
                        sw.Write(Scripts[key].ToString());
                        sw.Close();                        
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.StackTrace);
                }
            }
            return false;
        }        
    }
}
