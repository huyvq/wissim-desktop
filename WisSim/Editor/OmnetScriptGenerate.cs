﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WisSim.Model;
using WisSim.NetworkComponents;

namespace WisSim.Editor
{
    public class OmnetScriptGenerate : ScriptGenerator
    {
        private const string IniFile = "omnetpp.ini";

        public OmnetScriptGenerate()
            : base("Omnet")                    // name of this Generator
        {
            Scripts.Add(IniFile, new StringBuilder());
        }

        public override void Generate(Project project)
        {
            StringBuilder sw = Scripts[IniFile];
            sw.Clear();

            sw.AppendLine(@"[General]");
            sw.AppendLine(@"debug-on-errors=true");
            sw.AppendLine();
            sw.AppendLine(@"num-rngs = 3");
            sw.AppendLine(@"**.mobility.rng-0 = 1");
            sw.AppendLine(@"**.wlan[*].mac.rng-0 = 2");
            sw.AppendLine();
            sw.AppendLine(@"#tkenv-plugin-path = ../../../etc/plugins");
            sw.AppendLine();
            sw.AppendLine(@"# mobility");
            sw.AppendLine(@"**.host[*].mobilityType = ""StationaryMobility""");
            sw.AppendLine(@"**.mobility.constraintAreaMinZ = 0m");
            sw.AppendLine(@"**.mobility.constraintAreaMaxZ = 0m");
            sw.AppendLine();
            sw.AppendLine(@"# nic settings");
            sw.AppendLine(@"**.wlan[*].mgmtType = ""Ieee80211MgmtAdhoc""");
            sw.AppendLine(@"**.wlan[*].bitrate = 2Mbps");
            sw.AppendLine(@"**.wlan[*].mgmt.frameCapacity = 10");
            sw.AppendLine(@"**.wlan[*].mac.address = ""auto""");
            sw.AppendLine(@" * *.wlan[*].mac.maxQueueSize = 14");
            sw.AppendLine(@"**.wlan[*].mac.rtsThresholdBytes = 3000B");
            sw.AppendLine(@"**.wlan[*].mac.retryLimit = 7");
            sw.AppendLine(@"**.wlan[*].mac.cwMinData = 7");
            sw.AppendLine(@"**.wlan[*].mac.cwMinMulticast = 31");
            sw.AppendLine(@"**.wlan[*].radio.transmitter.power = 0.053mW # communication range = 40m");
            sw.AppendLine();
            sw.AppendLine(@"# energy model");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumerType = ""StateBasedEnergyConsumer""");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumer.offPowerConsumption = 0mW");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumer.sleepPowerConsumption = 1mW");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumer.switchingPowerConsumption = 1mW");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumer.receiverIdlePowerConsumption = 2mW");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumer.receiverBusyPowerConsumption = 5mW");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumer.receiverReceivingPowerConsumption = 10mW");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumer.transmitterIdlePowerConsumption = 2mW");
            sw.AppendLine(@"*.host*.wlan[0].radio.energyConsumer.transmitterTransmittingPowerConsumption = 100mW");
            sw.AppendLine(@"*.host*.energyStorageType = ""IdealEnergyStorage""");
            sw.AppendLine();
            sw.AppendLine(@"# udp apps");
            sw.AppendLine(@"**.host[*].numUdpApps = 1 # every node has to have a UDP app listener");
            sw.AppendLine(@"**.host[*].udpApp[*].typename = ""UDPBasicApp""");
            sw.AppendLine(@"**.host[*].udpApp[*].timeToLive = 128");
            sw.AppendLine(@"**.host[*].udpApp[*].messageLength = 512B #");
            sw.AppendLine(@"**.host[*].udpApp[*].sendInterval = 5s + uniform(-0.001s,0.001s)");
            sw.AppendLine(@"**.host[*].udpApp[*].startTime = 10s");
            sw.AppendLine(@"**.host[*].udpApp[*].stopTime = " + project.Network.SimulationTime + "s");
            sw.AppendLine(@"**.host[*].udpApp[0].localPort = 1234");
            sw.AppendLine(@"**.host[*].udpApp[0].destPort = 1234");
            sw.AppendLine();
            sw.AppendLine();
            sw.AppendLine(@"[Config Simulation]");
            sw.AppendLine(@"network = Network");

            sw.AppendLine(@"**.routingAlgo = ""GPSR""");
            sw.AppendLine(@"*.numHosts = " + project.Network.NodeList.Count);
            sw.AppendLine();
            sw.AppendLine(@"# node parameters");
            sw.AppendLine(@"**.range = 40m");
            sw.AppendLine(@"**.helloPeriod = 0");
            sw.AppendLine();
            sw.AppendLine(@"# node location data");
            sw.AppendLine(@"**.host[*].mobility.initFromDisplayString = false");
            for (int i = 0; i < project.Network.NodeList.Count; i++)
            {
                sw.AppendLine(String.Format(@"**.host[{0}].node.ID = {0}", i));
                sw.AppendLine(String.Format(@"**.host[{0}].mobility.initialX = {1}m", i, ((INode)project.Network.NodeList[i]).X));
                sw.AppendLine(String.Format(@"**.host[{0}].mobility.initialY = {1}m", i, ((INode)project.Network.NodeList[i]).Y));
            }

            sw.AppendLine(@"# data");
            foreach (UcConnect c in project.Network.Connects)
            {
                sw.AppendLine(String.Format(@"**.host[{0}].udpApp[0].destAddresses = ""host[{1}]""", c.SourceNode.ID, c.DestNode.ID));
                sw.AppendLine(String.Format(@"**.host[{0}].node.destX = {1}", c.SourceNode.ID, c.DestNode.X));
                sw.AppendLine(String.Format(@"**.host[{0}].node.destY = {1}", c.SourceNode.ID, c.DestNode.Y));
            }
        }
    }
}