﻿using ICSharpCode.AvalonEdit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using WisSim.GraphAlgorithms.Graph;
using WisSim.GraphAlgorithms.Voronoi;
using WisSim.GraphAlgorithms.Voronoi.Views;
using WisSim.Model;
using WisSim.NetworkComponents;

namespace WisSim.Editor
{
    /// <summary>
    /// Interaction logic for Editor.xaml
    /// <author>Trong Nguyen</author>
    /// </summary>
    public partial class Editor
    {
        public Editor(MainWindow mainWindow)
        {
            InitializeComponent();

            Main = mainWindow;
            Network = Project.Network;

            //Network.Update += Update;
            Network.PropertyChanged += (sender, e) => Update();
            DockPanel.Children.Add(Network);
            //Panel.Children.Add(Network);

            Network.GroupAdded += OnGroupAdded;
            Network.GroupDeleted += OnGroupDeleted;
            Network.GroupChanged += OnGroupChanged;

            ScriptGenerators = new List<ScriptGenerator>
            {
                new Ns2ScriptGenerate(),
                new OmnetScriptGenerate()
            };
        }

        # region Properties

        private MainWindow Main { get; set; }

        private Project Project
        {
            get
            {
                return Main != null ? Main.Project : null;
            }
        }

        //private Network Network { get { return Project != null ? Project.Network : null; } }

        public static readonly DependencyProperty NetworkProperty = DependencyProperty.Register(
            "Network", typeof(Network), typeof(Editor), new PropertyMetadata(default(Network)));

        public Network Network
        {
            get { return (Network)GetValue(NetworkProperty); }
            set { SetValue(NetworkProperty, value); }
        }

        private string StatusMessage
        {
            set { Main.StatusBarMessage = value; }
        }

        private XmlDataProvider SettingData { get { return (XmlDataProvider)Resources["Setting"]; } }

        private XmlDataProvider EnergyData { get { return (XmlDataProvider)Resources["Energy"]; } }

        public List<ScriptGenerator> ScriptGenerators
        {
            get { return (List<ScriptGenerator>)GetValue(ScriptGeneratorsProperty); }
            set { SetValue(ScriptGeneratorsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScriptGenerators.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScriptGeneratorsProperty =
            DependencyProperty.Register("ScriptGenerators", typeof(List<ScriptGenerator>), typeof(Editor), new PropertyMetadata(null));

        # endregion

        /// <summary>
        /// Load all setting values from project to view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Loaded(object sender, RoutedEventArgs e)
        {
            if (Environment.GetCommandLineArgs().Length == 2)
            {
                SettingData.Document = Project.Setting;
                EnergyData.Document = Project.Energy;

                //Panel.Height = Network.InitHeight;
                //Panel.Width = Network.InitWidth;
            }
            else
            {
                Project.Setting = SettingData.Document;
                Project.Energy = EnergyData.Document;

                //networkHeight.Value = Network.Height.ToString();
                //networkWidth.Value = Network.Width.ToString();
            }

            SettingData.Document.NodeChanged += EditSetting;
            LoadSetting();

            SimulateTime.Text = Network.SimulationTime.ToString();
        }

        private void Update()
        {
            //StatusMessage = Network.NodeList.Count.ToString();
            if (MainTab.SelectedIndex != 0)
            {
                GenerateTcl();
            }
        }

        #region Command

        // TODO: add command to main menu

        #endregion Command

        #region Setting

        private void LoadSetting()
        {
            BackgroundWorker loadSettingWorker = new BackgroundWorker();
            loadSettingWorker.DoWork += (sender, e) =>
            {
                StatusMessage = "Load Simulation setting...";

                try
                {
                    var oldXml = SettingData.Document;
                    var newXml = new XmlDocument();
                    newXml.LoadXml(Main.GetSimulationSetting());

                    foreach (XmlNode newChild in newXml.ChildNodes[1].ChildNodes)
                    {
                        var listType = oldXml.GetElementsByTagName(newChild.Name)[0];
                        if (listType == null) // add new newChild to oldXML
                        {
                            var xfrag = oldXml.CreateDocumentFragment();
                            xfrag.InnerXml = newChild.OuterXml;
                            oldXml.ChildNodes[1].AppendChild(xfrag);
                        }
                        else // update params
                        {
                            // TOOD: Can be update, using better code
                            // duyệt từng newType con của 1 node và kiểm tra xem có tồn tại hay chưa
                            // O(n^2)
                            foreach (XmlNode newType in newChild.ChildNodes)
                            {
                                foreach (XmlNode localType in listType.ChildNodes)
                                {
                                    if (newType.Attributes["type"].Value == localType.Attributes["type"].Value)
                                    {
                                        try
                                        {
                                            foreach (XmlNode newParam in newType.ChildNodes[0].ChildNodes)
                                            {
                                                foreach (XmlNode localParam in localType.ChildNodes[0].ChildNodes)
                                                {
                                                    if (newParam.Attributes["property"].Value == localParam.Attributes["property"].Value)
                                                    {
                                                        goto childCon;
                                                    }
                                                }
                                                XmlDocumentFragment xxfrag = oldXml.CreateDocumentFragment();
                                                xxfrag.InnerXml = newParam.OuterXml;
                                                localType.AppendChild(xxfrag);
                                            childCon:
                                                ;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            // ignored
                                        }
                                        goto con;
                                    }
                                }
                                XmlDocumentFragment xfrag = oldXml.CreateDocumentFragment();
                                xfrag.InnerXml = newType.OuterXml;
                                listType.AppendChild(xfrag);
                            con:
                                ;
                            }
                        }
                    }

                    StatusMessage = "Setting is updated!";
                }
                catch (Exception ex)
                {
                    StatusMessage = "Update Setting failuse, " + ex.Message;
                    e.Result = null;
                }
            };
            loadSettingWorker.RunWorkerAsync();
        }

        private void EditSetting(object sender, XmlNodeChangedEventArgs e)
        {
            if (e.Node.Name == "prop")
            {
                RunThreshold();
            }
        }

        /**
         * Run Threshold to configure propagationModel and Antenna to fit with nodeRange.
         * Call after setting each ones of nodeRange, propagationModel and Antenna
         */

        private void RunThreshold()
        {
            BackgroundWorker runThresholdWorker = new BackgroundWorker();
            runThresholdWorker.DoWork += (sender, e) =>
            {
                StatusMessage = "Calculating node rage";
                try
                {
                    XmlNode netif = SettingData.Document.GetElementById("netif").SelectSingleNode("/network-interface[@type='Phy/WirelessPhy']/params");
                    XmlNode antenna = SettingData.Document.GetElementById("ant").SelectSingleNode("/antenna[@type='Antenna/OmniAntenna']/params");

                    string threshold = Main.GetThreshold(
                        SettingData.Document.GetElementById("prop").ChildNodes[int.Parse(SettingData.Document.GetElementById("prop").Attributes["choose"].Value)].Attributes["type"].Value.Substring(12)
                        + " "
                        + Network.Range
                        );

                    string[] line = threshold.Split('\n');
                    // 0.  distance =
                    // 1.  propagation model:
                    // 2.
                    // 3.  Selected parameters:
                    // 4.  transmit power:		    	        //netinte.put("Pt_",    line.substring(16));
                    // 5.  frequency:                           //netinte.put("freq_",  line.substring(11));
                    // 6.  transmit antenna gain:               //antenna.put("Gt_",    line.substring(23));
                    // 7.  receive antenna gain:                //antenna.put("Gr_",    line.substring(22));
                    // 8.  system loss:                         //netinte.put("L_",     line.substring(13));
                    // 9.  transmit antenna height:             //antenna.put("Z_",     line.substring(25));
                    // 10. receive antenna height:
                    // 11.
                    // 12. Receiving threshold RXThresh_ is:    //netinte.put("RXThresh_", line.substring(34));

                    netif.SelectSingleNode("/param[@property='Pt_']").Attributes["value"].Value = line[4].Substring(16);
                    netif.SelectSingleNode("/param[@property='freq_']").Attributes["value"].Value = line[5].Substring(11);
                    netif.SelectSingleNode("/param[@property='L_']").Attributes["value"].Value = line[8].Substring(13);
                    netif.SelectSingleNode("/param[@property='RXThresh_']").Attributes["value"].Value = line[12].Substring(34);

                    antenna.SelectSingleNode("/param[@property='Gt_']").Attributes["value"].Value = line[6].Substring(23);
                    antenna.SelectSingleNode("/param[@property='Gr_']").Attributes["value"].Value = line[7].Substring(22);
                    antenna.SelectSingleNode("/param[@property='Z_']").Attributes["value"].Value = line[9].Substring(25);
                }
                catch (Exception err)
                {
                    MessageBox.Show("Something not right\n" + err.Message, "Cannot calulate new radiation setting");
                }
            };
        }

        #endregion Setting

        #region Generate Tcl

        private readonly Dictionary<String, TextEditor> textEditors = new Dictionary<string, TextEditor>();

        private void OnSelectionChangedGenerator(object sender, SelectionChangedEventArgs e)
        {
            while (MainTab.Items.Count > 2)
            {
                MainTab.Items.RemoveAt(2);
                textEditors.Clear();
            }

            ScriptGenerator generator = GeneratorListBox.SelectedItem as ScriptGenerator;

            foreach (var key in generator.Scripts.Keys)
            {
                TextEditor newTextEditor = new TextEditor() { Style = (Style)FindResource("TextEditorStyle") };

                TabItem newTabItem = new TabItem
                {
                    Header = key,
                    Content = newTextEditor
                };
                MainTab.Items.Add(newTabItem);

                textEditors.Add(key, newTextEditor);
            }
        }

        private void GenerateTcl()
        {
            try
            {
                ScriptGenerator generator = GeneratorListBox.SelectedItem as ScriptGenerator;
                generator.Generate(Project);

                foreach (var key in generator.Scripts.Keys)
                {
                    textEditors[key].Text = generator.Scripts[key].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }

        private int lastTab;

        private void SwitchView(object sender, SelectionChangedEventArgs e)
        {
            if (MainTab.SelectedIndex != 0)
            {
                if (lastTab == 0)
                {
                    GenerateTcl();
                }
            }
            else
            {
                ParseTcl();
            }
            lastTab = MainTab.SelectedIndex;
        }

        private void ParseTcl()
        {
            //#region simulate.tcl

            //#endregion

            //# region topo

            //try
            //{
            //    String line = sw.ReadLine();    // first line
            //    line = sw.ReadLine();
            //    do
            //    {
            //        string[] re = line.Split(' ');
            //        int x = int.Parse(re[3]);

            //        line = sw.ReadLine();
            //        re = line.Split(' ');
            //        int y = int.Parse(re[3]);

            //        line = sw.ReadLine();

            //        Network.AddNode(x, y);

            //        line = sw.ReadLine();
            //    }
            //    while (line != null);
            //    sw.Close();

            //    Update(this, null);

            //    //fileName.Text = FileName;
            //}
            //catch
            //{
            //}

            //# endregion

            //# region off_node

            //ofd = new OpenFileDialog();
            //ofd.Title = "Select offNode script file";
            //ofd.Filter = "OffNode|nodeoff.tcl|All|*.*";

            //if (ofd.ShowDialog() == true)
            //{
            //    WorkingDirectory = ofd.FileName;

            //    try
            //    {
            //        StreamReader sw = new StreamReader(WorkingDirectory);
            //        String line = sw.ReadLine();    // first line
            //        line = sw.ReadLine();
            //        do
            //        {
            //            string[] re = line.Split(' ', '\t');
            //            int id = int.Parse(re[2]);
            //            int ti = int.Parse(re[6]);

            //            ((Node)Network.NodeList[id]).OffTime = ti;

            //            line = sw.ReadLine();
            //        }
            //        while (!String.IsNullOrWhiteSpace(line));
            //        sw.Close();

            //        Update(this, null);

            //        //fileName.Text = FileName;
            //    }
            //    catch
            //    {
            //    }
            //}

            //# endregion

            //# region cbr

            //ofd = new OpenFileDialog();
            //ofd.Title = "Select CBR script file";
            //ofd.Filter = "CBR|cbr.tcl|All|*.*";

            //if (ofd.ShowDialog() == true)
            //{
            //    WorkingDirectory = ofd.FileName;

            //    try
            //    {
            //        StreamReader sw = new StreamReader(WorkingDirectory);
            //        String line = sw.ReadLine();    // first line
            //        line = sw.ReadLine();           // second line
            //        line = sw.ReadLine();           // set opt(tn)
            //        line = sw.ReadLine();           // set opt(interval)
            //        line = sw.ReadLine();           //
            //        line = sw.ReadLine();
            //        do
            //        {
            //            string[] re = line.Split(' ', '\t');
            //            int s = int.Parse(re[2]);
            //            int d = int.Parse(re[6]);

            //            Network.AddConnect(Network.NodeList[s] as Node, Network.NodeList[d] as Node);

            //            line = sw.ReadLine();
            //        }
            //        while (!String.IsNullOrWhiteSpace(line));
            //        sw.Close();

            //        Update(this, null);

            //        //fileName.Text = FileName;
            //    }
            //    catch
            //    {
            //    }
            //}

            //# endregion
        }

        private bool SaveTclFile()
        {
            StatusMessage = "Save scritp files...";

            if (Main.Project.WorkingDirectory != null)
            {
                try
                {
                    ScriptGenerator generator = GeneratorListBox.SelectedItem as ScriptGenerator;
                    generator.SaveFiles(Project);
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.StackTrace);
                    //MessageBox.Show(e.Message, "Cannot save files");
                }
            }
            return false;
        }

        # endregion

        private void Run(object sender, RoutedEventArgs e)
        {
            //Save Project before running
            Main.SaveProject();
            //Save
            MainTab.SelectedIndex = 1;
            if (SaveTclFile())
            {
                StatusMessage = "Call remote simulation function";
                ScriptGenerator generator = GeneratorListBox.SelectedItem as ScriptGenerator;
                List<string> list = new List<string>();
                foreach (var key in generator.Scripts.Keys)
                {
                    list.Add(key);
                }
                Main.RunSimulation(list.ToArray());
            }
        }

        #region Design

        private void BtnCreateConnect_Click(object sender, RoutedEventArgs e)
        {
            AddConnects addConnects = new AddConnects();
            addConnects.DataContext = Project;
            if (addConnects.ShowDialog() == true)
            {
                try
                {
                    if (addConnects.Type == 0)
                    {
                        Network.AddConnection(addConnects.SourceKey, addConnects.DestKey, addConnects.NumberConnection);
                    }
                    else
                    {
                        Network.AddConnection(addConnects.SourceDistance, addConnects.DestDistance, addConnects.SourceMargin, addConnects.DestMargin);
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show(er.Message, "Cannot automatically generate connections!");
                }
            }
        }

        private void BtnRandom_Click(object sender, RoutedEventArgs e)
        {
            AddNodes i = new AddNodes();
            if (i.ShowDialog() == true)
            {
                try
                {
                    Network.AddRandomNodes(i.NumNode, i.Rand);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ChbToggleEdgesClick(object sender, RoutedEventArgs e)
        {
            Network.ShowEdges = (bool)ChbToggleEdges.IsChecked;
        }

        private void ChbToggleConnectClick(object sender, RoutedEventArgs e)
        {
            Network.ConnectCanvas.Visibility = (bool)ChbToggleConnect.IsChecked ? Visibility.Visible : Visibility.Hidden;
            foreach (UcConnect uc in Network.ConnectCanvas.Children)
            {
                uc.SourceNode.States = NodeStates.Source;
                uc.DestNode.States = NodeStates.Dest;
            }
        }

        private void ChbToggleSelectClick(object sender, RoutedEventArgs e)
        {
            if (Network != null)
            {
                if ((sender as CheckBox).IsChecked == true)
                {
                    Network.NodeCanvas.EditingMode = InkCanvasEditingMode.Select;
                    Network.NodeCanvas.ContextMenu.Visibility = Visibility.Visible;
                }
                else
                {
                    Network.NodeCanvas.EditingMode = InkCanvasEditingMode.None;
                    Network.NodeCanvas.ContextMenu.Visibility = Visibility.Hidden;
                }
            }
        }

        private void ChbToggleSelectHoleClick(object sender, RoutedEventArgs e)
        {
            if ((sender as CheckBox).IsChecked == true)
            {
                Network.IsSelectHoles = true;
                Network.NodeCanvas.IsEnabled = false;
                Network.NodeCanvas.IsHitTestVisible = false;
            }
            else
            {
                Network.IsSelectHoles = false;
                Network.NodeCanvas.IsEnabled = true;
                Network.NodeCanvas.IsHitTestVisible = true;
            }
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Network.Reset();
        }

        private void TimerChange(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Network.CurrentTime = (int)Timer.Value;
        }

        private void SimulateTimeChange(object sender, TextChangedEventArgs e)
        {
            int newTime;

            try
            {
                newTime = int.Parse(SimulateTime.Text);
            }
            catch
            {
                SimulateTime.Text = Network.SimulationTime.ToString();
                return;
            }

            if (Network != null && Network.SimulationTime != newTime)
            {
                Network.SimulationTime = newTime;
                Timer.Maximum = newTime;
            }
        }

        #endregion Design

        #region Hole Properties

        private void MinRadius_ValueChanged(object sender, EventArgs e)
        {
            var s = sender as PropertyEntry;
            try
            {
                Network.MinRadius = Double.Parse(s.Value);
            }
            catch (Exception)
            {
                Minr.Value = 100.ToString();
            }
        }

        private void MaxRadius_ValueChanged(object sender, EventArgs e)
        {
            var s = sender as PropertyEntry;
            try
            {
                Network.MaxRadius = Double.Parse(s.Value);
            }
            catch (Exception)
            {
                Maxr.Value = 500.ToString();
            }
        }

        #endregion Hole Properties

        #region Group/label of editor

        public event EventHandler<NodesGroupEventArgs> GroupDeleted;

        public event EventHandler<NodesGroupEventArgs> GroupAdded;

        public event EventHandler<NodesGroupsChangedEventArgs> GroupChanged;

        private void OnGroupDeleted(object sender, NodesGroupEventArgs e)
        {
            if (GroupDeleted != null)
            {
                GroupDeleted(this, e);
            }
        }

        private void OnGroupAdded(object sender, NodesGroupEventArgs e)
        {
            List<String> groupNames = Network.GroupsList.Keys.ToList();
            GroupGrid.ItemsSource = groupNames;
            GroupGrid.Items.Refresh();
            NodeGroup.IsExpanded = true;
            if (GroupAdded != null)
            {
                GroupAdded(this, e);
            }
        }

        private void OnGroupChanged(object sender, NodesGroupsChangedEventArgs e)
        {
            if (GroupChanged != null)
            {
                GroupChanged(this, e);
            }
        }

        private void DeleteGroup_Click(object sender, RoutedEventArgs e)
        {
            if (GroupDeleted != null)
            {
                string deletedGroupName = (string)GroupGrid.SelectedItem;
                NodesGroup deletedGroup = new NodesGroup { Name = deletedGroupName };
                if (Network.GroupsList.ContainsKey(deletedGroupName))
                {
                    foreach (Model.Node node in Network.GroupsList[deletedGroupName])
                    {
                        deletedGroup.Nodes.Add(node.ID);
                    }

                    //Delete in network group list
                    Network.GroupsList.Remove(deletedGroupName);
                }

                //edit graphic interface
                var groupNames = Network.GroupsList.Keys.ToList();
                GroupGrid.ItemsSource = groupNames;
                GroupGrid.Items.Refresh();

                //Delete in main group list
                GroupDeleted(this, new NodesGroupEventArgs(deletedGroup));
            }
        }

        private void GroupGridSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (GroupGrid.SelectedItem != null)
            {
                string selectedGroupName = (string)GroupGrid.SelectedItem;
                if (Network.GroupsList.ContainsKey(selectedGroupName))
                {
                    Network.SelectGroup(Network.GroupsList[selectedGroupName]);
                }
            }
        }

        #endregion Group/label of editor

        #region Graph Algorithms

        private void GenerateVoronoi(object sender, RoutedEventArgs e)
        {
            Network.GenerateVoronoi();
        }

        private void GenerateDelaunay(object sender, RoutedEventArgs e)
        {
            Network.GenerateDelaunay();
        }

        private void ClearVoronoi(object sender, RoutedEventArgs e)
        {
            Network.ClearGraphs();
        }

        private void CalculateDijkstra(object sender, RoutedEventArgs e)
        {
            DebugConsole.Document.Blocks.Clear();
            List<Model.Node> nodeList = new List<Model.Node>();
            foreach (Model.Node n in Network.NodeList)
            {
                nodeList.Add(n);
            }

            Graph graph = new Graph(nodeList);
            foreach (UcConnect c in Network.Connects)
            {
                var path = graph.Dijkstra(c.SourceNode, c.DestNode);
                DebugConsole.AppendText(c.SourceNode.ID + "\t" + c.DestNode.ID + "\t" + (path.Count - 1) + "\t");
                foreach (var p in path)
                {
                    DebugConsole.AppendText(p.ID + ", ");
                }
                DebugConsole.AppendText("\r");
            }
        }

        #endregion Graph Algorithms

        #region Generate random hole

        private void GenerateRandomHole(object sender, RoutedEventArgs e)
        {
            var i = new GenHole();
            if (i.ShowDialog() == true)
            {
                try
                {
                    var node = Network.NodeList[0] as Model.Node;
                    Network.GenerateRandomHole(i.NumNode, node.Range * Math.Sqrt(2));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        #endregion Generate random hole
    }
}