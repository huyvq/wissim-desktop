﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using WissimConnector;

namespace Main.Controller.Connector
{
    internal class ClientServerConnector
    {
        #region Properties

        private string _serverAddress = "localhost";
        private int _serverPort = 58250;
        private int _clientPort = 58251;
        private WissimConnector.WissimConnectorService.Client _client;
        private bool _isConnected = false;
        private int timeWattingForRetry = 200;
        private int numberOfRetry = 1;

        public string ServerAddress
        {
            get
            {
                return _serverAddress;
            }
            set
            {
                _serverAddress = value;
            }
        }

        public int ServerPort
        {
            get { return _serverPort; }
            set { _serverPort = value; }
        }

        public int ClientPort
        {
            get { return _clientPort; }
            set { _clientPort = value; }
        }

        #endregion Properties

        #region Constructor

        public ClientServerConnector()
        {
        }

        public ClientServerConnector(string serverAddress)
        {
            this._serverAddress = serverAddress;
        }

        public ClientServerConnector(string serverAddress, int serverPort)
        {
            this._serverPort = serverPort;
            this._serverAddress = serverAddress;
        }

        public ClientServerConnector(string serverAddress, int serverPort, int clientPort)
        {
            this._serverPort = serverPort;
            this._serverAddress = serverAddress;
            this._clientPort = clientPort;
        }

        #endregion Constructor

        #region Method

        public int connectToServer(string serverAddress, int serverPort)
        {
            this._serverPort = serverPort;
            this._serverAddress = serverAddress;
            return this.connectToServer();
        }

        //Return 0 if success, 1 if false;
        public int connectToServer()
        {
            if (_isConnected == true)
            {
                return 0;
            }
            else
            {
                Thrift.Transport.TTransport transportServer = new Thrift.Transport.TSocket(this._serverAddress, this._serverPort);
                ((Thrift.Transport.TSocket)transportServer).TcpClient.NoDelay = true;
                Thrift.Protocol.TProtocol protocolServer = new Thrift.Protocol.TBinaryProtocol(transportServer);
                this._client = new WissimConnector.WissimConnectorService.Client(protocolServer);

                bool isConnectSuccess = ConnectToServerWithRetry(transportServer, timeWattingForRetry, numberOfRetry);
                if (!isConnectSuccess)
                {
                    Console.WriteLine("Can not connect to WissimServer at " + this._serverAddress + " on port " + this._serverPort.ToString());
                    // Close transport
                    transportServer.Close();
                    Exception ex = new Exception("Can not connect to WissimServer at " + this._serverAddress + " on port " + this._serverPort.ToString());
                    _isConnected = false;
                    throw ex;
                }
                else
                {
                    _isConnected = true;
                    return 0;
                }
            }
        }

        private bool ConnectToServerWithRetry(Thrift.Transport.TTransport transport, int period, int maxTriedNumber)
        {
            for (int i = 0; i < maxTriedNumber; i++)
            {
                try
                {
                    transport.Open();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + ex.StackTrace.ToString());
                    Console.WriteLine("Retry connect to WissimServer the " + (i + 1).ToString() + " time");
                    Thread.Sleep(period);
                    continue;
                }
            }
            return false;
        }

        /*
         *  Run Simulation
         *  Return
         *     statusCode: 0 (success)
         *     result: sessionID of job on server
         *  or
         *      statusCode: 1 (failed)
         *      result: error information.
         */

        public WissimConnector.AbtractResult runSimulation(string projectDirectory, string[] scriptFileNames, string startScriptFileName)
        {
            if (_isConnected == false)
            {
                try
                {
                    int serverConnected = connectToServer();
                    if (serverConnected == 1)
                    {
                        AbtractResult result = new AbtractResult();
                        result.StatusCode = 1;
                        result.Result = "Can not connect to server";
                        _isConnected = false;
                        throw new Exception(result.Result);
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            _isConnected = true;
            //1. Zip script file
            //1.1 Adjust file_name
            //string currentDir = System.IO.Directory.GetCurrentDirectory();
            //string scriptDir = System.IO.Path.Combine(currentDir, "tcl");
            string scriptDir = projectDirectory;
            if (System.IO.Directory.Exists(scriptDir) == false)
            {
                System.IO.Directory.CreateDirectory(scriptDir);
            }
            string zipPath = System.IO.Path.Combine(scriptDir, "tcl.zip");

            //1.2 Zip file

            if (System.IO.File.Exists(zipPath))
            {
                System.IO.File.Delete(zipPath);
            }
            try
            {
                ZipArchive zip = ZipFile.Open(zipPath, ZipArchiveMode.Create);
                //string[] files = scriptFileNames.ToArray();
                foreach (string file in scriptFileNames)
                {
                    zip.CreateEntryFromFile(System.IO.Path.Combine(projectDirectory, file), file, CompressionLevel.Optimal);
                }
                zip.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw ex;
            }

            //2. Read zip file
            //string zipFile;
            //System.IO.StreamReader streamReader = new System.IO.StreamReader(zipPath);
            //zipFile = streamReader.ReadLines();
            //streamReader.Close();

            byte[] zipFileBinary = System.IO.File.ReadAllBytes(zipPath);
            //3. Sent to server
            Dictionary<string, string> options = new Dictionary<string, string>();
            options.Add("0", startScriptFileName);
            return _client.run(zipFileBinary, options);
        }

        /*
         *  Get network setting in Serverside
         *  Return
         *     statusCode: 0 (success)
         *     result: network setting in xml format
         *  or
         *      statusCode: 1 (failed)
         *      result: error information.
         */

        public WissimConnector.AbtractResult getNetworkSetting()
        {
            if (_isConnected == false)
            {
                int serverConnected = connectToServer();
                if (serverConnected == 1)
                {
                    throw new Exception("Server not found.");
                }
            }
            _isConnected = true;
            return _client.getSetting("network");
        }

        /*
        *  Get tracefile from in Serverside
        *  Return
        *     statusCode: 0 (success)
        *     result: network setting in xml format
        *  or
        *      statusCode: 1 (failed)
        *      comment: error information.
        */

        public WissimConnector.AbtractResult getTraceFile(string sessionID, string workingDirectory)
        {
            AbtractResult result = new AbtractResult();

            if (_isConnected == false)
            {
                int serverConnected = connectToServer();
                if (serverConnected == 1)
                {
                    result.StatusCode = 1;
                    result.Result = "Can not connect to server";
                    throw new Exception("Server not found.");
                    _isConnected = false;
                    return result;
                }
            }
            _isConnected = true;
            FileResult fileResult = _client.getTraceFile(sessionID);
            if (fileResult.StatusCode == 0)
            {
                //0. Read a zip file
                //Change default folder here when needed!!!.
                string currentDir = workingDirectory; //System.IO.Directory.GetCurrentDirectory();
                //string scriptDir = System.IO.Path.Combine(currentDir, "tcl");
                //string zipPath = System.IO.Path.Combine(scriptDir, "script.zip");
                //byte[] zipFileBinary = System.IO.File.ReadAllBytes(zipPath);
                byte[] zipFileBinary = fileResult.Result;
                //1. Save script file
                string traceDir = System.IO.Path.Combine(currentDir, "trace");
                if (!System.IO.Directory.Exists(traceDir))
                {
                    System.IO.Directory.CreateDirectory(traceDir);
                }

                string sessionDir = System.IO.Path.Combine(traceDir, sessionID);
                if (!System.IO.Directory.Exists(sessionDir))
                {
                    System.IO.Directory.CreateDirectory(sessionDir);
                }

                string tracePath = System.IO.Path.Combine(sessionDir, "trace.zip");
                System.IO.File.WriteAllBytes(tracePath, zipFileBinary);

                //2. Extract file
                ZipFile.ExtractToDirectory(tracePath, sessionDir);

                result.StatusCode = 0;
                result.Result = "Get file successfully";
            }
            else
            {
                result.StatusCode = 1;
                result.Result = fileResult.Comment;
                throw new Exception(fileResult.Comment);
            }

            //Return
            return result;
        }

        public WissimConnector.AbtractResult getSessionStatus(string sessionID)
        {
            AbtractResult result = new AbtractResult();

            if (_isConnected == false)
            {
                int serverConnected = connectToServer();
                if (serverConnected == 1)
                {
                    result.StatusCode = 1;
                    result.Result = "Can not connect to server";
                    throw new Exception("Server not found.");
                    _isConnected = false;
                    return result;
                }
            }

            _isConnected = true;
            AbtractResult absResult = _client.getSessionStatus(sessionID);
            if (absResult.StatusCode != 0)
            {
                throw new Exception(absResult.Result);
            }
            return absResult;
        }

        public void Close()
        {
            Thrift.Protocol.TProtocol protocolServer = _client.InputProtocol;
            Thrift.Transport.TTransport transportServer = protocolServer.Transport;
            transportServer.Close();
        }

        #endregion Method
    }
}