﻿using System;
using System.Collections.Generic;
using System.Linq;
using WisSim.Model;

namespace WisSim.Controller.Parser
{
    /// <summary>
    /// Interface for Parser
    /// <author>Trong Nguyen</author>
    /// </summary>
    internal interface IParser
    {
        //TODO: NodeData --> INode
        //PacketData --> IPacket
        Dictionary<int, NodeData> GetNodes();

        Dictionary<int, PacketData> GetPackets();

        List<EventData> GetEvents();

        List<EventData> GetSimulateEvents();

        List<string> GetPacketTypes();

        Dictionary<string, object> GetOptData();
    }
}