﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using WisSim.Model;

namespace WisSim.Controller.Parser
{
    /// <summary>
    /// NS2 Parser, parser for ns2 trace file
    /// <author>Trong Nguyen</author>
    /// </summary>
    public class NS2Parser : IParser
    {
        private Dictionary<int, Model.NodeData> nodes = new Dictionary<int, Model.NodeData>();
        private Dictionary<int, Model.PacketData> packets = new Dictionary<int, Model.PacketData>();
        private List<Model.EventData> events = new List<Model.EventData>();
        private List<Model.EventData> simulateEvents = new List<Model.EventData>();
        private Dictionary<string, object> optData = new Dictionary<string, object>();
        private List<string> packetTypes = new List<string>();
        private readonly double energyPatternRate = 0.1; //(second)

        public const string TRACE_FILE_PREFIX = "TRACE";
        public const string NODE_FILE_PREFIX = "NODE";
        public const string OPTDATA_FILE_PREFIX = "OPTDATA"; // with option data require input trace file in format: OPTDATA-<protocol>-<opt id>-<file path>

        public const string OPT_BOUNDHOLE = "BOUNDHOLE";
        public const string OPT_PARALLELOGRAM = "PARALLELOGRAM";
        public const string OPT_ADJACENTHOLE = "ADJACENTHOLE";

        public Dictionary<int, Model.NodeData> GetNodes()
        {
            if (nodes == null) nodes = new Dictionary<int, Model.NodeData>();
            return nodes;
        }

        public Dictionary<int, Model.PacketData> GetPackets()
        {
            if (packets == null) packets = new Dictionary<int, Model.PacketData>();
            return packets;
        }

        public List<Model.EventData> GetEvents()
        {
            if (events == null) events = new List<Model.EventData>();
            return events;
        }

        public List<Model.EventData> GetSimulateEvents()
        {
            if (simulateEvents == null) simulateEvents = new List<EventData>();
            return simulateEvents;
        }

        public List<string> GetPacketTypes()
        {
            if (packetTypes == null)
            {
                packetTypes = new List<string>();
            }

            return packetTypes;
        }

        public Dictionary<string, object> GetOptData()
        {
            if (optData == null)
            {
                optData = new Dictionary<string, object>();
            }
            return optData;
        }

        public void Parse(string[] traceFiles)
        {
            if (traceFiles.Length >= 2)
            {
                Console.WriteLine(DateTime.Now.ToString("hh.mm.ss.ffffff"));
                foreach (string s in traceFiles)
                {
                    parseSelect(s);
                }
                Console.WriteLine(DateTime.Now.ToString("hh.mm.ss.ffffff"));
            }
            else
            {
                throw new Exception("You have to choose at least 2 files");
            }
        }

        private void parseSelect(string prefixFile)
        {
            int index = prefixFile.IndexOf('-');
            if (index < 0) return;

            string prefix = prefixFile.Substring(0, index);
            string traceFile = prefixFile.Substring(index + 1);
            if (prefix == NODE_FILE_PREFIX)
            {
                ParseNeighbourFile(traceFile);
            }
            else if (prefix == TRACE_FILE_PREFIX)
            {
                ParseMainFile(traceFile);
            }
            else if (prefix == OPTDATA_FILE_PREFIX)
            {
                string[] split = traceFile.Split('-');
                if (split.Length < 3) return;

                string protocol = split[0];
                string optId = split[1];
                traceFile = traceFile.Substring(split[0].Length + split[1].Length + 2);

                if (protocol == "ELBARGRIDOFFLINE")
                {
                    if (optId == OPT_BOUNDHOLE)
                    {
                        optData.Add(OPT_BOUNDHOLE, ParseBoundHoleFile(traceFile));
                    }
                    else if (optId == OPT_PARALLELOGRAM)
                    {
                        optData.Add(OPT_PARALLELOGRAM, ParseElbarParallelogramFile(traceFile));
                    }

                    return;
                }

                if (protocol == "BEHDS")
                {
                    if (optId == OPT_BOUNDHOLE)
                    {
                        optData.Add(OPT_BOUNDHOLE, ParseEllipseHoleFile(traceFile));
                    }

                    return;
                }

                if (protocol == "GRIDDYNAMIC")
                {
                    if (optId == OPT_BOUNDHOLE)
                    {
                        optData.Add(OPT_BOUNDHOLE, ParseDynamicBoundHoleFile(traceFile));
                    }
                    else if (optId == OPT_ADJACENTHOLE)
                    {
                        optData.Add(OPT_ADJACENTHOLE, ParseAdjacentHoleFile(traceFile));
                    }

                    return;
                }

                // others
                if (optId == OPT_BOUNDHOLE)
                {
                    optData.Add(OPT_BOUNDHOLE, ParseBoundHoleFile(traceFile));
                }
            }
        }

        #region parse main file for Analyzer and Visualizer

        public void ParseNeighbourFile(string neighbourFilePath)
        {
            //1. Read node trace file
            try
            {
                string currentLine = "";
                System.IO.StreamReader file = new System.IO.StreamReader(neighbourFilePath);
                while ((currentLine = file.ReadLine()) != null)
                {
                    //TODO: Change regex to String.Split here -for faster
                    if (string.IsNullOrEmpty(currentLine) == false)
                    {
                        string[] retval = Regex.Split(currentLine, "\\s+");
                        int id = System.Convert.ToInt32(retval[0]);
                        double x = System.Convert.ToDouble(retval[1]);
                        double y = System.Convert.ToDouble(retval[2]);
                        Model.NodeData node = new Model.NodeData(id, x, y);
                        nodes.Add(id, node);
                        try
                        {
                            node.OffTime = System.Convert.ToDouble(retval[3]);

                            // New event
                            EventData e = new EventData();
                            e.Id = simulateEvents.Count;
                            e.HappendTime = node.OffTime;
                            e.Node = node;
                            e.Type = EventData.EventType.SIM_NODEOFF;

                            simulateEvents.Add(e);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.StackTrace);
                        }
                    }
                }

                file.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ParseMainFile(string mainFilePath)
        {
            int eventIndex = 0;
            int i = 0;
            string mainCurrentLine = "";
            try
            {
                //2. Read event trace file
                System.IO.StreamReader mainFileStream = new System.IO.StreamReader(mainFilePath);
                string[] mainRetVal;

                while ((mainCurrentLine = mainFileStream.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(mainCurrentLine) == false)
                    {
                        i = i + 1;
                        //replace double space by one space

                        mainRetVal = mainCurrentLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        //string [] headers = mainCurrentLine.Split(new char[] { ']' }, StringSplitOptions.RemoveEmptyEntries);
                        switch (mainRetVal[0])
                        {
                            case "N": // Energy: N -t 0.147751 -n 3 -e 999.998548
                                if (mainRetVal.Length < 7)
                                {
                                    //continue;
                                    Console.WriteLine("Energy: Trace in line " + i.ToString() + " is not in correct format");
                                    Console.WriteLine(mainCurrentLine);
                                    break;
                                }
                                int nodeId = System.Convert.ToInt32(mainRetVal[4]);
                                double time = System.Convert.ToDouble(mainRetVal[2]);

                                // Lay mau voi tan so energyPatternRate
                                if ((nodes[nodeId].Energies.Count == 0) || (time - nodes[nodeId].Energies.Last().Time > energyPatternRate))
                                {
                                    double nodeEnergy = System.Convert.ToDouble(mainRetVal[6]);
                                    nodes[nodeId].Energies.Add(new NodeEnergy(time, nodeEnergy));
                                }

                                break;

                            case "s": // Send:      s 0.002259878 _237_ RTR  --- 0 HELLO 36 [0 0 0 0] ------- [237:255 -1:255 32 0]
                            case "r": //Receieve:   r 0.003336783 _475_ RTR  --- 1 HELLO 36 [0 ffffffff 4b 800] ------- [75:255 -1:255 32 0]
                            case "f": // forward:   f 34.925440740 _939_ RTR  --- 1112 GRID 76 [13a 3ab 3ac 800] ------- [86:255 938:255 65 938]
                            case "D": // drop:      D 34.936054784 _256_ RTR  REPEAT 1113 GRID 74 [13a 100 3c3 800] ------- [537:255 256:255 65 256]
                                //With energy:
                                //Send: s 0.002259878 _237_ RTR  --- 0 HELLO 36 [0 0 0 0] [energy 1000.000000 ei 0.000 es 0.000 et 0.000 er 0.000] ------- [237:255 -1:255 32 0]
                                //Forward: f 364.672019363 _879_ RTR  --- 1398 cbr 123 [13a 36f 2ce 800] [energy 996.434502 ei 3.484 es 0.000 et 0.003 er 0.079] ------- [527:0 -1:0 94 878]

                                // --> Truong hop nay lai ko co IP header o sau cung (ma dang dung goi tin ARP de hoi thong tin.
                                //D 31.118554157 _1177_ IFQ  --- 0 ARP 28 [0 ffffffff 499 806] [energy 999.683405 ei 0.285 es 0.000 et 0.006 er 0.026] ------- [REQUEST 1177/1177 0/1062]
                                // New packet
                                if (mainRetVal.Length < 17)
                                {
                                    throw new Exception("Event: Trace in line " + i.ToString() + " is not in correct format");
                                }

                                int packetId = System.Convert.ToInt32(mainRetVal[5]);
                                PacketData packet;
                                if (!packets.ContainsKey(packetId))
                                {
                                    packet = new PacketData(packetId);
                                    packet.PacketType = mainRetVal[6];
                                    if (!this.packetTypes.Contains(packet.PacketType))
                                    {
                                        packetTypes.Add(packet.PacketType);
                                    }
                                    packet.PacketSize = System.Convert.ToInt32(mainRetVal[7]);
                                    if (mainRetVal[0] == "s")
                                    {
                                        string sourceNodeId = mainRetVal[13];
                                        string destinationNodeId = mainRetVal[14];
                                        if (mainRetVal[12] == "[energy")
                                        {
                                            sourceNodeId = mainRetVal[23];
                                            destinationNodeId = mainRetVal[24];
                                        }

                                        sourceNodeId = sourceNodeId.Substring(1, sourceNodeId.IndexOf(":") - 1);
                                        packet.SourceNode = nodes[System.Convert.ToInt32(sourceNodeId)];
                                        destinationNodeId = destinationNodeId.Substring(0, destinationNodeId.IndexOf(":"));
                                        if (destinationNodeId == "-1")
                                        {
                                            packet.DestinationNode = null;
                                            packet.IsMultiCast = true;
                                        }
                                        else
                                        {
                                            packet.DestinationNode = nodes[System.Convert.ToInt32(destinationNodeId)];
                                            //packet.SourceNode = nodes[System.Convert.ToInt32(sourceNodeId)];
                                            packet.IsMultiCast = false;
                                        }
                                        packet.IsDrop = false;
                                    }
                                    packets.Add(packetId, packet);
                                }
                                else
                                {
                                    packet = packets[packetId];
                                }

                                nodeId = System.Convert.ToInt32(mainRetVal[2].Substring(1, mainRetVal[2].Length - 2));

                                // New event
                                EventData e = new EventData();
                                e.Id = eventIndex++;
                                e.HappendTime = System.Convert.ToDouble(mainRetVal[1]);
                                e.Node = nodes[nodeId];
                                e.Layer = mainRetVal[3];
                                e.Message = mainRetVal[4];
                                if (mainRetVal[0] == "s") e.Type = EventData.EventType.SEND;
                                if (mainRetVal[0] == "r")
                                {
                                    e.Type = EventData.EventType.RECEIVE;

                                    //Update previousNode
                                    string previousNodeId = mainRetVal[10];
                                    e.PreviousNode = nodes[System.Convert.ToInt32(previousNodeId, 16)];
                                }
                                if (mainRetVal[0] == "f") e.Type = EventData.EventType.FORWARD;
                                if (mainRetVal[0] == "D")
                                {
                                    e.Type = EventData.EventType.DROP;
                                    if (packet.DestinationNode != null)
                                    {
                                        //Ko phai BroadCast. Truong hop (Distribued, Unicast). Multicast chua xac dinh dat o dau
                                        if ((nodeId != packet.DestinationNode.ID) || (packet.SourceLayer == e.Layer))
                                        {
                                            //Note: Gia thiet la da co 1 event send danh cho packet dang xet nay.
                                            packet.IsDrop = true;
                                        }
                                    }
                                }
                                e.Packet = packet;
                                //Update header of packets.
                                //string[] headers = mainCurrentLine.Split(new char[] { '[',']' }, StringSplitOptions.RemoveEmptyEntries);
                                //e.AddHeader(headers[1]);
                                //e.AddHeader(headers[headers.Length - 2]);
                                events.Add(e);

                                // Update packet.NodeList; packet.EventList
                                packet.EventList.Add(e);
                                //Co the khong can den tham so nay...
                                if (!packet.NodeList.Contains(e.Node))
                                {
                                    packet.NodeList.Add(e.Node);
                                }
                                // Update node.nodeEvent
                                nodes[nodeId].NodeEvent.Add(e);

                                //TODO: Update node.Packet.
                                break;

                            case "M": // Move:
                                // do nothing
                                break;
                        }
                    }
                }
                mainFileStream.DiscardBufferedData();
                mainFileStream.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                Console.WriteLine(mainCurrentLine);
                throw ex;
            }
        }

        #endregion parse main file for Analyzer and Visualizer

        #region parse file for some specified case

        // parse file of circle that approximate hole
        private Dictionary<int, List<NodeData>> ParseEllipseHoleFile(string traceFile)
        {
            string currentLine = "";
            List<Model.NodeData> mBound = new List<Model.NodeData>();
            Dictionary<int, List<NodeData>> bounds = new Dictionary<int, List<NodeData>>();
            try
            {
                System.IO.StreamReader file = new System.IO.StreamReader(traceFile);
                while ((currentLine = file.ReadLine()) != null)
                {
                    //TODO: Change regex to String.Split here -for faster
                    if (string.IsNullOrEmpty(currentLine) == false)
                    {
                        string[] retval = Regex.Split(currentLine, "\\s+");
                        double x = System.Convert.ToDouble(retval[0]);
                        double y = System.Convert.ToDouble(retval[1]);
                        double range = System.Convert.ToDouble(retval[2]);
                        Model.NodeData node = new Model.NodeData(-1, x, y);
                        node.Range = range;
                        mBound.Add(node);
                        bounds.Add(bounds.Count, mBound);
                        mBound = new List<NodeData>();
                    }
                }

                file.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bounds;
        }

        /// <summary>
        /// parse normal boundhole file for most routing protocol base on BOUNDHOLE
        /// </summary>
        /// <param name="traceFile"></param>
        /// <returns></returns>
        private Dictionary<int, List<NodeData>> ParseBoundHoleFile(string traceFile)
        {
            string currentLine = "";
            Dictionary<int, List<NodeData>> bounds = new Dictionary<int, List<NodeData>>();
            List<Model.NodeData> mBound = new List<Model.NodeData>();
            try
            {
                System.IO.StreamReader file = new System.IO.StreamReader(traceFile);
                while ((currentLine = file.ReadLine()) != null)
                {
                    //TODO: Change regex to String.Split here -for faster
                    if (string.IsNullOrEmpty(currentLine) == false)
                    {
                        string[] retval = Regex.Split(currentLine, "\\s+");
                        int id = System.Convert.ToInt32(retval[0]);
                        double x = System.Convert.ToDouble(retval[1]);
                        double y = System.Convert.ToDouble(retval[2]);
                        Model.NodeData node = new Model.NodeData(id, x, y);
                        mBound.Add(node);
                    }
                    else
                    {
                        bounds.Add(bounds.Count, mBound);
                        mBound = new List<NodeData>();
                    }
                }

                if (mBound.Count != 0) bounds.Add(bounds.Count, mBound);

                file.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bounds;
        }

        /// <summary>
        /// parse dynamic grid that approximate hole file (for one hole only)
        /// </summary>
        /// <param name="traceFile"></param>
        /// <returns></returns>
        private Dictionary<double, List<NodeData>> ParseDynamicBoundHoleFile(string traceFile)
        {
            Dictionary<double, List<NodeData>> bounds = new Dictionary<double, List<NodeData>>();
            string currentLine = "";
            bool isFirstLine = true;
            double time = 0;
            int nodeId = 0;
            List<Model.NodeData> mBound = new List<Model.NodeData>();

            try
            {
                System.IO.StreamReader file = new System.IO.StreamReader(traceFile);
                while ((currentLine = file.ReadLine()) != null)
                {
                    //TODO: Change regex to String.Split here -for faster
                    if (string.IsNullOrEmpty(currentLine) == false)
                    {
                        string[] retval = Regex.Split(currentLine, "\\s+");

                        if (isFirstLine)
                        {
                            isFirstLine = false;
                            nodeId = System.Convert.ToInt32(retval[0]);
                            time = System.Convert.ToDouble(retval[1]);
                        }
                        else
                        {
                            double x = System.Convert.ToDouble(retval[0]);
                            double y = System.Convert.ToDouble(retval[1]);
                            Model.NodeData node = new Model.NodeData(-1, x, y);
                            mBound.Add(node);
                        }
                    }
                    else
                    {
                        bounds.Add(time, mBound);
                        mBound = new List<Model.NodeData>();
                        isFirstLine = true;

                        // New event
                        EventData e = new EventData();
                        e.Id = simulateEvents.Count;
                        e.HappendTime = time;
                        NodeData n = new NodeData(nodeId, -1, -1);
                        e.Node = n; // need update location after
                        e.Type = EventData.EventType.SIM_HOLE;

                        simulateEvents.Add(e);
                    }
                }

                if (mBound.Count > 0)
                {
                    bounds.Add(time, mBound);

                    // New event
                    EventData e = new EventData();
                    e.Id = simulateEvents.Count;
                    e.HappendTime = time;
                    NodeData n = new NodeData(nodeId, -1, -1);
                    e.Node = n; // need update location after
                    e.Type = EventData.EventType.SIM_HOLE;

                    simulateEvents.Add(e);
                }

                file.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bounds;
        }

        /// <summary>
        /// Parse adjacent hole file from grid dynamic routing protocol
        /// </summary>
        /// <param name="traceFile"></param>
        private Dictionary<double, int> ParseAdjacentHoleFile(string traceFile)
        {
            string currentLine = "";
            Dictionary<double, int> neighborNodes = new Dictionary<double, int>();
            try
            {
                System.IO.StreamReader file = new System.IO.StreamReader(traceFile);
                while ((currentLine = file.ReadLine()) != null)
                {
                    //TODO: Change regex to String.Split here -for faster\
                    string[] retval = Regex.Split(currentLine, "\\s+");

                    int id = System.Convert.ToInt32(retval[0]);
                    double time = System.Convert.ToDouble(retval[1]);
                    neighborNodes.Add(time, id);

                    // New event
                    EventData e = new EventData();
                    e.Id = simulateEvents.Count;
                    e.HappendTime = time;
                    NodeData n = new NodeData(id, -1, -1);
                    e.Node = n; // need update location after
                    e.Type = EventData.EventType.SIM_ADJACENTHOLE;

                    simulateEvents.Add(e);
                }

                file.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return neighborNodes;
        }

        /// <summary>
        /// Parse parallelogram file from elbar routing protocol
        /// </summary>
        private Dictionary<int, Dictionary<int, List<NodeData>>> ParseElbarParallelogramFile(string traceFile)
        {
            Dictionary<int, Dictionary<int, List<NodeData>>> parallelograms = new Dictionary<int, Dictionary<int, List<NodeData>>>();
            try
            {
                string currentLine = "";
                int countLine = 0;
                List<NodeData> p = new List<NodeData>();
                int packetId = -1;
                int nodeId = -1;

                System.IO.StreamReader file = new System.IO.StreamReader(traceFile);
                while ((currentLine = file.ReadLine()) != null)
                {
                    //TODO: Change regex to String.Split here -for faster
                    if (string.IsNullOrEmpty(currentLine) == false)
                    {
                        string[] retval = Regex.Split(currentLine, "\\s+");

                        if (countLine == 0)
                        {
                            nodeId = System.Convert.ToInt32(retval[0]);
                            packetId = System.Convert.ToInt32(retval[1]);

                            p = new List<NodeData>();
                            countLine++;
                        }
                        else
                        {
                            double x = System.Convert.ToDouble(retval[0]);
                            double y = System.Convert.ToDouble(retval[1]);
                            Model.NodeData node = new Model.NodeData(-1, x, y);
                            p.Add(node);

                            countLine++;
                            if (countLine == 5)
                            {
                                countLine = 0;
                                if (!parallelograms.ContainsKey(packetId))
                                {
                                    Dictionary<int, List<NodeData>> data = new Dictionary<int, List<NodeData>>();
                                    data.Add(nodeId, p);
                                    parallelograms.Add(packetId, data);
                                }
                                else
                                {
                                    parallelograms[packetId].Add(nodeId, p);
                                }
                            }
                        }
                    }
                }

                file.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return parallelograms;
        }

        #endregion parse file for some specified case

        public void Reset()
        {
            nodes.Clear();
            packets.Clear();
            events.Clear();
        }
    }
}