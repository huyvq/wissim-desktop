﻿using System;
using System.Linq;
using WisSim.NetworkComponents;

namespace WisSim.Controller.GraphAlgorithms
{
    /// <summary>
    /// DFS algorithm
    /// <author>Trong Nguyen</author>
    /// </summary>
    public class Dfs
    {
        private Graph Graph { get; set; }
        private TreeNode Tree { get; set; }

        public Dfs(INetwork input)
        {
            Graph = new Graph(input);
        }

        public TreeNode Process(INode node)
        {
            // reset label
            Graph.ResetLabel();

            // initialize result tree object with Node map to root
            Tree = new TreeNode(node);

            // create result tree
            Proc(Tree);

            // return result
            return Tree;
        }

        private void Proc(TreeNode roodNode)
	    {
		    // mark node1 as explored		
		    //graph.Label.Add(roodNode.INode, true);
            Graph.Label[roodNode.INode] = true;
		
		    // look for each NeighborNode of startNode
		    //for (INode INode : roodNode.INode.getNeighborList())
            foreach (var iNode in Graph.Adj[roodNode.INode])
            {
			    if (Graph.Label[iNode] == false)
			    {
				    // create tree node
				    TreeNode treeNode = new TreeNode(iNode);
				
				    // Add Node to roodNode's child list
				    roodNode.ChildList.Add(treeNode);
				
				    // Process with child node
				    Proc(treeNode);
			    }
		    }
	    }
	
    }
}
