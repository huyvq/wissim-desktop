﻿using System;
using System.Collections.Generic;
using System.Linq;
using WisSim.NetworkComponents;

namespace WisSim.Controller.GraphAlgorithms
{
    /// <summary>
    /// Tree Node
    /// <author>Trong Nguyen</author>
    /// </summary>
    public class TreeNode
    {
        public INode INode;	// wireless Node map to this node in the tree
        public List<TreeNode> ChildList;
        public int Cost;

        public TreeNode(INode rootNode)
        {
            this.INode = rootNode;
            this.ChildList = new List<TreeNode>();
            ChildList.Clear();
            Cost = 0;
        }
    }
}
