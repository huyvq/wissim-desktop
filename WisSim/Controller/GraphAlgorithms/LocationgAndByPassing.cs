﻿using System;
using System.Collections.Generic;
using System.Linq;
using WisSim.NetworkComponents;

namespace WisSim.Controller.GraphAlgorithms
{
    /// <summary>
    /// BoundHole algorithm
    /// <author>Trong Nguyen</author>    
    /// </summary>
    public class LocationgAndByPassing
    {
        private readonly Graph graph;

        public LocationgAndByPassing(INetwork network)
        {            
            this.graph = new Graph(network);
        }

        public List<INode> WeakStuckNode    
        {
            get
            {
                List<INode> result = new List<INode>();
                foreach (INode item in graph.Network.NodeList)
                    if (CheckWeakStuckNode(item))
                        result.Add(item);
                return result;
            }
        }
        private bool CheckWeakStuckNode(INode node) 
        {
            // node called "weak stuck node" if have a node outside node's transmission so none of the 1 hop neighbor of node closer than node
            foreach (INode item in graph.Network.NodeList)
            {
                double d0 = D(node, item);
                if (d0 > node.Range)
                {
                    bool ok = true;
                    foreach (var neight in node.NeighborList)
                        if (d0 >= D(neight, item))
                        {
                            ok = false;
                            break;
                        }
                    if (ok) return true;
                }
            }
            return false;
        }

        public List<INode> StrongStuckNode  
        {
            get
            {
                List<INode> result = new List<INode>();

                foreach (var item in graph.Network.NodeList)
                {
                    if (CheckStrongStuckNode((INode)item))
                        result.Add((INode)item);
                }
                return result;
            }
        }
        private bool CheckStrongStuckNode(INode node)
        {            
            if (node.NeighborList.Count < 3) return true;
            INode[] neighbor = node.NeighborList.ToArray();

            // TODO: Luu phep tinh goc de giam lan tinh toan
            for (int i = 0    ; i < neighbor.Count(); i++)
                for (int j = i + 1; j < neighbor.Count(); j++)
                {
                    double? angle1 = Angle(node, neighbor[i]);
                    double? angle2 = Angle(node, neighbor[j]);
                    if (angle1 > angle2)
                    {
                        INode a = neighbor[i];
                        neighbor[i] = neighbor[j];
                        neighbor[j] = a;
                    }
                }

            // check angle > pi
            for (int i = 0; i < neighbor.Count(); i++)
            {
                if (CheckAngle(neighbor[i], node, neighbor[(i + 1) % neighbor.Count()]))
                    return true;
            }

            // Áp dụng quy tắc TEND trên dãy sắp xếp neigbhor            
            for (int i = 0; i < neighbor.Count(); i++)            
            {
                int j = (i + 1) % neighbor.Count();
                
                // check with neighbor[i] and neighbor[j]
                double a1 = 2 * (neighbor[i].X - node.X);
                double b1 = 2 * (neighbor[i].Y - node.Y);
                double c1 = neighbor[i].X * neighbor[i].X + neighbor[i].Y * neighbor[i].Y - node.X * node.X - node.Y * node.Y;
                
                double a2 = 2 * (neighbor[j].X - node.X);
                double b2 = 2 * (neighbor[j].Y - node.Y);
                double c2 = neighbor[j].X * neighbor[j].X + neighbor[j].Y * neighbor[j].Y - node.X * node.X - node.Y * node.Y;
               
                double dd = a1 * b2 - a2 * b1;
                if (dd != 0)
                {
                    double dx = c1 * b2 - c2 * b1;
                    double dy = a1 * c2 - a2 * c1;
                    double x = dx / dd;
                    double y = dy / dd;
                    double? angle0 = Angle1(node.X, node.Y, x, y);
                    
                    // TODO: Luu phep tinh goc de giam lan tinh toan

                    if (Angle(node, neighbor[j]) > angle0 && angle0 > Angle(node, neighbor[i]))
                        if (Math.Sqrt((x - node.X) * (x - node.X) + (y - node.Y) * (y - node.Y)) > node.Range)
                            return true;
                }
            }            
            return false;
        }
        private bool CheckAngle(INode n1, INode n2, INode n3)
        {
            // check if agle n2n1 n2n3 ccw is bigger than pi            
            double? angle1 = Angle(n2, n1);
            double? angle2 = Angle(n2, n3);
            return (angle2 - angle1 >= 0 ? angle2 - angle1 : angle2 - angle1 + Math.PI * 2) >= Math.PI;
        }              
        
        /// <summary>
        /// Tinh goc hop boi vec to n2n1 va vector ox. donvi radian
        /// </summary>
        /// <param name="n2"></param>
        /// <param name="n1"></param>
        /// <returns></returns>        
        private double? Angle(INode n1, INode n2)           
        {
            // tinh go tao boi vec to ox va n2n1 theo chieu kim dong ho
            if (n1 == null || n2 == null) return null;
            return Angle1(n1.X, n1.Y, n2.X, n2.Y);
        }
        private double? Angle1(double x1, double y1, double x2, double y2)
        {
            // tinh go tao boi vec to ox va n2n1 theo chieu kim dong ho
            double x = x2 - x1;
            double y = y2 - y1;

            if (x == 0 && y == 0) return null;

            if (x == 0) return y > 0 ? Math.PI / 2 : Math.PI * 3 / 2;
            double angle = Math.Atan2(Math.Abs(y), Math.Abs(x));
            if (x > 0)
            {
                // y>= 0 do nothing
                if (y < 0) angle = 2 * Math.PI - angle;
            }
            else
            {
                if (y >= 0) angle = Math.PI - angle;
                else angle = Math.PI + angle;
            }

            return angle;
        }


        public List<List<INode>> BoundHoleGreedy()     
        {
            List<List<INode>> hole = new List<List<INode>>();                        
            graph.ResetLabel();
            foreach (INode item in graph.Network.NodeList)
            {
                if (!graph.Label[item])
                {
                    List<INode> result = FindBoud(item);
                    if (result != null)
                    {
                        hole.Add(result);
                    }
                    else System.Console.WriteLine("     not Stuck node");
                }
            }
            return hole;
        }
        private List<INode> FindBoud(INode node)
        {
            // check if node has less than 2 neighbor
            if (node.NeighborList.Count < 1)
            {
                List<INode> result = new List<INode>();
                result.Add(node);
                return result;
            }
            if (node.NeighborList.Count < 2)
                return Find(node, node.NeighborList[0]);
            
            INode[] neighbor = node.NeighborList.ToArray();

            // TODO: Luu phep tinh goc de giam lan tinh toan
            for (int i = 0; i < neighbor.Count(); i++)
                for (int j = i + 1; j < neighbor.Count(); j++)
                {
                    double? anglei = Angle(node, neighbor[i]);
                    double? anglej = Angle(node, neighbor[j]);
                    if (anglei > anglej || ((anglei == anglej) && (D(node, neighbor[i]) > D(node, neighbor[j]))))
                    {
                        var a = neighbor[i];
                        neighbor[i] = neighbor[j];
                        neighbor[j] = a;
                    }
                }

            // check angle > pi
            for (int i = 0; i < neighbor.Count(); i++)
            {
                if (CheckAngle(neighbor[i], node, neighbor[(i + 1) % neighbor.Count()]))
                {
                    System.Console.WriteLine("     angle bigger than Pi");
                    return Find(neighbor[i], node, neighbor[(i + 1) % neighbor.Count()]);
                }
            
            }

            // Áp dụng quy tắc TEND trên dãy sắp xếp neigbhor            
            for (int i = 0; i < neighbor.Count(); i++)
            {
                int j = (i + 1) % neighbor.Count();

                // check with neighbor[i] and neighbor[j]
                double a1 = 2 * (neighbor[i].X - node.X);
                double b1 = 2 * (neighbor[i].Y - node.Y);
                double c1 = neighbor[i].X * neighbor[i].X + neighbor[i].Y * neighbor[i].Y - node.X * node.X - node.Y * node.Y;

                double a2 = 2 * (neighbor[j].X - node.X);
                double b2 = 2 * (neighbor[j].Y - node.Y);
                double c2 = neighbor[j].X * neighbor[j].X + neighbor[j].Y * neighbor[j].Y - node.X * node.X - node.Y * node.Y;

                double dd = a1 * b2 - a2 * b1;
                if (dd != 0)
                {
                    double dx = c1 * b2 - c2 * b1;
                    double dy = a1 * c2 - a2 * c1;
                    double x = dx / dd;
                    double y = dy / dd;
                    double? angle0 = Angle1(node.X, node.Y, x, y);

                    // TODO: Luu phep tinh goc de giam lan tinh toan

                    if (Angle(node, neighbor[j]) > angle0 && angle0 > Angle(node, neighbor[i]))
                        if (Math.Sqrt((x - node.X) * (x - node.X) + (y - node.Y) * (y - node.Y)) > node.Range)
                        {
                            System.Console.WriteLine("     Tent rule");
                            return Find(neighbor[i], node, neighbor[j]);
                        }
                }
            }
            return null;
        }
        private List<INode> Find(INode p, INode t1)
        {
            return Find(null, p, t1);
        }
        private List<INode> Find(INode s, INode p, INode t1)  
        {
            List<INode> result = new List<INode>();
            result.Add(p);
            result.Add(t1);                        

            INode t2 = RightHand(s, p, t1);
            if (t2 == null)
            {
                goto end;
            }
            else s = p;
            while (t2 != p)
            {
                // Kiem tra t1 t2 co cat canh nao da cotrong result hay ko                
                int? ti = Intersection(t1, t2, result);
                if (ti != null)
                {
                    int i = (int)ti;
                    if (!result[i].NeighborList.Contains(t1) && !result[i + 1].NeighborList.Contains(t1))
                    {
                        // loai 1
                        result.RemoveRange(i + 1, result.Count() - i - 1);
                        result.Add(t2);
                        result.Add(t1);

                        t2 = t1;
                        t1 = result[result.Count - 2];
                        s = result[result.Count - 3];
                        continue;
                    }
                    if (!t1.NeighborList.Contains(result[i]) && !t2.NeighborList.Contains(result[i + 1]))
                    {
                        // loai 2
                        t2 = result[i + 1];
                    }
                }
                result.Add(t2);

                INode temp = RightHand(s, t1, t2);
                if (temp == null) goto end;
                else
                {
                    s = t1;
                    t1 = t2;
                    t2 = temp;
                }
            }

        end:
            result.Add(p);

            foreach (var item in result)
                graph.Label[item] = true;
                       
            return result;
        }
        private INode RightHand(INode ss, INode pp, INode tt1)
        {
            INode[] neighbor = tt1.NeighborList.ToArray();

            // TODO: Luu phep tinh goc de giam lan tinh toan
            for (int i = 0; i < neighbor.Count(); i++)
            for (int j = i + 1; j < neighbor.Count(); j++)
            {
                double? anglei = Angle(tt1, neighbor[i]);
                double? anglej = Angle(tt1, neighbor[j]);
                if ((anglei > anglej) || ((anglei == anglej) && (D(tt1, neighbor[i]) > D(tt1, neighbor[j]))))
                {
                    INode a = neighbor[i];
                    neighbor[i] = neighbor[j];
                    neighbor[j] = a;
                }
            }
            double? angle0 = Angle(tt1, pp);
            double? angle1 = Angle(pp, ss);          
            if (angle1 != null)
            {
                if (Angle(pp, tt1) > angle1)
                {
                    if (Angle(pp, tt1) - angle1 >= Math.PI) angle1 = null;
                }
                else
                {
                    if (Angle(pp, tt1) + 2 * Math.PI - angle1 >= Math.PI) angle1 = null;
                }
            }

            int k;
            for (k = 0; k < neighbor.Count(); k++)
                if (Angle(tt1, neighbor[k]) > angle0)
                {
                    if (angle1 == null) break;
                    else if (Angle(pp, neighbor[k]) > angle1) break; 
                }
            k = k % neighbor.Count();

            return neighbor[k];
        }

        private int? Intersection(INode tj, INode tj1, List<INode> t)
        {
            for (int i = 0; i < t.Count - 2; i++)
            {
                if (Intersection(tj, tj1, t[i], t[i + 1])) return i;
            }
            return null;
        }
        private bool Intersection(INode t1, INode t2, INode t3, INode t4)
        {
            double a1 = t1.Y - t2.Y;
            double b1 = t2.X - t1.X;
            double c1 = -t1.Y * t2.X + t2.Y * t1.X;
            
            double a2 = t3.Y - t4.Y;
            double b2 = t4.X - t3.X;
            double c2 = -t3.Y * t4.X + t4.Y * t3.X;

            if (a1 == 0 && b1 == 0) return false;
            if (a2 == 0 && b2 == 0) return false;

            double x, y;
            if (a1 == 0 && b2 == 0)
            {
                x = -c2 / a2;
                y = -c1 / b1;
            }
            if (a2 == 0 && b1 == 0)
            {
                x = -c1 / a1;
                y = -c2 / b2;
            }

            if (a1 * b2 != a2 * b1)
            {
                x = (b1 * c2 - b2 * c1) / (a1 * b2 - a2 * b1);
                y = (c1 * a2 - c2 * a1) / (a1 * b2 - a2 * b1);

                return (t1.X - x) * (t2.X - x) < 0 && (t3.X - x) * (t4.X - 4) < 0 && (t1.Y - y) * (t2.Y - y) < 0 && (t3.Y - y) * (t4.Y - y) < 0;
            }
            return false;
        }

        private double D(INode n, INode m)
        // calculate distance of N and M
        {
            return Math.Sqrt((n.X - m.X) * (n.X - m.X) + (n.Y - m.Y) * (n.Y - m.Y));
        }
    }
}
