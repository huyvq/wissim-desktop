﻿using System;
using System.Collections.Generic;
using System.Linq;
using WisSim.NetworkComponents;

namespace WisSim.Controller.GraphAlgorithms
{
    /// <summary>
    /// BFS algorithm
    /// <author>Trong Nguyen</author>
    /// </summary>
    public class Bfs
    {
        private readonly Graph graph;

        //private TreeNode Tree;
        //private Queue<TreeNode> Queue;

        public Bfs(INetwork input)
        {
            this.graph = new Graph(input);
        }

        public List<TreeNode> FinAllTree()
	    {
		    List<TreeNode> result = new List<TreeNode>();
		    graph.ResetLabel();
		
		    foreach (var node in graph.Network.NodeList)
		    {            
			    if (graph.Label[(INode)node] == false)
				    result.Add(Proc((INode) node));
		    }
		
		    return result;
	    }

        public TreeNode Process(INode node)
        // return Tree with root = node
        {
            // Initialize Graph label		
            graph.ResetLabel();

            return Proc(node);
        }

        private TreeNode Proc(INode node)
	    {
		    // Initialize result tree. map Node to tree root
		    TreeNode tree = new TreeNode(node);
		
		    // mark Node as explored		    
            graph.Label[tree.INode] = true;
		
		    // initialize Queue
		    Queue<TreeNode> queue = new Queue<TreeNode>();
            queue.Enqueue(tree);		    
		
		    // create result tree
	        while (queue.Count > 0)		    
		    {
                TreeNode rootNode = queue.Dequeue();			    
			    foreach (var iNode in graph.Adj[rootNode.INode]) 
			    {
				    // INode isn't explored
				    if (graph.Label[iNode] == false)
				    {
					    // mark INode as explored					    
                        graph.Label[iNode] = true;
																			
					    // create tree node
					    TreeNode treeNode = new TreeNode(iNode);
					
					    // add treeNode to rootNode's child list
					    rootNode.ChildList.Add(treeNode);
					
					    // add treeNode to Queue
					    queue.Enqueue(treeNode);
				    }
			    }		
		    }
		
		    return tree;
	    }

        public Boolean CheckConnectable(INode u, INode v)
	    // return true if U and V is connectable 
	    {
		    // Initialize Graph label		
		    graph.ResetLabel();
		
		    // Initialize result tree. map Node to tree root
		    TreeNode tree = new TreeNode(u);
		
		    // mark Node as explored		   
            graph.Label[tree.INode] = true;
		
		    // initialize Queue
		    Queue<TreeNode> queue = new Queue<TreeNode>();
		    queue.Enqueue(tree);
		
		    // find path to V
		    while (queue.Count > 0)
		    {
                var rootNode = queue.Dequeue();
                foreach (var iNode in graph.Adj[rootNode.INode]) 
			    {
				    // check V
				    if (iNode == v) return true;
				
				    // INode isn't explored
				    if (graph.Label[iNode] == false)
				    {
					    // mark INode as explored
                        graph.Label[iNode] = true;					    
																			
					    // create tree node
					    TreeNode treeNode = new TreeNode(iNode);
					
					    // add treeNode to rootNode's child list
					    rootNode.ChildList.Add(treeNode);
					
					    // add treeNode to Queue
					    queue.Enqueue(treeNode);
				    }
			    }		
		    }
		
		    return false;
	    }

    }
}
