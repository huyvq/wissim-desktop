﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WisSim.NetworkComponents;

namespace WisSim.Controller.GraphAlgorithms
{
    public class Dijkstra
    {
        private Graph graph;
        private IList list;
        private readonly INetwork input;        

        public Dijkstra(INetwork input)
        {
            this.input = input;
        }

        //public TreeNode Dijkstra_Process(INetwork Input, INode SourceNode){
        public TreeNode Process(INode sourceNode)
        {
            // initialize Graph label
            this.graph = new Graph(input);
            graph.ResetLabel();
            graph.ResetCost();

            // set rootNode cost = 0
            graph.Cost.Add(sourceNode, 0.0);

            // initialize List
            //List = Graph.Network.getNodeList();
            list = new List<INode>();
            list.Add(sourceNode);

            // create result tree
            Proc();

            list = graph.Network.NodeList;
            TreeNode tree = new TreeNode(sourceNode);
            MakeTree(tree);

            return tree;
        }

        public List<INode> Process(INode sourceNode, INode tagerNode)
        // return path from tagerNode to sourceNode
        // switch sourceNode and tagerNode to create path
        {
            // initialize Graph label		
            graph = new Graph(input);
            graph.ResetLabel();
            graph.ResetCost();

            // set rootNode cost = 0
            graph.Cost.Add(tagerNode, 0.0);

            // initialize List
            //List = Graph.Network.getNodeList();
            list = new List<INode>();
            list.Add(tagerNode);

            // create result tree
            Proc();

            // create part
            List<INode> result = new List<INode>();
            INode node = sourceNode;
            while (node != tagerNode)
            {
                result.Add(node);
                if (!graph.Prev.ContainsKey(node))
                {
                    return null;
                }
                else
                {
                    node = graph.Prev[node];
                }
            }
            result.Add(tagerNode);

            return result;
        }

        private void Proc()
	    {
            while(list.Count > 0)		    
		    {
			    // find Node with minimum distance from source node
			    double min = Double.MaxValue;
			    INode minNode = null;
			
                foreach (var item in list)
                {
				    if (graph.Cost[(INode)item] < min)
				    {
					    min = graph.Cost[(INode)item];
					    minNode = (INode) item;
				    }				
			    }						
			
			    // remove minNode
			    list.Remove(minNode);
			
			    // recalculate adj node of minNode
			    min += 1;	// distance = 1 with adj node
                foreach (var item in graph.Adj[minNode])
			    {		
	                if (!graph.Cost.ContainsKey(item)) 
                    {
                        graph.Cost.Add(item,min);
                        graph.Prev.Add(item,minNode);
                        if (!list.Contains(item)) list.Add(item);
                    }
				    if (graph.Cost[item] > min)
				    {
					    graph.Cost[item] = min;
                        graph.Prev[item] = minNode;
					    if (!list.Contains(item)) list.Add(item);                        
				    }
			    }
		    }
	    }

        private void MakeTree(TreeNode node)
	    {
            foreach (var item in list)
		    {
			    if (graph.Prev.ContainsKey((INode)item) && graph.Prev[(INode)item] == node.INode)
			    {
				    TreeNode newNode = new TreeNode((INode)item);
				    node.ChildList.Add(newNode);
				    //List.remove(wNode);
				    MakeTree(newNode);
			    }
		    }
	    }
    }
}
