﻿using System;
using System.Collections.Generic;
using System.Linq;
using WisSim.NetworkComponents;

namespace WisSim.Controller.GraphAlgorithms
{
    /// <summary>
    /// <author>Trong Nguyen</author>    
    /// </summary>
    public class Graph
    {
        public INetwork Network;

        // Label for each Node in graph
        public Dictionary<INode, Boolean> Label;
        public Dictionary<INode, Double> Cost;
        public Dictionary<INode, INode> Prev;        
        public Dictionary<INode, List<INode>> Adj;        

        public Graph(INetwork network)
	    // create graph from wireless Network
	    {
		    this.Network = network;
		
		    // create label for each node, default value = false;
		    Label = new Dictionary<INode, Boolean>();
            foreach (INode node in network.NodeList)      
            {
                Label[node] = false;
            }
		
		    Cost = new Dictionary<INode, Double>();		
		    Prev = new Dictionary<INode, INode>();
		
		    // create adjacency matrix
            Adj = new Dictionary<INode, List<INode>>();
            foreach (INode item in network.NodeList)            
                Adj.Add(item, new List<INode>());
            		
		    for (int i = 0; i < network.NodeList.Count; i++)
			    for (int j = 0; j < network.NodeList.Count; j++)
			    {
				    // check if node[i] is the neighbor of node[j]
                    if (((INode)network.NodeList[i]).NeighborList.Contains(network.NodeList[j]))
                        // create edge i to j
                        Adj[((INode)network.NodeList[i])].Add((INode)network.NodeList[j]);	
			    }
	    }

        public void ResetPre()
        {
            this.Prev.Clear();
        }

        public void ResetLabel()
	    // reset all Label to false
	    {
		    Label.Clear();

            foreach (INode node in Network.NodeList)
            {
                Label[node] = false;
            }
            //Label.Add((INode)node, false);
	    }

        public void ResetCost()
        // set all label's cost to max
        {
            if (Cost != null)
                Cost.Clear();
        }

        public void Reduce(INode u, INode v)
        // eliminate edge (U, v)
        {
            // TODO Auto-generated method stub
            Adj[u].Remove(v);
            Adj[v].Remove(u);            
        }
    }
}
