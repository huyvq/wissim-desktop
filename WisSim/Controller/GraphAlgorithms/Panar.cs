﻿using System;
using System.Linq;
using WisSim.NetworkComponents;

namespace WisSim.Controller.GraphAlgorithms
{
    /// <summary>
    /// Pannar graph
    /// <author>Trong Nguyen</author>    
    /// </summary>
    public class Panar
    {
        private Graph rngGraph = null;
        private Graph ggGraph = null;

        private readonly INetwork input;

        public Panar(INetwork input)
        {
            this.input = input;
        }

        public Graph Rng()
	    {
            if (rngGraph == null)
            {
			    // create new original graph 
			    rngGraph = new Graph(input);
			
			    // reduce graph
                foreach (INode u in this.input.NodeList)	
                {
                    foreach (var v in this.rngGraph.Adj[u])
                    {
                        foreach (var w in this.rngGraph.Adj[u])
                        {
                            if (w == v) continue;

                            // combine d(U, V) with d(U, W); d(V, W) 
                            double dUV = D(u, v);
                            if (dUV > D(u, w) && dUV > D(v, w))
                            {
                                //eliminate edge(u;v)
                                rngGraph.Reduce(u, v);
                                goto br;
                            }
                        }                        
                    }
                br: ;// DO NOTHING                    
                }
            }
            return rngGraph;            
	    }

        public Graph GG()
	    {
		    if (ggGraph == null)
		    {
			    // create new original graph
			    ggGraph = new Graph(input);
			
			    // reducre graph
                foreach (INode u in input.NodeList)
			    {
                    foreach (var v in ggGraph.Adj[u])
				    {
                        foreach (var w in ggGraph.Adj[u])	
					    {
						    if (w == v) continue;
						
						    // combine d(U, V) with d(U, W) + d(V, W)
						    if (D(u, v) > D(u, w) + D(v, w))
						    {
							    //eliminate edge(u;v)
							    ggGraph.Reduce(u, v);
                                goto br;
						    }
					    }
				    }
                br: ;
			    }
		    }
		
		    return ggGraph;
	    }

        /// <summary>
        /// calculate squared distance of N and M
        /// </summary>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        private double D(INode n, INode m)
        {
            return (n.X - m.X) * (n.X - m.X) + (n.Y - m.Y) * (n.Y - m.Y);
        }
    }
}
