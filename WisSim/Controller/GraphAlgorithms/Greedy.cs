﻿using System;
using System.Collections.Generic;
using System.Linq;
using WisSim.NetworkComponents;

namespace WisSim.Controller.GraphAlgorithms
{
    /// <summary>
    /// Greedy algorithm
    /// <author>Trong Nguyen</author>    
    /// </summary>
    public class Greedy
    {
        private Graph Graph { get; set; }
        private List<INode> list;
        private INode tagerNode;

        public Greedy(INetwork input)
        {
            this.Graph = new Graph(input);
        }

        public List<INode> Process(INode sourceNode, INode tagerNode)
        {
            this.tagerNode = tagerNode;

            // initialize Graph
            Graph.ResetLabel();
            Graph.ResetCost();

            // calculate all node cost = distance to TagerNode
            CalcNodeCost(tagerNode);

            list = new List<INode>();
            list.Add(sourceNode);

            Process(sourceNode);

            return list;
        }

        private void CalcNodeCost(INode tagerNode)
	    {
            foreach (var item in Graph.Network.NodeList)	
		    {
                if (Graph.Cost.ContainsKey((INode)item))
                    Graph.Cost[(INode)item] = Math.Sqrt(
                        (tagerNode.X - ((INode)item).X) * (tagerNode.X - ((INode)item).X) +
                        (tagerNode.Y - ((INode)item).Y) * (tagerNode.Y - ((INode)item).Y)
                    );
                else
                    Graph.Cost.Add(
                        (INode)item,
                        Math.Sqrt(
                            (tagerNode.X - ((INode)item).X) * (tagerNode.X - ((INode)item).X) +
                            (tagerNode.Y - ((INode)item).Y) * (tagerNode.Y - ((INode)item).Y)
                        )
                    );        
		    }
	    }

        private void Process(INode node)
	    {	
		    // mark as explored
            Graph.Label[node] = true;		    
		
		    // find adj node of Node with minimum cost
		    double min = Graph.Cost[node];
		    INode minNode = null;
		
            foreach (var item in Graph.Adj[node])
		    {			
			    if (!Graph.Label[item] && Graph.Cost[item] < min)
			    {
				    min = Graph.Cost[item];
				    minNode = item;
			    }
		    }
		
		    // add minNode to result list
            if (minNode != null)
            {
                list.Add(minNode);
                if (minNode != tagerNode)
                {
                    Process(minNode);
                }
            }
	    }
    }
}
