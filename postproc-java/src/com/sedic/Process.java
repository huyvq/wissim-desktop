package com.sedic;

import com.sedic.model.NodeData;
import com.sedic.model.PacketData;
import com.sun.corba.se.impl.orbutil.graph.Node;
import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by huyvq on 2/7/2016.
 */
public class Process {
    public static double calc_average_energy(Map<Integer, NodeData> energy) {
        double sum = 0;
        for (NodeData node : energy.values()) {
            sum += (node.getRemainingEnergy());
        }
        return sum / energy.size();
    }

    public static double calc_standard_deviation_energy(Map<Integer, NodeData> energy) {
        double sum = 0;
        double sum_of_mean = 0;

        for (NodeData node : energy.values()) {
            sum += (node.getEnergyConsumed());
        }

        double mean = sum / energy.size();

        for (NodeData node : energy.values()) {
            sum_of_mean += (node.getEnergyConsumed() - mean) * (node.getEnergyConsumed() - mean);
        }

        return Math.sqrt(sum_of_mean / energy.size());
    }

    public static double calc_avg_hop_count(Map<Integer, PacketData> packets) {
        double sum = 0;
        long cbr_number = 0;
        for (PacketData p : packets.values()) {
            if (!p.isDrop() && p.getPacketType().equalsIgnoreCase("CBR")) {
                sum += p.HopCount();
                cbr_number++;
            }
        }
        return sum / cbr_number;
    }

    public static double get_min_lifetime(Map<Integer, NodeData> energy) {
        double min = energy.get(0).getOfftime();
        for (NodeData node : energy.values()) {
            if (min > node.getOfftime()) {
                min = node.getOfftime();
            }
        }
        return min;
    }

    public static double calc_standard_deviation_packet_count(Map<Integer, NodeData> nodes) {
        double sum = 0;
        double sum_of_mean = 0;

        for (NodeData node : nodes.values()) {
            sum += (node.nonDropCBRPacketCount());
        }
        double mean = sum / nodes.size();

        for (NodeData node : nodes.values()) {
            sum_of_mean += (node.nonDropCBRPacketCount() - mean) * (node.nonDropCBRPacketCount() - mean);
        }
        return Math.sqrt(sum_of_mean / nodes.size());
    }

    public static double calc_balance_factor(Map<Integer, NodeData> nodes) {
        double sum = 0;
        double sum_square = 0;

        for (NodeData node : nodes.values()) {
            sum += node.cbrPacketCount();
            sum_square += Math.pow(node.cbrPacketCount(), 2);
        }

        return Math.pow(sum, 2) / (nodes.size() * sum_square);
    }

    public static double calc_balance_factor2(Map<Integer, NodeData> nodes) {
        double sum = 0;
        double sum_square = 0;

        for (NodeData node : nodes.values()) {
            long tmp = node.getCbrCount() + node.getDroppedCBRCount();
            sum += tmp;
            sum_square += Math.pow(tmp, 2);
        }

        return Math.pow(sum, 2) / (nodes.size() * sum_square);
    }

}
