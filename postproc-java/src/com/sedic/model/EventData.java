package com.sedic.model;

/**
 * Created by huyvq on 2/7/2016.
 */
public class EventData {
    public enum EventType {
        SEND, RECEIVE, FORWARD, DROP, ENERGY, UNKNOWN
    }

    private int id;
    private EventType type;
    private double happenedTime;
    private NodeData node;
    private NodeData previousNode;
    private PacketData packet;
    private String layer;
    private String message;

    public void setId(int id) {
        this.id = id;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public double getHappenedTime() {
        return happenedTime;
    }

    public void setHappenedTime(double happenedTime) {
        this.happenedTime = happenedTime;
    }

    public NodeData getNode() {
        return node;
    }

    public void setNode(NodeData node) {
        this.node = node;
    }

    public NodeData getPreviousNode() {
        return previousNode;
    }

    public void setPreviousNode(NodeData previousNode) {
        this.previousNode = previousNode;
    }

    public PacketData getPacket() {
        return packet;
    }

    public void setPacket(PacketData packet) {
        this.packet = packet;
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}


