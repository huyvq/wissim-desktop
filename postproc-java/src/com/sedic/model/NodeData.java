package com.sedic.model;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by huyvq on 2/7/2016.
 */
public class NodeData {
    private int id;
    private double x;
    private double y;
    private double offtime;
    private double remainingEnergy;
    private ArrayList<PacketData> packetList;
    private ArrayList<EventData> nodeEvent;
    private double energyConsumed;

    private long cbrCount;
    private long droppedCBRCount;

    public NodeData(int id, double x, double y, double energy, double offtime, double energyConsumed) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.remainingEnergy = energy;
        this.offtime = offtime;
        this.energyConsumed = energyConsumed;
        this.packetList = new ArrayList<>();
        this.nodeEvent = new ArrayList<>();
    }

    public NodeData(int id, double x, double y, long cbrCount, long droppedCBRCount) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.cbrCount = cbrCount;
        this.droppedCBRCount = droppedCBRCount;
    }

    public int getId() {
        return id;
    }

    public double getOfftime() {
        return offtime;
    }

    public double getRemainingEnergy() {
        return remainingEnergy;
    }

    public double getEnergyConsumed() {
        return energyConsumed;
    }

    public ArrayList<EventData> getNodeEvent() {
        return nodeEvent;
    }

    public ArrayList<PacketData> getPacketList() {
        if (packetList.isEmpty()) {
            nodeEvent.stream().filter(e -> !packetList.contains(e.getPacket())).forEach(e -> packetList.add(e.getPacket()));
        }
        return packetList;
    }

    public long nonDropCBRPacketCount() {
        long count = 0;
        ArrayList<PacketData> PacketList = getPacketList();
        if (!PacketList.isEmpty()) {
            count = PacketList.stream().filter(e -> Objects.equals(e.getPacketType(), "cbr") && !e.isDrop()).count();
        }
        return count;
    }

    public long dropCBRPacketCount() {
        long count = 0;
        ArrayList<PacketData> PacketList = getPacketList();
        if (!PacketList.isEmpty()) {
            count = PacketList.stream().filter(e -> Objects.equals(e.getPacketType(), "cbr") && e.isDrop()).count();
        }
        return count;
    }

    public long cbrPacketCount() {
        return nonDropCBRPacketCount() + dropCBRPacketCount();
    }

    public double getY() {
        return y;
    }

    public double getX() {
        return x;
    }

    public long getCbrCount() {
        return cbrCount;
    }

    public long getDroppedCBRCount() {
        return droppedCBRCount;
    }
}
