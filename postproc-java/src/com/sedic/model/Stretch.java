package com.sedic.model;

/**
 * Created by huyvq on 6/1/2016.
 */
public class Stretch {
    double hop_count;
    double time;

    public double getHopCount() {
        return hop_count;
    }

    public double getTime() {
        return time;
    }

    public Stretch(double hop_count_, double time_) {
        hop_count = hop_count_;
        time = time_;
    }

    public void addHopCount(double hop) {
        hop_count += hop;
        time += 1;
    }
}
