package com.sedic.model;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by huyvq on 2/7/2016.
 */
public class PacketData {
    private int id;
    private ArrayList<EventData> eventList;
    private ArrayList<NodeData> nodeList;
    private String packetType;
    private int packetSize;
    private Boolean isMultiCast;
    private boolean isDrop;
    private NodeData sourceNode;
    private NodeData destinationNode;

    public PacketData(int packetId) {
        this.id = packetId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<EventData> getEventList() {
        if (eventList == null) eventList = new ArrayList<>();
        return eventList;
    }

    public ArrayList<NodeData> getNodeList() {
        if(nodeList == null) nodeList = new ArrayList<>();
        return nodeList;
    }

    public String getPacketType() {
        return packetType;
    }

    public void setPacketType(String packetType) {
        this.packetType = packetType;
    }

    public void setPacketSize(int packetSize) {
        this.packetSize = packetSize;
    }

    public Boolean getMultiCast() {
        return isMultiCast;
    }

    public void setMultiCast(Boolean multiCast) {
        isMultiCast = multiCast;
    }

    public boolean isDrop() {
        return isDrop;
    }

    public void setDrop(boolean drop) {
        isDrop = drop;
    }

    public NodeData getSourceNode() {
        return sourceNode;
    }

    public void setSourceNode(NodeData sourceNode) {
        this.sourceNode = sourceNode;
    }

    public NodeData getDestinationNode() {
        return destinationNode;
    }

    public void setDestinationNode(NodeData destinationNode) {
        this.destinationNode = destinationNode;
    }

    public int HopCount() {
        if (!this.isMultiCast && !this.isDrop) {
            int hopcount = 0;
            for (EventData e : eventList)
            {
                if (Objects.equals(e.getLayer(), "RTR") && e.getType() == EventData.EventType.RECEIVE)
                {
                    hopcount++;
                }
            }
            return hopcount;
        }
        return 0;
    }

    public String SourceLayer() {
        return eventList.get(0).getLayer();
    }
}
