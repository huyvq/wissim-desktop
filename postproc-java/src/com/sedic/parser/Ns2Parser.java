package com.sedic.parser;

import com.sedic.model.EventData;
import com.sedic.model.NodeData;
import com.sedic.model.PacketData;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by huyvq on 2/7/2016.
 */
public class Ns2Parser {
    private Map<Integer, NodeData> nodeEnergies = new HashMap<>();
    private Map<Integer, NodeData> nodes = new HashMap<>();
    private Map<Integer, PacketData> packets = new HashMap<>();
    private ArrayList<EventData> events = new ArrayList<>();
    private ArrayList<String> packetTypes = new ArrayList<>();

    public Map<Integer, NodeData> GetNodes() {
        if (nodes == null) nodes = new HashMap<>();
        return nodes;
    }

    public Map<Integer, NodeData> GetNodeEnergies() {
        if (nodeEnergies == null) nodeEnergies = new HashMap<>();
        return nodeEnergies;
    }

    public Map<Integer, PacketData> GetPackets() {
        if (packets == null) packets = new HashMap<>();
        return packets;
    }

    public ArrayList<EventData> GetEvents() {
        if (events == null) events = new ArrayList<>();
        return events;
    }

    public ArrayList<String> GetPacketTypes() {
        if (packetTypes == null) packetTypes = new ArrayList<>();
        return packetTypes;
    }

    public void Parse(String[] traceFiles) {
        ParseEnergyFile(traceFiles[0]);
        ParseMainFile(traceFiles[1]);
    }

    public void Parse2(String[] traceFiles) {
        ParsePacketCountFile(traceFiles[0]);
    }

    private void ParseMainFile(String traceFile) {
        int[] eventIndex = {0};
        try (Stream<String> stream = Files.lines(Paths.get(traceFile))) {
            stream.forEach((mainCurrentLine) -> {
                String[] mainRetVal;
                if (!mainCurrentLine.isEmpty()) {
                    mainRetVal = mainCurrentLine.split("\\s+");
                    switch (mainRetVal[0]) {
                        case "N": // Energy: N -t 0.147751 -n 3 -e 999.998548
                            break;
                        case "s": // Send:      s 0.002259878 _237_ RTR  --- 0 HELLO 36 [0 0 0 0] ------- [237:255 -1:255 32 0]
                        case "r": //Receieve:   r 0.003336783 _475_ RTR  --- 1 HELLO 36 [0 ffffffff 4b 800] ------- [75:255 -1:255 32 0]
                        case "f": // forward:   f 34.925440740 _939_ RTR  --- 1112 GRID 76 [13a 3ab 3ac 800] ------- [86:255 938:255 65 938]
                        case "D": // drop:      D 34.936054784 _256_ RTR  REPEAT 1113 GRID 74 [13a 100 3c3 800] ------- [537:255 256:255 65 256]
                            //With energy:
                            //Send: s 0.002259878 _237_ RTR  --- 0 HELLO 36 [0 0 0 0] [energy 1000.000000 ei 0.000 es 0.000 et 0.000 er 0.000] ------- [237:255 -1:255 32 0]
                            //Forward: f 364.672019363 _879_ RTR  --- 1398 cbr 123 [13a 36f 2ce 800] [energy 996.434502 ei 3.484 es 0.000 et 0.003 er 0.079] ------- [527:0 -1:0 94 878]

                            // --> Truong hop nay lai ko co IP header o sau cung (ma dang dung goi tin ARP de hoi thong tin.
                            //D 31.118554157 _1177_ IFQ  --- 0 ARP 28 [0 ffffffff 499 806] [energy 999.683405 ei 0.285 es 0.000 et 0.006 er 0.026] ------- [REQUEST 1177/1177 0/1062]
                            // New packet
                            if (mainRetVal.length < 17) {
                                break;
                            }

                            int packetId = Integer.valueOf(mainRetVal[5]);
                            PacketData packet;
                            if (!packets.containsKey(packetId)) {
                                packet = new PacketData(packetId);
                                packet.setPacketType(mainRetVal[6]);
                                if (!this.packetTypes.contains(packet.getPacketType())) {
                                    packetTypes.add(packet.getPacketType());
                                }
                                packet.setPacketSize(Integer.valueOf(mainRetVal[7]));
                                if (mainRetVal[0].equals("s")) {
                                    String sourceNodeId = mainRetVal[13];
                                    String destinationNodeId = mainRetVal[14];
                                    if (mainRetVal[12].equals("[energy")) {
                                        sourceNodeId = mainRetVal[23];
                                        destinationNodeId = mainRetVal[24];
                                    }

                                    sourceNodeId = sourceNodeId.substring(1, sourceNodeId.indexOf(":"));
                                    packet.setSourceNode(nodes.get(Integer.valueOf(sourceNodeId)));
                                    destinationNodeId = destinationNodeId.substring(0, destinationNodeId.indexOf(":"));
                                    if (destinationNodeId.equals("-1")) {
                                        packet.setDestinationNode(null);
                                        packet.setMultiCast(true);
                                    } else {
                                        packet.setDestinationNode(nodes.get(Integer.valueOf(destinationNodeId)));
                                        packet.setMultiCast(false);
                                    }
                                    packet.setDrop(false);
                                }
                                packets.put(packetId, packet);
                            } else {
                                packet = packets.get(packetId);
                            }
                            int nodeId = Integer.valueOf(mainRetVal[2].substring(1, mainRetVal[2].length() - 1));

                            // New event
                            EventData e = new EventData();
                            e.setId(eventIndex[0]++);
                            e.setHappenedTime(Double.valueOf(mainRetVal[1]));
                            e.setNode(nodes.get(nodeId));
                            e.setLayer(mainRetVal[3]);
                            e.setMessage(mainRetVal[4]);
                            if (mainRetVal[0].equals("s")) e.setType(EventData.EventType.SEND);
                            if (mainRetVal[0].equals("r")) {
                                e.setType(EventData.EventType.RECEIVE);

                                //Update previousNode
                                String previousNodeId = mainRetVal[10];
                                e.setPreviousNode(nodes.get(Integer.parseInt(previousNodeId, 16)));
                            }
                            if (mainRetVal[0].equals("f")) e.setType(EventData.EventType.FORWARD);
                            if (mainRetVal[0].equals("D")) {
                                e.setType(EventData.EventType.DROP);
                                if (packet.getDestinationNode() != null) {
                                    //Ko phai BroadCast. Truong hop (Distribued, Unicast). Multicast chua xac dinh dat o dau
                                    if ((nodeId != packet.getDestinationNode().getId()) || (packet.SourceLayer().equals(e.getLayer()))) {
                                        //Note: Gia thiet la da co 1 event send danh cho packet dang xet nay.
                                        packet.setDrop(true);
                                    }
                                }
                            }
                            e.setPacket(packet);
                            //Update header of packets.
                            //string[] headers = mainCurrentLine.Split(new char[] { '[',']' }, StringSplitOptions.RemoveEmptyEntries);
                            //e.AddHeader(headers[1]);
                            //e.AddHeader(headers[headers.Length - 2]);
                            events.add(e);

                            // Update packet.NodeList; packet.EventList
                            packet.getEventList().add(e);
                            packet.getNodeList().add(e.getNode());
//                            if (!packet.getNodeList().contains(e.getNode())) {
//                            packet.getNodeList().add(e.getNode());
//                            }
                            // Update node.nodeEvent
                            nodes.get(nodeId).getNodeEvent().add(e);
                            break;

                        case "M": // Move:
                            // do nothing
                            break;
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void ParseEnergyFile(String energyFilePath) {
        try (Stream<String> stream = Files.lines(Paths.get(energyFilePath))) {
            stream.forEach((currentLine) -> {
                if (!currentLine.isEmpty()) {
                    String[] retVal = currentLine.split("\\t");
                    int id = Integer.valueOf(retVal[0]);
                    double x = Double.valueOf(retVal[1]);
                    double y = Double.valueOf(retVal[2]);
                    double energyConsumed = Double.valueOf(retVal[3]);
                    double energy = Double.valueOf(retVal[5]) + Double.valueOf(retVal[6]);
                    double offtime = Double.valueOf(retVal[4]);
                    NodeData node = new NodeData(id, x, y, energy, offtime, energyConsumed);
                    nodeEnergies.put(id, node);
                    nodes.put(id, node);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ParsePacketCountFile(String packetCountFilePath) {
        try (Stream<String> stream = Files.lines(Paths.get(packetCountFilePath))) {
            stream.forEach((currentLine) -> {
                if (!currentLine.isEmpty()) {
                    String[] retVal = currentLine.split("\\t");
                    int id = Integer.valueOf(retVal[0]);
                    double x = Double.valueOf(retVal[1]);
                    double y = Double.valueOf(retVal[2]);
                    long cbrCount = Long.valueOf(retVal[3]);
                    long droppoed = Long.valueOf(retVal[4]);
                    NodeData node = new NodeData(id, x, y, cbrCount, droppoed);
                    nodes.put(id, node);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
