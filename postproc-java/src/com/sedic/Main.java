package com.sedic;

import com.sedic.model.Stretch;
import com.sedic.parser.Ns2Parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        System.out.println("[BF] Enter path: ");
        Scanner scanner = new Scanner(System.in);
        String path = scanner.nextLine();
//        String path = "/home/wissim/elbar";
        process(new File(path));
    }

    public static void process(File node) {
        ArrayList<File> files = new ArrayList<>(Arrays.asList(node.listFiles()));
        files.sort((p1, p2) -> p1.getAbsolutePath().compareTo(p2.getAbsolutePath()));
        if (files.get(0).isFile()) {
            String[] traces = new String[2];
            files.forEach(f -> {
                if (f.getName().equalsIgnoreCase("trace.tr")) {
                    traces[1] = f.getAbsolutePath();
                } else if (f.getName().equalsIgnoreCase("energy.tr")) {
                    traces[0] = f.getAbsolutePath();
                }
            });
            Ns2Parser parser = new Ns2Parser();
            parser.Parse(traces);
            dump(node.getAbsolutePath(), parser);
            dump3(node.getAbsolutePath(), parser);
            dump4(node.getAbsolutePath(), parser);
        } else if (node.isDirectory()) {
            files.forEach(Main::process);
        }
    }

    // convert packet count.tr to BF factor
    public static void process2(File node) {
        ArrayList<File> files = new ArrayList<>(Arrays.asList(node.listFiles()));
        files.sort((p1, p2) -> p1.getAbsolutePath().compareTo(p2.getAbsolutePath()));
        if (files.get(0).isFile()) {
            String[] traces = new String[2];
            files.forEach(f -> {
                if (f.getName().equalsIgnoreCase("packetcount.tr")) {
                    traces[0] = f.getAbsolutePath();
                }
            });
            Ns2Parser parser = new Ns2Parser();
            parser.Parse2(traces);
            dump5(node.getAbsolutePath(), parser);
        } else if (node.isDirectory()) {
            files.forEach(Main::process2);
        }
    }

    public static void dump(String exp, Ns2Parser parser) {
        System.out.println(exp);
        System.out.println(Process.calc_average_energy(parser.GetNodeEnergies()));
        System.out.println(Process.calc_avg_hop_count(parser.GetPackets()));
        System.out.println(Process.calc_standard_deviation_energy(parser.GetNodeEnergies()));
        System.out.println(Process.get_min_lifetime(parser.GetNodeEnergies()));
        System.out.println(Process.calc_standard_deviation_packet_count(parser.GetNodes()));
        System.out.println(Process.calc_balance_factor(parser.GetNodes()));
    }

    public static void dump3(String sfile, Ns2Parser parser) {
        List<String> lines = new ArrayList<>();
        parser.GetNodes().forEach((k, v) -> {
            String l = String.format("%d\t%f\t%f\t%d\t%d\t%d", v.getId(), v.getX(), v.getY(),
                    v.nonDropCBRPacketCount(), v.dropCBRPacketCount(), v.nonDropCBRPacketCount() + v.dropCBRPacketCount());
            lines.add(l);
        });
        Path file = Paths.get(sfile + "/PacketCount.tr");
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void dump4(String sfile, Ns2Parser parser) {
        List<String> lines = new ArrayList<>();

        Map<Integer, Stretch> stretch = new HashMap<>();
        parser.GetPackets().forEach((k, v) -> {
            if (!v.isDrop() && v.getPacketType().equalsIgnoreCase("CBR")) {
                Stretch str;
                if (stretch.containsKey(v.getSourceNode().getId())) {
                    str = stretch.get(v.getSourceNode().getId());
                } else {
                    str = new Stretch(0, 0);
                }
                str.addHopCount(v.HopCount());
                stretch.put(v.getSourceNode().getId(), str);
            }
        });

        stretch.forEach((k, v) -> {
            String l = String.format("%d\t%f", k, v.getHopCount() / v.getTime());
            lines.add(l);
        });

        Path file = Paths.get(sfile + "/Stretch.tr");
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void dump5(String exp, Ns2Parser parser) {
        System.out.println(exp);
        System.out.println(Process.calc_balance_factor2(parser.GetNodes()));
    }

    public static void dump2(String file) {
        String[] filter = new String[]{"n1-normal", "nn-normal", "n1-lifetime", "nn-lifetime"};
        HashMap<String, HashMap<String, HashMap<Integer, ArrayList<Double>>>> dump = new HashMap<>();
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            for (String f : filter) {
                List<String> list;
                HashMap<String, HashMap<Integer, ArrayList<Double>>> result = new HashMap<>();
                list = stream
                        .filter(line -> line.indexOf(f) > 0)
                        .map(String::toString)
                        .collect(Collectors.toList());
                for (String l : list) {
                    String[] ret = l.split("\\s+");
                    String[] exp = ret[0].split("/");
                    int cbr = Integer.parseInt(exp[0]);
                    String alg = exp[1];
                    if (result.containsKey(alg)) {
                        if (!result.get(alg).containsKey(cbr)) {
                            result.get(alg).put(cbr, new ArrayList<>());
                        }
                    } else {
                        result.put(alg, new HashMap<>());
                    }
                    for (int i = 0; i < 4; i++) {
                        result.get(alg).get(cbr).add(Double.parseDouble(ret[i + 1]));
                    }
                }
                dump.put(f, result);
            }
        } catch (Exception ignored) {
        }

        dump.forEach((k, v) -> {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(k + "\t");

        });
    }
}
